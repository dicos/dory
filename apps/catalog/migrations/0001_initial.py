# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table(u'catalog_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('machine_id', self.gf('django.db.models.fields.SlugField')(default='Cat', unique=True, max_length=50)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=125)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='children', null=True, to=orm['catalog.Category'])),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('price', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('index', self.gf('django.db.models.fields.SmallIntegerField')(default=1)),
        ))
        db.send_create_signal(u'catalog', ['Category'])

        # Adding model 'Parameter'
        db.create_table(u'catalog_parameter', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=125)),
        ))
        db.send_create_signal(u'catalog', ['Parameter'])

        # Adding model 'Parameter_group'
        db.create_table(u'catalog_parameter_group', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=125)),
            ('parameter_type', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'catalog', ['Parameter_group'])

        # Adding M2M table for field parameters on 'Parameter_group'
        m2m_table_name = db.shorten_name(u'catalog_parameter_group_parameters')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('parameter_group', models.ForeignKey(orm[u'catalog.parameter_group'], null=False)),
            ('parameter', models.ForeignKey(orm[u'catalog.parameter'], null=False))
        ))
        db.create_unique(m2m_table_name, ['parameter_group_id', 'parameter_id'])

        # Adding model 'CategoryParameterGroup'
        db.create_table(u'catalog_categoryparametergroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Category'])),
            ('parameter_group', self.gf('django.db.models.fields.related.ForeignKey')(related_name='category_parameters', to=orm['catalog.Parameter_group'])),
        ))
        db.send_create_signal(u'catalog', ['CategoryParameterGroup'])

        # Adding model 'ParameterParameterGroup'
        db.create_table(u'catalog_parameterparametergroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parameter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Parameter'])),
            ('parameter_group', self.gf('django.db.models.fields.related.ForeignKey')(related_name='Parameter_parameters', to=orm['catalog.Parameter_group'])),
        ))
        db.send_create_signal(u'catalog', ['ParameterParameterGroup'])

        # Adding model 'Ad'
        db.create_table(u'catalog_ad', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='client_ads', to=orm['client.Client'])),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='category_ads', null=True, to=orm['catalog.Category'])),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_geoip.Region'])),
            ('city', self.gf('smart_selects.db_fields.ChainedForeignKey')(related_name='city_ads', to=orm['django_geoip.City'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=125)),
            ('price', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('text', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('bet', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2015, 2, 25, 0, 0), auto_now=True, blank=True)),
            ('priority', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('days_for_expire', self.gf('django.db.models.fields.PositiveIntegerField')(default=3)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_deleted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('views', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'catalog', ['Ad'])

        # Adding model 'AdPhone'
        db.create_table(u'catalog_adphone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ad', self.gf('django.db.models.fields.related.ForeignKey')(related_name='phones', to=orm['catalog.Ad'])),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'catalog', ['AdPhone'])

        # Adding model 'AdPhoto'
        db.create_table(u'catalog_adphoto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('ad', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='ad_photos', null=True, to=orm['catalog.Ad'])),
            ('img_title', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('index', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0, null=True, blank=True)),
        ))
        db.send_create_signal(u'catalog', ['AdPhoto'])

        # Adding model 'AdTerms'
        db.create_table(u'catalog_adterms', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ad', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='ad_terms', null=True, to=orm['catalog.Ad'])),
            ('term_text', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'catalog', ['AdTerms'])

        # Adding model 'AdParams'
        db.create_table(u'catalog_adparams', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ad', self.gf('django.db.models.fields.related.ForeignKey')(related_name='ad_parameters', to=orm['catalog.Ad'])),
            ('parameter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Parameter_group'], null=True)),
            ('parameter_value', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Parameter'], null=True, blank=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('number', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('boolean', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'catalog', ['AdParams'])

        # Adding model 'AdFavorits'
        db.create_table(u'catalog_adfavorits', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ad', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Ad'])),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='client_favorits', to=orm['client.Client'])),
        ))
        db.send_create_signal(u'catalog', ['AdFavorits'])

        # Adding model 'Coupon'
        db.create_table(u'catalog_coupon', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='client_coupons', null=True, to=orm['client.Client'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='category_coupons', null=True, to=orm['catalog.CouponCategory'])),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='city_coupons', null=True, to=orm['django_geoip.City'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('count', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2015, 2, 25, 0, 0), auto_now_add=True, blank=True)),
            ('date_start', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('date_finish', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'catalog', ['Coupon'])

        # Adding model 'CouponPhoto'
        db.create_table(u'catalog_couponphoto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('coupon', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='coupon_photos', null=True, to=orm['catalog.Coupon'])),
            ('img_title', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('index', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0, null=True, blank=True)),
        ))
        db.send_create_signal(u'catalog', ['CouponPhoto'])

        # Adding model 'CouponOption'
        db.create_table(u'catalog_couponoption', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('coupon', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Coupon'])),
            ('option', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Option'])),
            ('date_activation', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'catalog', ['CouponOption'])

        # Adding model 'Option'
        db.create_table(u'catalog_option', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('cost', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('duration', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('permanent', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'catalog', ['Option'])

        # Adding model 'CouponCategory'
        db.create_table(u'catalog_couponcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='children', null=True, to=orm['catalog.CouponCategory'])),
            ('index', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'catalog', ['CouponCategory'])

        # Adding M2M table for field bind_categories on 'CouponCategory'
        m2m_table_name = db.shorten_name(u'catalog_couponcategory_bind_categories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('couponcategory', models.ForeignKey(orm[u'catalog.couponcategory'], null=False)),
            ('category', models.ForeignKey(orm[u'catalog.category'], null=False))
        ))
        db.create_unique(m2m_table_name, ['couponcategory_id', 'category_id'])


    def backwards(self, orm):
        # Deleting model 'Category'
        db.delete_table(u'catalog_category')

        # Deleting model 'Parameter'
        db.delete_table(u'catalog_parameter')

        # Deleting model 'Parameter_group'
        db.delete_table(u'catalog_parameter_group')

        # Removing M2M table for field parameters on 'Parameter_group'
        db.delete_table(db.shorten_name(u'catalog_parameter_group_parameters'))

        # Deleting model 'CategoryParameterGroup'
        db.delete_table(u'catalog_categoryparametergroup')

        # Deleting model 'ParameterParameterGroup'
        db.delete_table(u'catalog_parameterparametergroup')

        # Deleting model 'Ad'
        db.delete_table(u'catalog_ad')

        # Deleting model 'AdPhone'
        db.delete_table(u'catalog_adphone')

        # Deleting model 'AdPhoto'
        db.delete_table(u'catalog_adphoto')

        # Deleting model 'AdTerms'
        db.delete_table(u'catalog_adterms')

        # Deleting model 'AdParams'
        db.delete_table(u'catalog_adparams')

        # Deleting model 'AdFavorits'
        db.delete_table(u'catalog_adfavorits')

        # Deleting model 'Coupon'
        db.delete_table(u'catalog_coupon')

        # Deleting model 'CouponPhoto'
        db.delete_table(u'catalog_couponphoto')

        # Deleting model 'CouponOption'
        db.delete_table(u'catalog_couponoption')

        # Deleting model 'Option'
        db.delete_table(u'catalog_option')

        # Deleting model 'CouponCategory'
        db.delete_table(u'catalog_couponcategory')

        # Removing M2M table for field bind_categories on 'CouponCategory'
        db.delete_table(db.shorten_name(u'catalog_couponcategory_bind_categories'))


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'catalog.ad': {
            'Meta': {'object_name': 'Ad'},
            'bet': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'category_ads'", 'null': 'True', 'to': u"orm['catalog.Category']"}),
            'city': ('smart_selects.db_fields.ChainedForeignKey', [], {'related_name': "'city_ads'", 'to': u"orm['django_geoip.City']"}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'client_ads'", 'to': u"orm['client.Client']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 2, 25, 0, 0)', 'auto_now': 'True', 'blank': 'True'}),
            'days_for_expire': ('django.db.models.fields.PositiveIntegerField', [], {'default': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['django_geoip.Region']"}),
            'text': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'views': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'catalog.adfavorits': {
            'Meta': {'object_name': 'AdFavorits'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Ad']"}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'client_favorits'", 'to': u"orm['client.Client']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'catalog.adparams': {
            'Meta': {'object_name': 'AdParams'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ad_parameters'", 'to': u"orm['catalog.Ad']"}),
            'boolean': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'parameter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Parameter_group']", 'null': 'True'}),
            'parameter_value': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Parameter']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'catalog.adphone': {
            'Meta': {'object_name': 'AdPhone'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'phones'", 'to': u"orm['catalog.Ad']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'catalog.adphoto': {
            'Meta': {'ordering': "['index', 'id']", 'object_name': 'AdPhoto'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ad_photos'", 'null': 'True', 'to': u"orm['catalog.Ad']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_title': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'index': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        u'catalog.adterms': {
            'Meta': {'object_name': 'AdTerms'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ad_terms'", 'null': 'True', 'to': u"orm['catalog.Ad']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'term_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'catalog.category': {
            'Meta': {'ordering': "['index', 'name']", 'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'machine_id': ('django.db.models.fields.SlugField', [], {'default': "'Cat'", 'unique': 'True', 'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'parameters': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['catalog.Parameter_group']", 'null': 'True', 'through': u"orm['catalog.CategoryParameterGroup']", 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['catalog.Category']"}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        u'catalog.categoryparametergroup': {
            'Meta': {'object_name': 'CategoryParameterGroup'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parameter_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'category_parameters'", 'to': u"orm['catalog.Parameter_group']"})
        },
        u'catalog.coupon': {
            'Meta': {'object_name': 'Coupon'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'category_coupons'", 'null': 'True', 'to': u"orm['catalog.CouponCategory']"}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'city_coupons'", 'null': 'True', 'to': u"orm['django_geoip.City']"}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'client_coupons'", 'null': 'True', 'to': u"orm['client.Client']"}),
            'count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2015, 2, 25, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'date_finish': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_start': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'catalog.couponcategory': {
            'Meta': {'object_name': 'CouponCategory'},
            'bind_categories': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['catalog.Category']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['catalog.CouponCategory']"}),
            'slug': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'catalog.couponoption': {
            'Meta': {'object_name': 'CouponOption'},
            'coupon': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Coupon']"}),
            'date_activation': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'option': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Option']"})
        },
        u'catalog.couponphoto': {
            'Meta': {'ordering': "['index', 'id']", 'object_name': 'CouponPhoto'},
            'coupon': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'coupon_photos'", 'null': 'True', 'to': u"orm['catalog.Coupon']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'img_title': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'index': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        u'catalog.option': {
            'Meta': {'object_name': 'Option'},
            'cost': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'duration': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'permanent': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'catalog.parameter': {
            'Meta': {'object_name': 'Parameter'},
            'child_groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['catalog.Parameter_group']", 'through': u"orm['catalog.ParameterParameterGroup']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '125'})
        },
        u'catalog.parameter_group': {
            'Meta': {'object_name': 'Parameter_group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'parameter_type': ('django.db.models.fields.IntegerField', [], {}),
            'parameters': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'parent_group'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['catalog.Parameter']"})
        },
        u'catalog.parameterparametergroup': {
            'Meta': {'object_name': 'ParameterParameterGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parameter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Parameter']"}),
            'parameter_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'Parameter_parameters'", 'to': u"orm['catalog.Parameter_group']"})
        },
        u'client.client': {
            'Meta': {'object_name': 'Client'},
            'city': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': u"orm['django_geoip.City']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'interests': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['catalog.Category']", 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['django_geoip.Region']", 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'type': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'client'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'django_geoip.city': {
            'Meta': {'unique_together': "(('region', 'name'),)", 'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '6', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '6', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cities'", 'to': u"orm['django_geoip.Region']"})
        },
        u'django_geoip.country': {
            'Meta': {'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2', 'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'django_geoip.region': {
            'Meta': {'unique_together': "(('country', 'name'),)", 'object_name': 'Region'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'regions'", 'to': u"orm['django_geoip.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['catalog']