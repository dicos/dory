#!-*-coding:utf-8-*-
import urllib2
import json

from django_geoip.models import City, IpRange

from client.models import Client


def load_cities():
    url = 'http://api.vk.com/method/database.getCities?country_id=1&need_all=1&v=5.5&count=1000'

    r = urllib2.urlopen(url)
    r = json.loads(r.read())
    cities = r['response']['items']
    for i in cities:
        print(i['title'])


def get_city(request):
    ip = request.META.get('REMOTE_ADDR')

    if request.session.get('city_ad_display', None):
        try:
            return City.objects.get(pk=request.session.get('city_ad_display'))
        except City.DoesNotExist:
            pass

    if request.user.is_authenticated():
        client = Client.get_client(request)
        if client and client.city_id is not None:
            return client.city

    try:
        return IpRange.objects.by_ip(ip).city
    except IpRange.DoesNotExist:
        return City.objects.get(name__icontains=u'Москва')