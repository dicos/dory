# -*- coding:utf-8 -*-
from django.conf.urls import patterns, include, url, static
from client.views import AccountProfile, PartnerListView

urlpatterns = patterns('',
    url(r'^profile/(?P<pk>\d+)/$', AccountProfile.as_view(), name='profile'),  #Профиль
    url(r'^login/$', 'client.views.login', name='login'),  #Вход
    url(r'^logout/$', 'client.views.logout', name='logout'),  #Выход
    url(r'^signup/$', 'client.views.sign_up', name='sign_up'),  #Регистрация
    url(r'^reg/$', 'client.views.registration', name='registration'),  #Завершение регистрации
    url(r'^reset_password/$', 'client.views.reset_password', name='reset_password'),  #Запрос на сброс пароля
    url(r'^reset_password_confirm/', 'client.views.reset_password_confirm', name='reset_password_confirm'),  #Сброс пароля
    url(r'^account/$', 'client.views.client_account_view', name='account'),
    url(r'^upload_image/$', 'client.views.upload_client_image', name='upload_client_image'),
    url(r'^partners/$', PartnerListView.as_view(), name='get_partners'),
    url(r'^rating/$', 'client.views.client_account_rating_view', name='account_rating'),
)