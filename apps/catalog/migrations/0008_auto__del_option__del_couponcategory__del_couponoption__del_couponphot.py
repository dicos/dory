# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Option'
        db.delete_table(u'catalog_option')

        # Deleting model 'CouponCategory'
        db.delete_table(u'catalog_couponcategory')

        # Removing M2M table for field bind_categories on 'CouponCategory'
        db.delete_table(db.shorten_name(u'catalog_couponcategory_bind_categories'))

        # Deleting model 'CouponOption'
        db.delete_table(u'catalog_couponoption')

        # Deleting model 'CouponPhoto'
        db.delete_table(u'catalog_couponphoto')

        # Deleting model 'Coupon'
        db.delete_table(u'catalog_coupon')

        # Deleting field 'AdPhoto.img_title'
        db.delete_column(u'catalog_adphoto', 'img_title')


    def backwards(self, orm):
        # Adding model 'Option'
        db.create_table(u'catalog_option', (
            ('cost', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('permanent', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('duration', self.gf('django.db.models.fields.IntegerField')(default=0)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'catalog', ['Option'])

        # Adding model 'CouponCategory'
        db.create_table(u'catalog_couponcategory', (
            ('index', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(related_name='children', null=True, to=orm['catalog.CouponCategory'], blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slug', self.gf('django.db.models.fields.CharField')(max_length=255, unique=True)),
        ))
        db.send_create_signal(u'catalog', ['CouponCategory'])

        # Adding M2M table for field bind_categories on 'CouponCategory'
        m2m_table_name = db.shorten_name(u'catalog_couponcategory_bind_categories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('couponcategory', models.ForeignKey(orm[u'catalog.couponcategory'], null=False)),
            ('category', models.ForeignKey(orm[u'catalog.category'], null=False))
        ))
        db.create_unique(m2m_table_name, ['couponcategory_id', 'category_id'])

        # Adding model 'CouponOption'
        db.create_table(u'catalog_couponoption', (
            ('coupon', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Coupon'])),
            ('date_activation', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('option', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['catalog.Option'])),
        ))
        db.send_create_signal(u'catalog', ['CouponOption'])

        # Adding model 'CouponPhoto'
        db.create_table(u'catalog_couponphoto', (
            ('index', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0, null=True, blank=True)),
            ('img', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('coupon', self.gf('django.db.models.fields.related.ForeignKey')(related_name='coupon_photos', null=True, to=orm['catalog.Coupon'], blank=True)),
            ('img_title', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'catalog', ['CouponPhoto'])

        # Adding model 'Coupon'
        db.create_table(u'catalog_coupon', (
            ('count', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2016, 11, 21, 0, 0), auto_now_add=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name='category_coupons', null=True, to=orm['catalog.CouponCategory'])),
            ('city', self.gf('smart_selects.db_fields.ChainedForeignKey')(related_name='city_coupons', null=True, to=orm['django_geoip.City'], blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['django_geoip.Region'], null=True, blank=True)),
            ('date_start', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name='client_coupons', null=True, to=orm['client.Client'])),
            ('date_finish', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'catalog', ['Coupon'])

        # Adding field 'AdPhoto.img_title'
        db.add_column(u'catalog_adphoto', 'img_title',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'catalog.ad': {
            'Meta': {'object_name': 'Ad'},
            'bet': ('django.db.models.fields.FloatField', [], {'default': '0.0', 'null': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'category_ads'", 'null': 'True', 'to': u"orm['catalog.Category']"}),
            'check_color_price': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('smart_selects.db_fields.ChainedForeignKey', [], {'related_name': "'city_ads'", 'to': u"orm['django_geoip.City']"}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'client_ads'", 'to': u"orm['client.Client']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2017, 4, 4, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'days_for_expire': ('django.db.models.fields.PositiveIntegerField', [], {'default': '3'}),
            'declines': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'declines_rel_+'", 'null': 'True', 'to': u"orm['catalog.Ad']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_deleted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_shown': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'priority': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['django_geoip.Region']"}),
            'text': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'views': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'catalog.adfavorits': {
            'Meta': {'object_name': 'AdFavorits'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Ad']"}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'client_favorits'", 'to': u"orm['client.Client']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'catalog.adparams': {
            'Meta': {'object_name': 'AdParams'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ad_parameters'", 'to': u"orm['catalog.Ad']"}),
            'boolean': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'parameter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Parameter_group']", 'null': 'True'}),
            'parameter_value': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Parameter']", 'null': 'True', 'blank': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'catalog.adphone': {
            'Meta': {'object_name': 'AdPhone'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'phones'", 'to': u"orm['catalog.Ad']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'catalog.adphoto': {
            'Meta': {'ordering': "['index', 'id']", 'object_name': 'AdPhoto'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ad_photos'", 'null': 'True', 'to': u"orm['catalog.Ad']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'index': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'})
        },
        u'catalog.adterms': {
            'Meta': {'object_name': 'AdTerms'},
            'ad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ad_terms'", 'null': 'True', 'to': u"orm['catalog.Ad']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'term_text': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'catalog.category': {
            'Meta': {'ordering': "['index', 'name']", 'object_name': 'Category'},
            'color_price': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'machine_id': ('django.db.models.fields.SlugField', [], {'default': "'Cat'", 'unique': 'True', 'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'parameters': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['catalog.Parameter_group']", 'null': 'True', 'through': u"orm['catalog.CategoryParameterGroup']", 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['catalog.Category']"}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        u'catalog.categoryparametergroup': {
            'Meta': {'object_name': 'CategoryParameterGroup'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parameter_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'category_parameters'", 'to': u"orm['catalog.Parameter_group']"})
        },
        u'catalog.parameter': {
            'Meta': {'object_name': 'Parameter'},
            'child_groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['catalog.Parameter_group']", 'through': u"orm['catalog.ParameterParameterGroup']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '125'})
        },
        u'catalog.parameter_group': {
            'Meta': {'object_name': 'Parameter_group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'parameter_type': ('django.db.models.fields.IntegerField', [], {}),
            'parameters': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'parent_group'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['catalog.Parameter']"})
        },
        u'catalog.parameterparametergroup': {
            'Meta': {'object_name': 'ParameterParameterGroup'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parameter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['catalog.Parameter']"}),
            'parameter_group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'Parameter_parameters'", 'to': u"orm['catalog.Parameter_group']"})
        },
        u'client.client': {
            'Meta': {'object_name': 'Client'},
            'city': ('smart_selects.db_fields.ChainedForeignKey', [], {'to': u"orm['django_geoip.City']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'interests': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['catalog.Category']", 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['django_geoip.Region']", 'null': 'True', 'blank': 'True'}),
            'token': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'type': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'client'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'django_geoip.city': {
            'Meta': {'unique_together': "(('region', 'name'),)", 'object_name': 'City'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '6', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '9', 'decimal_places': '6', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cities'", 'to': u"orm['django_geoip.Region']"})
        },
        u'django_geoip.country': {
            'Meta': {'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2', 'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'django_geoip.region': {
            'Meta': {'unique_together': "(('country', 'name'),)", 'object_name': 'Region'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'regions'", 'to': u"orm['django_geoip.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['catalog']