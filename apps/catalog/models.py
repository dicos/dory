#! -*-coding:utf-8 -*-
import datetime

from django.db import models
from django.db.models import Q
from django.db.models.query import QuerySet

from catalog.settings import AD_TYPES, SELL, BUY, RENT_YOU, RENT_ME
from dory.settings import DAYS_FOR_EXPIRE
from tinymce.models import HTMLField
from django_geoip.models import City, Region
from smart_selects.db_fields import ChainedForeignKey

class Category(models.Model):
    machine_id = models.SlugField(u'Машинное имя', max_length=50, default='Cat', unique=True)
    name = models.CharField(u'Наименование категории', max_length=125)
    parent = models.ForeignKey('Category', limit_choices_to={'parent__isnull': True}, related_name='children',
                               null=True, blank=True, verbose_name=u'Родительская категория')
    parameters = models.ManyToManyField('Parameter_group', null=True, blank=True, through='CategoryParameterGroup')
    is_active = models.BooleanField(u'Отображать на сайте', default=True)
    price = models.FloatField(u'Цена категории', default=0, help_text=u'Показывать контакты для объявления после оплаты посетителем конкретной суммы указанной в админке для категории объявления')

    index = models.SmallIntegerField(u'Порядковый номер', default=1)
    color_price = models.FloatField(u'Цена выделения обьявления', default=0, help_text=u'Цена выделения обьявления для категории')


    def __unicode__(self):
        if self.parent:
            return self.name
        return self.name

    class Meta:
        ordering = ['index', 'name']
        verbose_name = u'Категория объявления'
        verbose_name_plural = u'Категории объявлений'

    def get_all_children(self):
        ids = [self.pk]
        for child_cat in self.children.all():
            ids.append(child_cat.pk)
        return Ad.objects.filter(category__in=ids)


class Parameter(models.Model):
    name = models.CharField(u'Значение параметра', max_length=125)
    child_groups = models.ManyToManyField('Parameter_group', verbose_name=u'Потомственные группы параметров',
                                          through='ParameterParameterGroup')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Значение параметра'
        verbose_name_plural = u'Значения параметров'


class Parameter_group(models.Model):
    TYPE = [(0, u'Группа параметров'), (1, u'Текстовое поле'), (2, u'Числовое поле'), (3, u'Галка')]

    name = models.CharField(u'Название группы параметров', max_length=125)
    parameter_type = models.IntegerField(u'Тип параметра', choices=TYPE)
    parameters = models.ManyToManyField(Parameter, verbose_name=u'Параметры', related_name='parent_group', null=True,
                                        help_text=u'Только для параметров типа "Группа параметров"', blank=True)

    def get_parameters(self):
        res = {'name': self.name,
               'type': self.parameter_type,
               'params': []}
        if res['type'] == 0:
            for param in self.parameters.all():
                print param

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Параметр'
        verbose_name_plural = u'Параметры'


class CategoryParameterGroup(models.Model):
    category = models.ForeignKey('Category')
    parameter_group = models.ForeignKey('Parameter_group', related_name='category_parameters')


class ParameterParameterGroup(models.Model):
    parameter = models.ForeignKey('Parameter')
    parameter_group = models.ForeignKey('Parameter_group', related_name='Parameter_parameters')


class AdFilterManager(models.Manager):
    def get_queryset(self):
        return self.model.QuerySet(self.model).filter(is_deleted=False)


class Ad(models.Model):
    client = models.ForeignKey('client.Client', verbose_name=u'Клиент', related_name='client_ads')
    type = models.CharField(u'Тип объявления', max_length=50, choices=AD_TYPES)
    category = models.ForeignKey('Category', verbose_name=u'Категория', limit_choices_to={'parent__isnull': False},
                                 related_name='category_ads', null=True, blank=True)
    region = models.ForeignKey(Region, verbose_name=u'Регион')
    city = ChainedForeignKey(City, verbose_name=u'Город', chained_field="region", chained_model_field="region", related_name='city_ads')
    title = models.CharField(u'Название', max_length=125)
    price = models.FloatField(u'Цена', blank=True, null=True)
    price_free = models.BooleanField(u"Бесплатно", default=False)
    price_agreement = models.BooleanField(u"Договорная", default=False)
    text = HTMLField(u'Текст объявления', blank=True, null=True)
    phone = models.CharField(u'Телефон', max_length=100, blank=True, null=True)
    date = models.DateTimeField(u'Дата подачи', auto_now_add=True, default=datetime.datetime.now())

    priority = models.PositiveIntegerField(u'Приоритет', default=1)

    days_for_expire = models.PositiveIntegerField(u'Дней активно', default=DAYS_FOR_EXPIRE)
    is_active = models.BooleanField(u'Активно', default=True)
    is_shown = models.BooleanField(u'Отображается в ленте', default=True)
    is_deleted = models.BooleanField(u'Удалено', default=False)
    views = models.IntegerField(u'Просмотров', default=0, editable=False)
    check_color_price = models.BooleanField(u'Выделено объявления', default=False)

    declines = models.ManyToManyField("self", verbose_name=u"Отклоненные предложения", blank=True, null=True)

    client_declines = models.ManyToManyField("client.Client",
                                             verbose_name=u"Отклонили предложение",
                                             blank=True, null=True)

    objects = AdFilterManager()

    def __unicode__(self):
        return self.title

    def get_type(self):
        for i in AD_TYPES:
            if i[0] == self.type:
                return i[1]

    def get_phone(self):
        return self.phone or self.client.client_phone_numbers.first()

    def get_parameter_values(self):
        return [param.parameter_value for param in self.ad_parameters.all()]

    def get_cost(self):
        # orders = self.ad_option_orders.filter(closed=False, paid=False).aggregate(Sum('cost'))
        # options = self.options.filter(paid=False).aggregate(Sum('cost'))
        #return orders['cost__sum'] or 0
        return 0

    def is_favorits(self):
        return AdFavorits.objects.filter(ad=self).count()>0

    def get_deals(self):
        d_type = None
        if self.type == SELL:
            d_type = BUY
        elif self.type == BUY:
            d_type = SELL
        elif self.type == RENT_YOU:
            d_type = RENT_ME
        elif self.type == RENT_ME:
            d_type = RENT_YOU
        declines = self.declines.values_list('pk', flat=True)
        deals = Ad.objects.exclude(Q(client=self.client) | Q(pk__in=declines)) \
                          .filter(category=self.category, type=d_type)
        return deals

    class Meta:
        verbose_name = u'Объявление'
        verbose_name_plural = u'Объявления'

    class QuerySet(QuerySet):
        def filter_by_type(self, type):
            return self.filter(type=type)

        def filter_by_price(self, gt=None, lt=None):
            qs = self
            if gt: qs = qs.filter(price__gte=gt)
            if lt: qs = qs.filter(price__lte=lt)
            return qs


class AdPhone(models.Model):
    ad = models.ForeignKey(Ad, verbose_name=u'Объявление', related_name='phones')
    phone = models.CharField(u'Номер телефона', max_length=100)

    def __unicode__(self):
        return self.phone

    class Meta:
        verbose_name = u'Номер телефона объявления'
        verbose_name_plural = u'Номера телефонов объявления'


class AdPhoto(models.Model):
    img = models.ImageField(u'Изображение', upload_to='ad_photos', blank=True, null=True)
    ad = models.ForeignKey(Ad, related_name='ad_photos', null=True, blank=True)
    index = models.PositiveSmallIntegerField(u'Порядковый номер', default=0, null=True, blank=True)

    def __unicode__(self):
        return str(self.img.url)

    class Meta:
        ordering = ['index', 'id']


class AdTerms(models.Model):
    ad = models.ForeignKey(Ad, related_name='ad_terms', null=True, blank=True)
    term_text = models.CharField(u'Условия', max_length=255, blank=True)

    def __unicode__(self):
        return self.text


class AdParams(models.Model):
    ad = models.ForeignKey('Ad', verbose_name=u'Товар', related_name='ad_parameters')
    parameter = models.ForeignKey(Parameter_group, verbose_name=u'Тип параметра', null=True)
    parameter_value = models.ForeignKey(Parameter, verbose_name=u'Значение параметра',
                                        help_text=u'Только для параметров типа "Группа параметров"', null=True, blank=True)
    text = models.TextField(u'Значение текстового параметра',
                            help_text=u'Только для параметров типа "Текстовый параметр"', blank=True, null=True)
    number = models.FloatField(u'Значение числового параметра',
                               help_text=u'Только для параметров типа "Числовой параметр"', blank=True, null=True)
    boolean = models.BooleanField(u'Значение булевого параметра', help_text=u'Только для параметров типа "Галочка"',
                                  default=False)

    def __unicode__(self):
        return str(self.id)

    class Meta:
        verbose_name = u'Параметр объявления'
        verbose_name_plural = u'Параметры объявления'


class AdFavorits(models.Model):
    ad = models.ForeignKey(Ad, verbose_name=u'Избранное')
    client = models.ForeignKey('client.Client', verbose_name=u'Клиент', related_name='client_favorits')

    class Meta:
        verbose_name = u'Избранное объявление'
        verbose_name_plural = u'Избранные объявления'