from django.conf.urls import patterns, include, url, static
import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^chaining/', include('smart_selects.urls')),

    url(r'^$', include('content.urls')),
    url(r'^catalog/', include('catalog.urls')),
    url(r'^client/', include('client.urls')),
    url(r'^accounts/', include('client.urls')),
    url(r'^offers/', include('offers.urls')),
    url(r'^payments/', include('payments.urls')),
    url(r'^msg/', include('msg.urls')),
    url(r'^feedback/', include('feedbacks.urls')),
    url(r'^(?P<template_name>[A-Za-z0-9-]+)/$', 'content.views.test', name='test'),
)

urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
