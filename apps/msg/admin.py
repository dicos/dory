from django.contrib import admin
from msg.models import AdMessage, PrivateMessage

admin.site.register(AdMessage)
admin.site.register(PrivateMessage)
