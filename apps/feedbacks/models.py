#!-*-coding: utf-8-*-

from django.db import models
from django.db.models import Avg
from feedbacks.settings import RATINGS, FEEDBACK_TYPES


class FeedbackActiveManager(models.Manager):
    def get_queryset(self):
        return super(FeedbackActiveManager, self).get_queryset().filter(is_active=True)


class Feedback(models.Model):
    about = models.ForeignKey('client.Client', verbose_name=u'О клиенте', related_name='client_about_feedbacks')
    client = models.ForeignKey('client.Client', verbose_name=u'От клиента', related_name='client_from_feedbacks')
    type = models.SmallIntegerField(u'Тип отзыва', choices=FEEDBACK_TYPES, default=1)
    rating = models.SmallIntegerField(u'Оценка', choices=RATINGS, default=5)
    text = models.TextField(u'Текст', blank=True, null=True)

    date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    objects = FeedbackActiveManager()

    def __unicode__(self):
        return self.text or 'Feedback about %s' % self.about.user.username

    @classmethod
    def get_rating(cls, client):
        return client.client_about_feedbacks.filter(is_active=True).aggregate(Avg('rating'))

    @classmethod
    def get_positive(cls, client):
        return client.client_about_feedbacks.filter(is_active=True, rating__gte=4).count()

    @classmethod
    def get_neutral(cls, client):
        return client.client_about_feedbacks.filter(is_active=True, rating=3).count()

    @classmethod
    def get_negative(cls, client):
        return client.client_about_feedbacks.filter(is_active=True, rating__lte=2).count()

    def rating_point(self):
        if self.rating == 1:
            return '-2'
        if self.rating == 2:
            return '-1'
        if self.rating == 3:
            return '+1'
        if self.rating == 4:
            return '+2'
        if self.rating == 5:
            return '+3'

    class Meta:
        verbose_name = u'отзыв'
        verbose_name_plural = u'Отзывы'


class Answer(models.Model):
    feedback = models.ForeignKey(Feedback, verbose_name=u'Фидбэк')
    text = models.TextField(u'Ответ')

    date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.text

    class Meta:
        verbose_name = u'ответ'
        verbose_name_plural = u'Ответы'
