#!-*-coding: utf-8-*-

ORDER_UNCONFIRMED = 0
ORDER_IS_PAID = 1
ORDER_IS_UNPAID = 2
ORDER_FAILED = 3

REFILL_ORDER_STATUSES = [
    (ORDER_UNCONFIRMED, u'Не подтверждено'),
    (ORDER_IS_PAID, u'Оплачен'),
    (ORDER_IS_UNPAID, u'Не оплачен'),
    (ORDER_FAILED, u'Отклонен'),
]