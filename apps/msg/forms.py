#!-*-coding:utf-8-*-

from django import forms
from models import AdMessage, MailAdMessage, PrivateMessage


class AdMessageForm(forms.ModelForm):
    class Meta:
        model = AdMessage
        exclude = ('is_active', 'date')
        widgets = {
            'ad': forms.HiddenInput(),
            'client': forms.HiddenInput(),
            'text': forms.Textarea(attrs={'class': 'b-textarea b-textarea--full', 'rows': 4})
        }


class MailAdMessageForm(forms.ModelForm):
    class Meta:
        model = MailAdMessage
        exclude = ('date',)
        widgets = {
            'ad': forms.HiddenInput(),
            'email': forms.TextInput(attrs={'class': 'b-input-text'}),
            'text': forms.Textarea(attrs={'class': 'b-textarea b-textarea--full', 'rows': 4})
        }


class PrivateMessageForm(forms.ModelForm):
    class Meta:
        model = PrivateMessage
        exclude = ('is_readed', 'is_active', 'date')
        widgets = {
            'sender': forms.HiddenInput(),
            'recipient': forms.HiddenInput(),
            'text': forms.Textarea(attrs={'class': 'b-textarea b-textarea--full', 'rows': 4})
        }