#!-*-coding:utf-8-*-
from django.contrib.auth.models import User
from django.db import models


class News(models.Model):
    author = models.ForeignKey(User, verbose_name=u'Автор', related_name='user_news')
    title = models.CharField(u'Заголовок', max_length=255)
    short_text = models.TextField(u'Краткий текст')
    text = models.TextField(u'Текст')
    date = models.DateTimeField(u'Дата публикации', auto_now_add=True)
    is_active = models.BooleanField(u'Активно', default=True)

    def __unicode__(self):
        return self.title