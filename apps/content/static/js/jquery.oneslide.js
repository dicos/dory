;
jQuery.fn.oneslide = function(options) {
    o = $.extend({
        classNameScrollWrap: 'oneslide-scroll-wrap',
        classNameScroll: 'oneslide-scroll'
    }, options);
    // 
    var papa = $(this)
    var scroll = '.' + o.classNameScroll
    var scrollWrap = '.' + o.classNameScrollWrap
    //
    $(this).after('<div class="' + o.classNameScrollWrap + '"><div class="' + o.classNameScroll + '"></div></div>')
    // длина ширина проценты
    var widthTrue = $(this)[0].scrollWidth
    var width = $(this).width()
    var ww = (width * 100) / widthTrue

    $(scroll).css({
        'width': ww + '%',
    })
    var scrollWidth = $(scroll).width()
    var maxLeft = $(scrollWrap).width()-$(scroll).width()

    var procent =0// процент смещения скролла для фоток
    // клик передвижение...
    $(scroll).mousedown(function(event) {
        if (event.which != 1) {
            return false;
        }
        console.log(event)
        $(this).addClass('oneslide-scroll--down')
        var e = event
        var pageX = e.pageX
        var left = ($(this).css('left').replace('px', '')) * 1
        $('body').on('mousemove', $(scroll), function(event) {
            var x = event.pageX - pageX
            if ($(scroll).hasClass('oneslide-scroll--down')) {
            	//двигаем слайде
                x = x + left
                if (x < 0) {
                    x = 0
                }
                if (x > maxLeft) {
                    x = maxLeft
                }
                $(scroll).css('left', x)

                procent = (x*100)/maxLeft	
                
                //двигаем фотки
                var trueWidth = 

                $(papa)[0].scrollLeft = ( (procent*(widthTrue-$(papa).width()))/100 )      
            }
        })
    });
    $(document).mouseup(function(event) {
        $(scroll).removeClass('oneslide-scroll--down')
    })
}