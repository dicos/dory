#!-*-coding:utf-8-*-
import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, ListView, DeleteView, UpdateView,\
                                 DetailView
from django.core.urlresolvers import reverse

from catalog.models import Ad
from client.models import Client
from offers.forms import OfferForm
from offers.models import Offer
from catalog.settings import AD_TYPES
from offers.models import OFFER_STATUS, OfferMessage
from msg.models import PrivateMessage


class CreateOffer(CreateView):
    model = Offer
    form_class = OfferForm
    success_url = '/'

    def get(self, request, *args, **kwargs):
        """get offer popup"""
        ad_id = kwargs['ad_id']
        ad = get_object_or_404(Ad, pk=ad_id)
        if not self.request.user.is_authenticated():
            response = {'status': False, 'message': u'Авторизуйтесь для отправки предложения'}
        elif ad.client.user == request.user:
            response = {'status': False, 'message': u'Вы не можете послать предложение по своему объявлению'}
        else:
            form = OfferForm(initial={'ad': ad, 'client': Client.get_client(request)})
            my_ads = Ad.objects.filter(client__user=self.request.user)
            cat_ads = {ad. category for ad in my_ads}

            html = render_to_string('offers/popup_offer.html', {'form': form, 'my_ads': my_ads, 'cat_ads': cat_ads})
            response = {'status': True, 'html': html}
        return HttpResponse(json.dumps(response), 'json')

    def get_initial(self):
        return {'client': Client.get_client(self.request)}

    def form_invalid(self, form):
        return HttpResponse(json.dumps({'status': False, 'message': u'Ошибка'}), 'json')

    def form_valid(self, form):
        p = form.save()

        try:
            ad_url = self.request.build_absolute_uri(reverse('ad_view',
                                                             args=[p.ad.pk]))
            user_url = self.request.build_absolute_uri(reverse('profile',
                                                               args=[p.client.pk]))
            msg = PrivateMessage()
            msg.sender = p.client
            msg.recipient = p.ad.client
            msg.text = OfferMessage.objects.get(types=1)\
                                   .message.replace('%ads%', '<a href="%s">%s</a>' % (ad_url, ad_url))\
                                           .replace('%author%', '<a href="%s">%s</a>' % (user_url, p.client.get_pretty_name()))
            msg.save()
        except OfferMessage.DoesNotExist:
            pass

        return HttpResponse(json.dumps({'status': True, 'message': u'Предложение отправлено'}), 'json')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CreateOffer, self).dispatch(request, *args, **kwargs)


class DeleteOffer(DeleteView):
    model = Offer

    def post(self, request, *args, **kwargs):
        self.object = super(DeleteOffer, self).get_object()
        client = Client.get_client(request)
        if client and self.object in client.offers.all():
            self.object.is_active = False
            self.object.save()
            return HttpResponse(json.dumps({'status': True}), 'json')
        else:
            return HttpResponse(json.dumps({'status': False}), 'json')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteOffer, self).dispatch(request, *args, **kwargs)


class EditOffer(UpdateView):
    model = Offer
    form_class = OfferForm
    success_url = '/'
    context_object_name = 'offer'

    def get(self, request, *args, **kwargs):
        """get offer popup"""
        self.object = super(EditOffer, self).get_object()
        ad = self.object.ad
        if not self.request.user.is_authenticated():
            response = {'status': False, 'message': u'Авторизуйтесь для отправки предложения'}
        elif ad.client.user == request.user:
            response = {'status': False, 'message': u'Вы не можете редактировать это предложение'}
        else:
            form = self.get_form(self.get_form_class())
            html = render_to_string('offers/popup_edit_offer.html', {'form': form, 'offer': self.object, 'my_ads': Ad.objects.filter(client__user=self.request.user)})
            response = {'status': True, 'html': html}
        return HttpResponse(json.dumps(response), 'json')

    def form_invalid(self, form):
        return HttpResponse(json.dumps({'status': False, 'message': u'Ошибка', 'form': form}), 'json')

    def form_valid(self, form):
        form.save()
        self.object.status = 0
        self.object.save()
        return HttpResponse(json.dumps({'status': True, 'message': u'Предложение отредактировано'}), 'json')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(EditOffer, self).dispatch(request, *args, **kwargs)


class AdOffers(ListView):
    model = Offer
    template_name = 'offers/ad_offers.html'

    def get_context_data(self, **kwargs):
        context = super(AdOffers, self).get_context_data(**kwargs)
        context['ad'] = self.ad
        return context

    def get_queryset(self):
        ad_id = self.kwargs['ad_id']
        self.ad = get_object_or_404(Ad, pk=ad_id)
        client = Client.get_client(self.request)
        if not (client and self.ad in client.client_ads.all()):
            raise Http404
        return Offer.active_objects.filter(ad=self.ad)

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AdOffers, self).dispatch(request, *args, **kwargs)


class MyOffers(ListView):
    model = Offer
    template_name = 'offers/my_offers.html'
    context_object_name = 'offers'

    def get_queryset(self):
        client = Client.get_client(self.request)
        self.ad_type = self.kwargs.get('ad_type')
        result = client.offers.filter(is_active=True)
        if self.ad_type in [k for k,v in AD_TYPES]:
            result = result.filter(ad__type=self.ad_type)

        self.offer_state = self.kwargs.get('offer_state')
        if self.offer_state:
            if str(self.offer_state) in [str(k) for k,v in OFFER_STATUS]:
                result = result.filter(status=self.offer_state)

        return result

    def get_context_data(self, **kwargs):
        context = super(MyOffers, self).get_context_data(**kwargs)
        context['ad_type']=self.ad_type
        context['offer_state']=self.offer_state
        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(MyOffers, self).dispatch(request, *args, **kwargs)


class OfferView(DetailView):
    model = Offer
    template_name = 'offers/offer_view.html'
    context_object_name = 'offer'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(OfferView, self).dispatch(request, *args, **kwargs)


def apply_cancel_offer(request, offer_id):
    offer = get_object_or_404(Offer, pk=offer_id)
    client = Client.get_client(request)
    ad = Ad.objects.get(pk=offer.ad.pk)

    if client and offer.ad not in client.client_ads.all():
        return HttpResponse(json.dumps({'status': False, 'message': u'Вы не можете ответить на это предложение'}), 'json')

    status = request.GET.get('status', None)
    if status == 'apply':
        offer.status = 1
        if request.GET.get('action', '') == 'deactivate':
            ad.is_active = False
        ad.save()

        try:
            ad_url = request.build_absolute_uri(reverse('ad_view',
                                                        args=[offer.ad.pk]))
            user_url = request.build_absolute_uri(reverse('profile',
                                                          args=[offer.client.pk]))
            msg = PrivateMessage()
            msg.sender = offer.client
            msg.recipient = offer.ad.client
            msg.text = OfferMessage.objects.get(types=2)\
                                   .message.replace('%ads%', '<a href="%s">%s</a>' % (ad_url, ad_url))\
                                           .replace('%author%', '<a href="%s">%s</a>' % (user_url, client.get_pretty_name()))
            msg.save()
        except OfferMessage.DoesNotExist:
            pass

        msg = u'Вы приняли предложение'

    elif status == 'cancel':
        offer.status = 2
        offer.is_active = False
        msg = u'Вы отклонили предложение'
    else:
        offer.status = 0
        msg = u'Вы рассматриваете предложение'
    offer.save()
    return HttpResponse(json.dumps({'status': True, 'message': msg}), 'json')
