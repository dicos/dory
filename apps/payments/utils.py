#!-*-coding: utf-8-*-
import json
from django.core.urlresolvers import reverse
from django.http import HttpResponse

from client.models import Client
from payments.models import RefillOrder


def activate_ad(request, ad, confirmed=False):
    client = Client.get_client(request)
    if not ad.is_active and ad.client == client:
        if ad.get_cost() > client.client_balance.money:
            ad.is_active = False
            ad.save()
            refill_order = RefillOrder.objects.create(
                balance=client.client_balance,
                amount=ad.get_cost() - client.client_balance.money
            )
            robokassa_redirect_url = '{url}?refill_order_id={refill_order_id}'.format(url=reverse('redirect_robokassa'), refill_order_id=refill_order.id)
            return HttpResponse(json.dumps({'status': True, 'redirect_url': robokassa_redirect_url}), 'json')

        else:
            if not confirmed:
                pass
            result = client.client_balance.decrease(ad.get_cost())
            if result:
                #ad.ad_option_orders.filter(closed=False, paid=False).update(paid=True)
                ad.is_active = True
                ad.save()
                return HttpResponse(json.dumps({'status': True, 'url': reverse('ad_view', args=[ad.id]), 'message': u'Дополнительные услуги оплачены.'}), 'json')
            else:
                return HttpResponse(json.dumps({'status': False, 'message': u'Недостаточно средств на счете.'}), 'json')

    return HttpResponse(json.dumps({'status': False, 'message': u'Ошибка активации объявления'}), 'json')

