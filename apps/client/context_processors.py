# -*- coding:utf-8 -*-
from .models import Client


def client(request):
    return {'client': Client.get_client(request)}