from django.conf.urls import patterns, url
from feedbacks.views import FeedbackListView, CreateFeedback


urlpatterns = patterns('',
    url(r'^$', FeedbackListView.as_view(), name='feedbacks_all'),
    url(r'^(?P<pk>\d+)/', FeedbackListView.as_view(), name='feedbacks'),
    url(r'^create/$', CreateFeedback.as_view(), name='create_feedback'),
)