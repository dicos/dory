from django.contrib import admin
import models

class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('about','client', 'type','rating','date','is_active')
    list_filter = ('about','client', 'type',)

admin.site.register(models.Feedback, FeedbackAdmin)
admin.site.register(models.Answer)