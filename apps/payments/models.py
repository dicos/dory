# -*-coding:utf-8 -*-
from random import choice
from datetime import datetime
from django.db import models
from payments.settings import REFILL_ORDER_STATUSES, ORDER_IS_UNPAID, ORDER_IS_PAID


class Balance(models.Model):
    client = models.OneToOneField('client.Client', verbose_name=u'Кошелек', related_name='client_balance')
    number = models.CharField(u'Номер счета', max_length=100, default="".join([choice('ABCDEFGHIJKOPQRSTUVWXYZabcdefghijkopqrstuvwxyz1234567890') for _ in range(8)]))
    money = models.FloatField(u'Денег на счете', default=0)

    def __unicode__(self):
        return self.number

    def increase(self, amount):
        self.money += amount
        self.save()
        return True

    def decrease(self, amount):
        if amount <= self.money:
            self.money -= amount
            self.save()
            return True
        return False
    class Meta:
        verbose_name = u'баланс'
        verbose_name_plural = u'Балансы'


class Order(models.Model):
    balance = models.ForeignKey(Balance, verbose_name=u'Баланс', related_name='balance_%(class)s', null=True)
    cost = models.FloatField(u'Стоимость', default=0)
    number = models.CharField(u'Номер заказа', max_length=100, default="".join([choice('ABCDEFGHIJKOPQRSTUVWXYZabcdefghijkopqrstuvwxyz1234567890') for _ in range(8)]))
    date = models.DateTimeField(u'Дата', auto_now_add=True)

    paid = models.BooleanField(u'Оплачено', default=False)
    date_payment = models.DateTimeField(u'Дата оплаты', null=True, blank=True)

    closed = models.BooleanField(u'Закрыт', default=False)
    date_closing = models.DateTimeField(u'Дата закрытия', null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.paid and not self.date_payment:
            self.date_payment = datetime.now()
        if self.closed and not self.date_closing:
            self.date_closing = datetime.now()
        super(Order, self).save(*args, **kwargs)

    class Meta:
        abstract = True
        ordering = ['date']
        verbose_name = u'заказ'
        verbose_name_plural = u'Заказы'


#TODO наследовать от Order, мигрировать базу
# class AdOptionOrder1(Order):
#     ad = models.ForeignKey(Ad, verbose_name=u'Объявление', related_name='ad_option_orders')
#     ad_option = models.ForeignKey(AdOption, verbose_name=u'Опция', related_name='option_orders')

#
class RefillOrder(models.Model):
    """refill balance from payment system event"""
    balance = models.ForeignKey(Balance, verbose_name=u'Баланс', related_name='balance_refill_orders')
    number = models.CharField(u'Номер заказа', max_length=100, default="".join([choice('1234567890') for _ in range(8)]))
    amount = models.FloatField(u'Сумма пополнения', default=0)

    date = models.DateTimeField(u'Дата', auto_now_add=True)

    status = models.PositiveSmallIntegerField(u'Статус заказа', choices=REFILL_ORDER_STATUSES, default=ORDER_IS_UNPAID)
    confirmed = models.BooleanField(u'Оплата подтверждена', default=False)
    date_confirmed = models.DateTimeField(u'Дата подтверждения', null=True, blank=True)

    def __unicode__(self):
        return self.number

    def save(self, *args, **kwargs):
        if self.status == ORDER_IS_PAID:
            self.date_confirmed = datetime.now()
        super(RefillOrder, self).save(*args, **kwargs)

    def get_str_amount(self):
        try:
            return str(int(self.amount))
        except:
            return ''

#TODO наследовать от Order, мигрировать базу, поправить оплату
# class RefillOrder1(Order):
#     balance = models.ForeignKey(Balance, verbose_name=u'Баланс', related_name='balance_refill_orders')
#     status = models.PositiveSmallIntegerField(u'Статус заказа', choices=REFILL_ORDER_STATUSES, default=ORDER_IS_UNPAID)
#     amount = models.FloatField(u'Сумма пополнения', default=0)
#
#
#     def save(self, *args, **kwargs):
#         if self.status == ORDER_IS_PAID:
#             self.paid = True
#             self.date_payment = datetime.now()
#         elif self.status == ORDER_IS_UNPAID:
#             self.paid = False
#             self.date_payment = None
#             self.closed = True
#             self.date_closing = True
#         super(RefillOrder, self).save(*args, **kwargs)
#
#     def get_str_amount(self):
#         try:
#             return str(int(self.amount))
#         except:
#             return ''


class ShowPersonalInfoOrder(Order):
    """order for allow watch user info"""
    client = models.ForeignKey('client.Client', verbose_name=u'Клиент', related_name='client_personalinfo_orders')

    def __unicode__(self):
        return self.client
