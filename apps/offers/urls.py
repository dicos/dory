from django.conf.urls import patterns, include, url, static
from offers.views import CreateOffer, AdOffers, MyOffers, OfferView, EditOffer, DeleteOffer

urlpatterns = patterns('',
    # url(r'^get_offer_popup/(?P<ad_id>\d+)/', 'offers.views.get_offer_popup', name='get_offer_popup'),
    url(r'^get_popup/(?P<ad_id>\d+)?/', CreateOffer.as_view(), name='get_popup_offer'),
    url(r'^create/', CreateOffer.as_view(), name='create_offer'),
    url(r'^edit/(?P<pk>\d+)', EditOffer.as_view(), name='edit_offer'),
    url(r'^delete/(?P<pk>\d+)', DeleteOffer.as_view(), name='delete_offer'),
    url(r'^ad_offers/(?P<ad_id>\d+)?/', AdOffers.as_view(), name='ad_offers'),
    url(r'^my_offers/(?P<ad_type>\w+)?-(?P<offer_state>\w+)?/?', MyOffers.as_view(), name='my_offers'),
    url(r'^offer/(?P<pk>\d+)', OfferView.as_view(), name='offer_view'),
    url(r'^apply_cancel/(\d+)/', 'offers.views.apply_cancel_offer', name='apply_cancel_offer'),

)