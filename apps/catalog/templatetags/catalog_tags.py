#!-*-coding:utf-8-*-
from django.template import Library
from django.core.urlresolvers import resolve
from django.core.urlresolvers import reverse

from client.models import Client

register = Library()


@register.filter(name='get_value')
def get_value(dictionary, key):
    return dictionary.get(key)


@register.inclusion_tag('catalog/ad-filter-side.html', takes_context=True)
def ad_filter_side(context, for_only_ads=False):
    request = context['request']
    view = resolve(request.get_full_path())   
    current_ad_type = view.kwargs['ad_type']
    for k,v in view.kwargs.items():        
        if v is None:
            view.kwargs.pop(k)
    

    from catalog.settings import AD_TYPES, BIDS
    items = []
    if 'ad_type' in view.kwargs.keys():
        view.kwargs.pop('ad_type')
    items.append({'title': u'Все',
                  'url': reverse(view.url_name, args=view.args, kwargs=view.kwargs),
                  'current': current_ad_type==None,
                  'key': None})

    for k,v in AD_TYPES:
        view.kwargs['ad_type'] = k
        items.append({'title': v,
                      'url': reverse(view.url_name, args=view.args, kwargs=view.kwargs),
                      'current': k == current_ad_type,
                      'key': k})
    if for_only_ads:
        items = [i for i in items if (i['key'] in context['all_types'] and i['key'] in BIDS) or i['key'] is None]

    return {'ad_types': items}


@register.filter
def is_favorits(ad, client):
    if not client:
        return False
    return ad.adfavorits_set.filter(client=client).exists()