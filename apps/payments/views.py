#!-*-coding:utf-8-*-
import json
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.generic import ListView, TemplateView
from catalog.models import Ad
from client.models import Client

from payments.models import RefillOrder, Balance as BalanceModel
from robokassa.utils import get_redirect_url


class Balance(TemplateView):
    '''
    Тут раньше была история пополнения баланса для опций объявлений
    У нас встала задача удалить опции, соотвественно удалили их оплату
    Наверное тут надо будет сделать пополнение баланса
    '''
    template_name = 'payments/money.html'

    def get_context_data(self, **kwargs):
        ctx = super(Balance, self).get_context_data(**kwargs)

        ctx['balance'] = balance = get_object_or_404(BalanceModel, client=self.request.user.client)
        ctx['orders'] = self.request.user.client.client_personalinfo_orders.all()
        # ctx['orders'] = balance.balance_showpersonalinfoorder.all()
        # print balance.balance_showpersonalinfoorder.all()
        return ctx


    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(Balance, self).dispatch(request, *args, **kwargs)


@login_required()
def redirect_robokassa(request):
    if request.method == 'GET':
        if request.GET.get('refill_order_id', None):
            try:
                refill_order = RefillOrder.objects.get(pk=request.GET.get('refill_order_id', None))
            except:
                refill_order = None
        else:
            client = Client.get_client(request)
            balance = client.client_balance if client else None
            if balance:
                refill_order = RefillOrder.objects.create(balance=balance, amount=1000)
            else:
                refill_order = None

        return render_to_response('payments/redirect_robokassa.html', RequestContext(request, {'refill_order': refill_order}))

    elif request.method == 'POST':
        try:
            refill_order = RefillOrder.objects.get(pk=request.POST.get('refill_order_id', None))
        except:
            client = Client.get_client(request)
            balance = client.client_balance if client else None
            if not balance:
                raise Http404
            refill_order = RefillOrder(balance=balance)

        amount = request.POST.get('amount', None)
        if amount:
            try:
                amount = float(amount)
            except:
                return render_to_response('payments/redirect_robokassa.html', RequestContext(request, {'refill_order': refill_order, 'error': u'Введите корректную сумму'}))

            refill_order.amount = amount
            refill_order.save()
        elif not amount and not refill_order.amount:
            error = u'Введите сумму для пополнения'
            return render_to_response('payments/redirect_robokassa.html', RequestContext(request, locals()))
        print get_redirect_url(refill_order)
        return redirect(get_redirect_url(refill_order))

    return HttpResponse(status=404)


def payment_message_inline(request):
    try:
        ad = Ad.objects.get(pk=request.GET['ad_id'])
    except:
        return HttpResponse(json.dumps({'status': False, 'msg': u'Клиент не найден'}))

    cost = ad.get_cost()
    balance = ad.client.client_balance

    if balance.money < cost:
        refill_amount = cost - balance.money
        html = render_to_string('payments/inline_refill_needed.html', locals())
        return HttpResponse(json.dumps({'status': True, 'html': html}))

    elif balance.money >= cost:
        html = render_to_string('payments/inline_can_paid.html', locals())
        return HttpResponse(json.dumps({'status': True, 'html': html}))
