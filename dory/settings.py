import os
import sys
#import djcelery
#djcelery.setup_loader()

#CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
#CELERY_ALWAYS_EAGER = False
BROKER_BACKEND = "djkombu.transport.DatabaseTransport"

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Adding modules directory to python path
sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))

SECRET_KEY = '0gfr_12_ob2)$y7c+f0kc-4h)mktu)o(5uxm&1erclf+n#@_a#'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'dory.sqlite',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'POSRT': '',
    },
    # '_default': {
    #     'ENGINE': 'django.db.backends.mysql',
    #     'NAME': 'dory',
    #     'USER': 'dory_db_user',
    #     'PASSWORD': 'FJvj5uxu'
    # }
}

DEBUG = TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = (
    (os.path.join(BASE_DIR, 'apps/content/static')),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'

LOGIN_URL = '/'

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'content',
    'catalog',
    'client',
    'offers',
    'msg',
    'feedbacks',
    'payments',
    'robokassa',
    'dory',


    'south',
    'sorl.thumbnail',
    'django_evolution',
    'django_geoip',
    'smart_selects',
    #'djcelery',
    #'djkombu',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_geoip.middleware.LocationMiddleware',
)

ROOT_URLCONF = 'dory.urls'

WSGI_APPLICATION = 'dory.wsgi.application'

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.load_template_source',
)

TEMPLATE_DIRS = [
    os.path.join(os.path.dirname(BASE_DIR), 'dory', 'templates'),
]

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',

    'catalog.context_processors.info',
    'client.context_processors.client',
)

TINYMCE_DEFAULT_CONFIG = {
    'theme': "simple",
    'relative_urls': False
}

THUMBNAIL_PRESERVE_FORMAT=True

DAYS_FOR_EXPIRE = 3

#ROBOKASSA
ROBOKASSA_LOGIN = 'dorydory'
ROBOKASSA_PASSWORD1 = 'Qwerty123'
ROBOKASSA_PASSWORD2 = 'Qwerty1231'
TEST_MODE = True

# django_geoip
IPGEOBASE_ALLOWED_COUNTRIES = ['RU']

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_USER = 'dory@dicos.ru'
EMAIL_HOST_PASSWORD = 'tmazqtfnnjxqeywr'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

try:
    from local_settings import *
except ImportError:
    pass

THUMBNAIL_COLORSPACE = None
THUMBNAIL_PRESERVE_FORMAT = True
