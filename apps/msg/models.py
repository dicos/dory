#!-*-coding:utf-8-*-

from django.db import models
from catalog.models import Ad
from client.models import Client


class AdMessage(models.Model):
    ad = models.ForeignKey(Ad, verbose_name=u'Объявление', related_name='ad_messages')
    parent = models.ForeignKey('AdMessage', verbose_name=u'Родительское сообщение', related_name='ad_message_children', blank=True, null=True)
    client = models.ForeignKey('client.Client', verbose_name=u'Пользователь', related_name='client_ad_messages')
    date = models.DateTimeField(u'Дата', auto_now_add=True)
    text = models.TextField(u'Сообщение', blank=True, null=True)

    is_active = models.BooleanField(u'Активно', default=True)

    def __unicode__(self):
        return self.text or str(self.id)


class MailAdMessage(models.Model):
    ad = models.ForeignKey(Ad, verbose_name=u'Объявление', related_name='ad_mail_messages')
    email = models.EmailField(u'Ваш email')
    date = models.DateTimeField(u'Дата', auto_now_add=True)
    text = models.TextField(u'Сообщение', blank=True, null=True)

    send_to_me = models.BooleanField(u'Отправить копию', default=False)

    def __unicode__(self):
        return self.text or str(self.id)


class PrivateMessage(models.Model):
    sender = models.ForeignKey(Client, verbose_name=u'Отправитель', related_name='client_sended_messages')
    recipient = models.ForeignKey(Client, verbose_name=u'Получатель', related_name='client_received_messages')
    date = models.DateTimeField(u'Дата', auto_now_add=True)
    text = models.TextField(u'Сообщение', blank=True, null=True)

    is_readed = models.BooleanField(u'Прочтено', default=False)

    is_active = models.BooleanField(u'Активно', default=True)

    def __unicode__(self):
        return self.text or str(self.id)

    def get_sender_name(self):
        return self.sender.get_pretty_name()

    def get_recipient_name(self):
        return self.get_pretty_name()

    class Meta:
        ordering = ['-date']

