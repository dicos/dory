#! -*-coding:utf-8 -*-

SELL = 'AD'
BUY = 'ORDER'
RENT_YOU = 'RENT_YOU'
RENT_ME = 'RENT_ME'
GIVE = 'GIVE'
ACCEP_GIFT = 'ACCEP_GIFT'
OFFER_SERVICE = 'OFFER_SERVICE'
USE_SERVICE = 'USE_SERVICE'

AD_TYPES = [
    (SELL, u'Продам'),
    (BUY, u'Куплю'),
    (RENT_YOU, u'Сдам в аренду'),
    (RENT_ME, u'Арендую'),
    (GIVE, u'Отдам даром'),
    (ACCEP_GIFT, u'Приму в дар'),
    (OFFER_SERVICE, u'Предлагаю услуги'),
    (USE_SERVICE, u'Воспользуюсь услугами'),
]

BIDS = (BUY, RENT_ME, ACCEP_GIFT, USE_SERVICE)
ADS = (SELL, RENT_YOU, GIVE, OFFER_SERVICE)

MAX_OFFERS_IN_STRIP = 15

REQUIRED_LOGIN = (BUY, RENT_ME, USE_SERVICE,)