from django.contrib import admin
import models


class ChildrenParameterInlineCat(admin.TabularInline):
    model = models.Category.parameters.through


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'parent', 'is_active', 'index',)
    exclude = ('parameters', )
    inlines = [ChildrenParameterInlineCat]
    list_filter = ('parent',)


class ParameterGroupAdmin(admin.ModelAdmin):
    filter_horizontal = ('parameters',)


class ChildrenParameterInlineParam(admin.TabularInline):
    model = models.Parameter.child_groups.through


class ParameterAdmin(admin.ModelAdmin):
    # exclude = ('child_groups',)
    inlines = [ChildrenParameterInlineParam]
    filter_horizontal = ('child_groups',)


class GoodParamsInline(admin.TabularInline):
    model = models.AdParams


class GoodPhoneInline(admin.StackedInline):
    model = models.AdPhone


class GoodPhotoInline(admin.StackedInline):
    model = models.AdPhoto


class AdAmin(admin.ModelAdmin):
    list_display = ('title', 'type', 'category', 'client', 'is_active', 'city')
    list_editable = ('is_active',)
    list_filter = ('category', 'type', 'client',)
    exclude = ()
    inlines = [GoodParamsInline, GoodPhoneInline,
               GoodPhotoInline]


class GoodKindAdmin(admin.ModelAdmin):
    pass


class AdMessageAdmin(admin.ModelAdmin):
    pass


class AdFavoritsAdmin(admin.ModelAdmin):
    list_display = ('ad', 'client',)
    list_filter = ('client',)


admin.site.register(models.AdFavorits, AdFavoritsAdmin)
admin.site.register(models.Ad, AdAmin)
admin.site.register(models.Parameter, ParameterAdmin)
admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.Parameter_group, ParameterGroupAdmin)
