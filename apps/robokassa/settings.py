#-* coding: UTF-8 *-

from dory import settings

LOGIN = settings.ROBOKASSA_LOGIN
PASSWORD1 = settings.ROBOKASSA_PASSWORD1
PASSWORD2 = getattr(settings, 'ROBOKASSA_PASSWORD2', None)

TEST_MODE = getattr(settings, 'ROBOKASSA_TEST_MODE', True)

FORM_TARGET = u'http://test.robokassa.ru/Index.aspx' if TEST_MODE else u'https://merchant.roboxchange.com/Index.aspx'


