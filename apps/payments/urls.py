from django.conf.urls import patterns, include, url, static
from payments.views import Balance

urlpatterns = patterns('',
    url(r'^$', Balance.as_view(), name='balance'),
    url(r'^redirect_robokassa/$', 'payments.views.redirect_robokassa', name='redirect_robokassa'),
    url(r'^payment_message_inline/$', 'payments.views.payment_message_inline', name='payment_message_inline'),
    url(r'^result/$', 'robokassa.views.result_view', name='robokassa_result'),
    url(r'^success/$', 'robokassa.views.success_view', name='robokassa_success'),
    url(r'^fail/$', 'robokassa.views.fail_view', name='robokassa_fail_view'),
)