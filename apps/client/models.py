# -*-coding:utf-8 -*-
from random import choice
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.contrib import auth
from catalog.models import Category, Ad
from catalog.settings import SELL, BUY, BIDS
from feedbacks.models import Feedback
from payments.models import ShowPersonalInfoOrder, Balance
from django_geoip.models import City, Region
from smart_selects.db_fields import ChainedForeignKey

User._meta.get_field('email')._unique = True
User._meta.get_field('email').blank = False
User._meta.get_field('first_name').blank = False
User._meta.get_field('last_name').blank = True


class Client(models.Model):
    REGISTRATION = 0
    ACTIVE = 1

    PRIVATE = 1
    LEGAL = 2
    user = models.OneToOneField(User, related_name='client')
    region = models.ForeignKey(Region, verbose_name=u'Регион', blank=True, null=True)
    city = ChainedForeignKey(City, verbose_name=u'Город', chained_field="region", chained_model_field="region", null=True, blank=True)
    raw_password = models.CharField(max_length=10, default='', blank=True)
    img = models.ImageField(verbose_name=u'Фото профиля', upload_to='profiles', blank=True, null=True)
    type = models.SmallIntegerField(choices=[(PRIVATE, u'Частное лицо'), (LEGAL, u'Юридическое лицо')], blank=True, null=True)
    interests = models.ManyToManyField(Category, verbose_name=u'Сферы деятельности', blank=True, null=True)
    status = models.PositiveSmallIntegerField(u'Статус', default=REGISTRATION)

    @classmethod
    def get_client(cls, request):
        try:
            return Client.objects.get(user=request.user)
        except Exception:
            return

    def save(self, *args, **kwargs):
        self.user.email = str(self.user.email).lower()
        super(Client, self).save(*args, **kwargs)

    @classmethod
    def create_raw_user(cls, e_mail):
        User.objects.filter(username__iexact=e_mail, is_active=False).delete()
        try:
            u = User.objects.get(username__iexact=e_mail)
            if not u.is_active:
                u.delete()
            return None
        except User.DoesNotExist:
            pass
        new_pass = "".join(choice("1234567890") for i in xrange(5))
        new_user = User(username=e_mail.lower(), email=e_mail.lower(), is_active=False)
        new_user.set_password(new_pass)
        new_user.save()
        new_client = Client()
        new_client.user = new_user
        new_client.raw_password = new_pass
        new_client.save()
        Balance.objects.create(client=new_client)
        return new_client

    @classmethod
    def delete_nonactive_user(cls):
        clients = cls.objects.exclude(token="")
        for client in clients:
            if not client.is_not_confirmed():
                user = client.user
                client.delete()
                user.delete()

    @classmethod
    def authenticate_user(cls, request):
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = auth.authenticate(username=email, password=password)
        if user and user.is_active and user.client.status == user.client.ACTIVE:
            auth.login(request, user)
            return True
        else:
            return False

    def is_not_confirmed(self):
        dif = datetime.now() - self.user.date_joined.replace(tzinfo=None)
        if dif.days >= 1 and not self.user.is_active:
            return True
        else:
            return False

    def is_person(self):
        return self.type == 1

    def is_organization(self):
        return self.type == 2

    def get_pretty_name(self):
        if self.is_person():
            if self.user.first_name and self.user.last_name:
                return '%s %s' % (self.user.first_name, self.user.last_name)
            elif self.user.first_name:
                return '%s' % self.user.first_name
        elif self.is_organization():
            if self.client_organization_info.first() and self.client_organization_info.first().short_name:
                return self.client_organization_info.first().short_name
        return u'Пользователь'

    def get_feedbacks(self):
        return self.client_about_feedbacks.order_by('-date')

    def get_rating(self):
        rating = 0

        '''
        Расчёт рейтинга по оценке в отзыве
        1 - очень плохо, икс = -2
        2 - плохо, икс = -1
        3 - нормально, икс = 1
        4 - хорошо, иск = 2
        5 - отлично, икс = 3
        '''
        for x in Feedback.objects.filter(about=self):
            if x.rating == 1:
                rating -= 2
            if x.rating == 2:
                rating -= 1
            if x.rating == 3:
                rating += 1
            if x.rating == 4:
                rating += 2
            if x.rating == 5:
                rating += 3

        '''
        Расчёт рейтинга по покупкам контактов
        за каждую покупку контактов +0.1 к рейтингу
        '''
        for x in PurchaseContact.objects.filter(who=self):
            rating += 0.1

        '''
        Расчёт рейтинга за просмотр объявления
        +0.01 балла за просмотр объявления
        '''
        for x in self.client_ads.all():
            c = 0
            while c < x.views:
                rating += 0.01
                c += 1

        return int(round(rating))

    def get_strip(self):
        return Ad.objects.exclude(client=self, client_declines=self) \
                         .filter(category__in=self.interests.all(), type__in=BIDS, is_shown=True)

    def has_sell_ads(self):
        return bool(self.client_ads.filter(type=SELL).count())

    def has_buy_ads(self):
        return bool(self.client_ads.filter(type=BUY).count())

    def check_can_see_profile(self, client, profile_client):
        return PurchaseContact.objects.filter(who=client, client=profile_client).exists()

    def __unicode__(self):
        return u'%s %s <--> %s' % (self.user.first_name, self.user.last_name, self.user.username)

    class Meta:
        verbose_name = u'клиент'
        verbose_name_plural = u'Клиенты'


class Phone(models.Model):
    client = models.ForeignKey(Client, related_name='client_phone_numbers')
    number = models.CharField(u'Номер телефона', max_length=50, blank=True)

    def __unicode__(self):
        return self.number

    class Meta:
        verbose_name = u'телефон'
        verbose_name_plural = u'Телефоны'


class OrganizationInfo(models.Model):
    client = models.ForeignKey(Client, related_name='client_organization_info')
    name = models.CharField(u'Наименование', max_length=255)
    short_name = models.CharField(u'Краткое наименование', max_length=255)
    address = models.CharField(u'Адрес', max_length=255, blank=True, null=True)
    description = models.TextField(u'Об организации', blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = u'Информация об организации'


class PersonalInfo(models.Model):
    client = models.ForeignKey(Client, related_name='client_personal_info')
    description = models.TextField(u'О пользователе', null=True, blank=True)

    def __unicode__(self):
        return self.description

    class Meta:
        verbose_name_plural = u'Персональная информация'


class PurchaseContact(models.Model):
    '''
    При нажатии кнопки «отправка предложения» на объявлении,
    контакты которого ещё не куплены, должно вылезти
    окно с предложением оплатить
    '''
    who = models.ForeignKey(Client, verbose_name=u'Кто купил', related_name='who')
    client = models.ForeignKey(Client, verbose_name=u'Чей контакт куплен')


# class Balance(models.Model):
#     client = models.OneToOneField(Client, verbose_name=u'Клиент', related_name='balance')
#     balance = models.FloatField(u'Баланс', default=0)
#     bonus = models.FloatField(u'Бонусы', default=0)
#
#     def add_money(self, amount):
#         if amount > 0:
#             self.balance += amount
#             return True
#
#     def sub_money(self, amount):
#         if amount >= self.balance:
#             self.balance -= amount
#             return True
#
#     def __unicode__(self):
#         return '%s - %d (%d)' % (self.client.user.username, self.balance, self.bonus)

