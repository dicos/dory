#!-*-coding:utf-8-*-
from django import forms
from django.core.exceptions import ValidationError
from django.forms.widgets import TextInput

from catalog.models import Ad, AdPhoto, AdTerms
from django_geoip.models import IpRange, City, Region
from tinymce.widgets import TinyMCE


class NumberInput(TextInput):
    input_type = 'number'


class AdForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(AdForm, self).__init__(*args, **kwargs)
        self.fields['region'].queryset = Region.objects.order_by('name')

        if kwargs['initial']['client'].region and kwargs['initial']['client'].city:
            region = kwargs['initial']['client'].region
            city = kwargs['initial']['client'].city
        else:
            try:
                geoip_record = IpRange.objects.by_ip(kwargs['initial']['ip'])
                region = geoip_record.region
                city = geoip_record.city
            except IpRange.DoesNotExist:
                region = Region.objects.get(pk=83)
                city = City.objects.get(pk=2097)

        self.fields['region'].initial = region
        self.fields['city'].initial = city

    class Meta:
        model = Ad
        exclude = ('days_for_expire', 'priority', 'is_active', 'is_deleted', 'category',)
        widgets = {
            'client': forms.HiddenInput(),
            'title': forms.TextInput(attrs={'class': 'i-input input'}),
            'price': forms.TextInput(attrs={'class': 'i-input input input--short'}),
            'text': TinyMCE(attrs={'class': 'i-textarea textarea'}),
            'phone': forms.TextInput(attrs={'class': 'i-input input'}),
            'options': forms.CheckboxSelectMultiple(),
        }


class AdImageForm(forms.ModelForm):

    class Meta:
        model = AdPhoto
        widgets = {
            'ad': forms.HiddenInput(attrs={'class': 'good_for_image'}),
            'img': forms.FileInput(attrs={'class': 'upload-input', 'accept': 'image/*', 'multiple': 'true'}),
            'index': forms.HiddenInput()
        }


class AdTermsForm(forms.ModelForm):
    class Meta:
        model = AdTerms
        widgets = {
            'ad': forms.HiddenInput()
        }
        exclude = ['category',]