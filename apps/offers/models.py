# -*-coding:utf-8 -*-
from django.db import models
from catalog.models import Ad


OFFER_STATUS = [
    (0, u'На рассмотрении'),
    (1, u'Принято'),
    (2, u'Отказано'),
]


class ActiveManager(models.Manager):
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(is_active=True)


class Offer(models.Model):
    ad = models.ForeignKey(Ad, verbose_name=u'Объявление', related_name='ad_offers')
    client = models.ForeignKey('client.Client', verbose_name=u'Отправитель', related_name='offers')
    type = models.CharField(u'Тип предложения', max_length=20, choices=[('SELL_OFFER', u'Предложения на объявление о продаже/сдаче'), ('BUY_OFFER', u'Предложение на объявление о покупке/съеме')], blank=True, null=True)
    price = models.FloatField(u'Цена', default=0)
    title = models.CharField(u'Заголовок', max_length=100, blank=True, null=True)
    message = models.TextField(u'Сообщение', blank=True, null=True)
    ad_offers = models.ManyToManyField(Ad, verbose_name=u'Объявление в качестве предложения', related_name='ad_as_offers', blank=True, null=True)
    date = models.DateTimeField(u'Дата', auto_now_add=True, blank=True, null=True)
    is_active = models.BooleanField(u'Активно', default=True)
    status = models.SmallIntegerField(u'Статус', max_length=100, choices=OFFER_STATUS, default=0)

    active_objects = ActiveManager()

    class Meta:
        verbose_name = u'Предложение'
        verbose_name_plural = u'Предложения'

    def __unicode__(self):
        return str(self.pk)

    def get_status(self):
        return [v for k, v in OFFER_STATUS if k == self.status][0]


class OfferMessage(models.Model):
    TYPES = (
        (1, u'Когда кто-то ответил на объявление предложением'),
        (2, u'После принятия предложения'),
    )
    types = models.SmallIntegerField(u'Тип', choices=TYPES, unique=True)
    message = models.TextField(u'Сообщение', help_text=u'Для вставки ссылки на объявление напишите %ads%. Для вставки ссылки на автора напишите %author%')

    class Meta:
        verbose_name = u'Сообщение'
        verbose_name_plural = u'Шаблоны сообщений предложений'

    def __unicode__(self):
        return self.message
