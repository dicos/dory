#!-*-coding:utf-8-*-

from django import forms
from catalog.models import Ad
from client.models import Client
from offers.models import Offer


class OfferForm(forms.ModelForm):

    class Meta:
        model = Offer
        exclude = ('is_active', 'status', 'date', 'title')
        widgets = {
            'ad': forms.HiddenInput(),
            'client': forms.HiddenInput(),
            'type': forms.HiddenInput(),
            'price': forms.TextInput(attrs={'class': 'input input--short'})
        }