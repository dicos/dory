from django.contrib import admin

import models


class OfferAdmin(admin.ModelAdmin):
    list_display = ('title', 'type', 'ad', 'client',
                    'date', 'is_active', 'status')
    list_display_links = ('title', 'type', 'ad', 'client',
                          'date', 'is_active', 'status')
    list_filter = ('type', 'client', 'date', 'is_active', 'status')


admin.site.register(models.Offer, OfferAdmin)
admin.site.register(models.OfferMessage)
