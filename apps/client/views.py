# -*- coding:utf-8 -*-
import mimetypes
import os
import random

from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages
from django.core.files import File
from django.core.urlresolvers import reverse
from django.conf import settings
from django.views.decorators.http import require_POST
from django.template.loader import render_to_string
from django.views.generic import DetailView, ListView
from django.contrib.auth import login as auth_login

from client.forms import EditClient, PersonalInfoForm, PhoneForm,\
                         OrganizationInfoForm
from catalog.models import Category
from dory.settings import MEDIA_ROOT
from feedbacks.forms import FeedbackForm
from forms import ResetPasswordForm, CheckPasswordForm
from django.shortcuts import *
from models import *
import json
from msg.forms import PrivateMessageForm
from .utils import get_city
from django.core.mail import send_mail


@require_POST
def sign_up(request):
    client = Client.create_raw_user(request.POST.get('email'))
    if client is not None:
        message = render_to_string('mail/e_mail.html', {'client': client, 'request': request})
        client_email = str(client).split()[1]
        send_mail('Запрос на регистрацию', message, settings.DEFAULT_FROM_EMAIL,
                  [ '\"{}\"'.format(client_email) ,],
                  fail_silently=False)
        messages.info(request, u'Для продолжения регистрации перейдтие по ссылке, которая отправлена на указанную почту')
        request.session['new_client_id'] = client.pk
        return HttpResponse(json.dumps({'link': reverse('registration')}))
    else:
        return HttpResponse(json.dumps({'message': u'Пользователь с таким адресом уже существует'}))


@require_POST
def login(request):
    form = AuthenticationForm(data=request.POST)
    if form.is_valid():
        auth.login(request, form.get_user())
        return HttpResponse(json.dumps({'ok': True, 'message': u'Авторизация успешно пройдена'}),
                            mimetype='application/json')
    else:
        return HttpResponse(json.dumps({'ok': False, 'message': u'Логин или пароль введены неверно'}),
                            mimetype='application/json')


# @require_POST
def reset_password(request):
    email = request.POST.get('email', None)
    if email:
        try:
            client = Client.objects.get(user__username=email)
        except Client.DoesNotExist:
            return HttpResponse(
                json.dumps({'ok': False, 'message': u'Пользователя с таким E-mail не зарегистрировано'}),
                mimetype='application/json')
        if not client.raw_password:
            client.raw_password = "".join(choice("1234567890") for i in xrange(5))
            client.user.set_password(client.raw_password)
            client.user.save()
            client.save()
        message = render_to_string('mail/e_mail_reset_password.html', {'client': client, 'request': request})
        try:
            # client.user.email_user(subject=u'Запрошен сброс пароля', message=message)
            send_mail('Восстановление пароля', message, settings.DEFAULT_FROM_EMAIL, [ '\"{}\"'.format(email) ,], fail_silently=False)
        except Exception as e:
            return HttpResponse(json.dumps({'ok': False, 'message': u'Не удалось отправить сообщение. Попробуйте позже.'}))
        return HttpResponse(json.dumps(
            {'ok': True, 'message': u'На указанный почтовый ящик выслано письмо с инструкцией по сбросу пароля'}))
    return HttpResponse(json.dumps({'ok': False, 'message': u'Укажите e-mail'}))


# @login_required()
def logout(request):
    auth.logout(request)
    # if request.GET:
    #     return HttpResponseRedirect(request.GET.get('back'))
    # else:
    return HttpResponseRedirect('/')


def registration(request):
    if request.user.is_anonymous():
        if request.session.get('new_client_id'):
            request.user = get_object_or_404(Client, pk=request.session['new_client_id']).user
        else:
            return redirect("%s?next=%s" % (reverse('login'), reverse('account')))
    else:
        redirect('account')
    form = CheckPasswordForm(request.POST or None, instance=request.user.client)
    if request.POST and form.is_valid():
        client = form.save()
        user = client.user
        user.is_active = True
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        auth_login(request, user)
        messages.info(request, u'Код подтвержден')
        return HttpResponseRedirect(reverse('account'))
    return render(request, 'reg.html', {'form': form, 'is_registration': True})


def reset_password_confirm(request):
    if request.POST:
        form = ResetPasswordForm(request.POST)
        if form.is_valid():
            form.set_password()
            return HttpResponseRedirect('/')
        return render(request, 'reset_password_confirm.html', locals())

    elif request.GET:
        token = request.GET.get('token', None)

        if token:
            client = get_object_or_404(Client, token=token)
            form = ResetPasswordForm(initial={'token': token})
            return render(request, 'reset_password_confirm.html', locals())
        else:
            raise Http404
    return HttpResponseRedirect('/')


def handle_uploaded_file(f):
    destination = open('images', 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()


class AccountProfile(DetailView):
    model = Client
    template_name = 'user-profile.html'
    context_object_name = 'account'

    def get_context_data(self, **kwargs):
        client = Client.get_client(self.request)
        context = super(AccountProfile, self).get_context_data(**kwargs)
        context['msg_form'] = PrivateMessageForm(initial={'sender': client, 'recipient': self.get_object()})
        context['feedback_form'] = FeedbackForm(initial={'client': client, 'about': self.get_object()})
        context['feedback_exists'] = Feedback.objects.filter(about=self.object, client=client).exists()
        if client:
            context['interests']= client.interests.all()
        return context


@login_required()
def client_account_view(request, template_name='client/registration.html'):
    client = Client.get_client(request)
    categories = Category.objects.filter(parent__isnull=True)
    personal_info_form = PersonalInfoForm(instance=(client.client_personal_info.first() if client else None),
                                          initial={'client': client})
    organization_info_form = OrganizationInfoForm(instance=(client.client_organization_info.first() if client else None),
                                                  initial={'client': client})

    if request.method == 'GET':
        if not client:
            raise Http404
        client_form = EditClient(
            instance=client,
            initial={
                'region': client.region,
                'city': client.city,
                'first_name': client.user.first_name,
                'last_name': client.user.last_name,
                'email': client.user.email,
                'user': client.user,
                'interests': client.interests.all()
            }
        )
        phone_form = PhoneForm(instance=client.client_phone_numbers.first(), initial={'client': client})

    if request.method == 'POST':
        client_form = EditClient(request.POST, instance=client)
        phone_form = PhoneForm(request.POST, instance=client.client_phone_numbers.first())
        personal_info_form = PersonalInfoForm(request.POST, request.FILES, instance=client.client_personal_info.first())
        organization_info_form = OrganizationInfoForm(request.POST, request.FILES,
                                                      instance=client.client_organization_info.first())

        if client_form.is_valid():
            client = client_form.save(commit=False)
            client.status = Client.ACTIVE
            if client.is_person() and personal_info_form.is_valid():
                personal_info_form.save()
            elif not client.is_person() and organization_info_form.is_valid():
                organization_info_form.save()

            client.save()
            client_form.save_m2m()
            if 'new_client_id' in request.session:
                del request.session['new_client_id']
                client.user.backend = 'django.contrib.auth.backends.ModelBackend'
                auth_login(request, client.user)
            if phone_form.is_valid():
                if phone_form.cleaned_data.get("number"):
                    phone_form.save()
                else:
                    client.client_phone_numbers.all().delete()
                return redirect(".")
            elif client.type == Client.PRIVATE:
                return redirect(".")

    c = {
        'client': client,
        'client_form': client_form,
        'personal_info_form': personal_info_form,
        'organization_info_form': organization_info_form,
        'phone_form': phone_form,
        'is_personal': client.is_person(),
        'categories': categories,
        'is_registration': client.status == Client.REGISTRATION,
    }

    return render_to_response(template_name, RequestContext(request, c))


@require_POST
@login_required()
def upload_client_image(request):
    client = Client.get_client(request)
    if client:
        #savefile
        file = request.FILES['files[]']
        img_name = 'image_%s.jpg' % "".join(
                [random.choice('ABCDEFGHIJKOPQRSTUVWXYZabcdefghijkopqrstuvwxyz1234567890') for _ in range(8)])
        destination = open(os.path.join(MEDIA_ROOT, 'upload', img_name), 'wb+')
        for chunk in file.chunks():
            destination.write(chunk)
        destination.close()

        client.img.save(img_name, File(open(os.path.join(MEDIA_ROOT, 'upload', img_name))))
        client.save()
        files = [{
             'url': client.img.url,
             'name': client.img.name,
             'type': mimetypes.guess_type(client.img.path)[0] or 'image/png',
             'thumbnailUrl': client.img.url,
             'size': client.img.size,
             # 'deleteUrl': reverse('/catalog/upload_delete/%d/' % image.pk),
             'deleteType': 'DELETE',
         }]
        response = json.dumps({'files': files})
        return HttpResponse(response, content_type='application/json')




class PartnerListView(ListView):
    model = Client
    context_object_name = 'partners'
    template_name = 'client/partners.html'

    def get_queryset(self):
        client = Client.get_client(self.request)
        city = get_city(self.request)
        clients = Client.objects.filter(type='2', region_id=city.region_id)
        return clients

@login_required()
def client_account_rating_view(request, template_name='account/rating.html'):

    client = Client.get_client(request)
    categories = Category.objects.filter(parent__isnull=True)

    if request.method == 'GET':
        if not client:
            raise Http404
        client_form = EditClient(
            instance=client,
            initial={
                'city': client.city,
                'first_name': client.user.first_name,
                'last_name': client.user.last_name,
                'email': client.user.email,
                'user': client.user,
                'interests': client.interests.all()
            }
        )
        phone_form = PhoneForm(instance=client.client_phone_numbers.first(), initial={'client': client})
        personal_info_form = PersonalInfoForm(instance=client.client_personal_info.first(), initial={'client': client})
        organization_info_form = OrganizationInfoForm(instance=client.client_organization_info.first(), initial={'client': client})

    if request.method == 'POST':
        client_form = EditClient(request.POST, instance=client)
        phone_form = PhoneForm(request.POST, instance=client.client_phone_numbers.first())

        if client_form.is_valid() and phone_form.is_valid():
            client = client_form.save(commit=False)
            personal_info_form = PersonalInfoForm(request.POST, request.FILES, instance=client.client_personal_info.first())
            organization_info_form = OrganizationInfoForm(request.POST, request.FILES, instance=client.client_organization_info.first())
            if client.is_person() and personal_info_form.is_valid():
                personal_info_form.save()
            elif not client.is_person() and organization_info_form.is_valid():
                organization_info_form.save()

            client_form.save()
            phone_form.save()

    c = {
        'client': client,
        'client_form': client_form,
        'personal_info_form': personal_info_form,
        'organization_info_form': organization_info_form,
        'phone_form': phone_form,
        'is_personal': client.is_person(),
        'categories': categories
    }

    return render_to_response(template_name, RequestContext(request, c))
