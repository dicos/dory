from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, render_to_response
from catalog.models import Category


def test(request, template_name):
    return render(request, template_name + '.html', locals())


def index(request):
    categories = Category.objects.filter(is_active=True, parent__isnull=True)
    return render_to_response('content/index.html', locals())
