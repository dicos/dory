#-*- coding:utf-8 -*-
from itertools import chain
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.forms.util import flatatt
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.shortcuts import get_object_or_404
from django.forms.forms import NON_FIELD_ERRORS
from django.forms.util import ErrorDict
from catalog.models import Category
from client.models import PersonalInfo
from models import Client, OrganizationInfo
from models import Phone, PurchaseContact
from django_geoip.models import City, Region, IpRange
from smart_selects.form_fields import ChainedModelChoiceField
from smart_selects.widgets import ChainedSelect


class ImprovedFormMixin(object):
    "Default office form."

    def __init__(self, *args, **kwargs):
        super(ImprovedFormMixin, self).__init__(*args, **kwargs)

        for field_name, field_object in self.fields.iteritems():
            if isinstance(field_object, forms.CharField):
                def get_clean_func(original_clean):
                    return lambda value: original_clean(value and value.strip())

                clean_func = get_clean_func(getattr(field_object, 'clean'))
                setattr(field_object, 'clean', clean_func)

        if hasattr(self, 'placeholders') and self.placeholders:
            for field_name, field in self.fields.items():
                if field_name in self.placeholders:
                    field.widget.attrs.update({'placeholder': self.placeholders[field_name]})

        if hasattr(self, 'css_classes') and self.css_classes:
            for field_name, field in self.fields.items():
                if field_name in self.css_classes:
                    field.widget.attrs.update({'class': self.css_classes[field_name]})

        if hasattr(self, 'help_texts') and self.help_texts:
            for field_name, field in self.fields.items():
                if field_name in self.help_texts:
                    field.help_text = self.help_texts[field_name]

        if hasattr(self, 'labels') and self.labels:
            for field_name, field in self.fields.items():
                if field_name in self.labels:
                    field.label = self.labels[field_name]

    def add_error(self, field_name, error):
        if not self._errors:
            self._errors = ErrorDict()

        if field_name is None:
            field_name = NON_FIELD_ERRORS
        if not field_name in self._errors:
            self._errors[field_name] = self.error_class()

        if isinstance(error, Exception):
            self._errors[field_name].extend(error.messages)
        else:
            if not isinstance(error, unicode):
                error = unicode(error)
            self._errors[field_name].append(error)

    def reset_errors(self, field_name):
        if field_name in self._errors:
            del self._errors[field_name]

    def as_table(self):
        "Returns this form rendered as HTML <tr>s -- excluding the <table></table>."
        return self._html_output(
            normal_row=u'<tr%(html_class_attr)s><td>%(label)s</td><td>%(field)s%(help_text)s%(errors)s</td></tr>',
            error_row=u'<tr><td colspan="2">%s</td></tr>',
            row_ender=u'</td></tr>',
            help_text_html=u'<br /><span class="helptext">%s</span>',
            errors_on_separate_row=False)

    def get_fieldsets(self):
        if hasattr(self, 'fieldsets') and self.fieldsets:
            for title, fields in self.fieldsets:
                yield (title, (self[fname] for fname in fields))

    @property
    def errors_ajax(self):
        errors = {}
        if getattr(self, 'errors', None):
            for k, value in self.errors.items():
                errors[self.add_prefix(k)] = [error for error in value]
        return errors


class HierarhyCheckboxSelectMultiple(forms.CheckboxSelectMultiple):

    def __init__(self, queryset=None, ordering_by=None, **kwargs):
        super(HierarhyCheckboxSelectMultiple, self).__init__(**kwargs)
        self.queryset = queryset
        self.ordering = ordering_by
        self.hierarchy_list = []

        if self.queryset:
            level_0 = self.queryset.filter(parent__isnull=True)
            if self.ordering:
                level_0 = level_0.order_by(self.ordering)
            ids = [i.pk for i in self.queryset]
            for item in level_0:
                self.hierarchy_list.append(item)
                for i in item.children.all().order_by(self.ordering):
                    if i.pk in ids:
                        self.hierarchy_list.append(i)

    def render(self, name, value, attrs=None, choices=()):
        if value is None: value = []
        final_attrs = self.build_attrs(attrs, name=name)
        output = [format_html(u'<ul {0}>', flatatt(final_attrs))]
        options = ''
        choices = chain(self.choices, choices)
        for choice_value, label in choices:
            if choice_value in self.queryset:
                options += u'<li><label for="interest_{id}"><input id="interest_{id}" type="checkbox" name="interest" value="{value}"/> <strong>{label}</strong></label></li>'.format(id=choice_value, value=choice_value, label=label)
            else:
                options += u'<li><label for="interest_{id}"><input id="interest_{id}" type="checkbox" name="interest" value="{value}"/> {label}</label></li>'.format(id=choice_value, value=choice_value, label=label)
        print options
        if options:
            output.append(options)
        output.append(u'</ul>')
        return mark_safe('\n'.join(output))


class ChangePasswordForm(forms.Form):
    password = forms.CharField(label=u'Текущий пароль', max_length=100, widget=forms.PasswordInput())
    new_password = forms.CharField(label=u'Новый пароль', max_length=100, widget=forms.PasswordInput())

    def clean(self):
        pass

    def save_password(self):
        cleaned_data = super(ChangePasswordForm, self).clean()

class ResetPasswordForm(forms.Form):
    token = forms.CharField(widget=forms.HiddenInput())
    password = forms.CharField(label=u'Новый пароль', max_length=100, widget=forms.PasswordInput())
    pass2 = forms.CharField(label=u'Подтверждение', max_length=100, widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super(ResetPasswordForm, self).clean()
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('pass2')

        if password.strip() and password == confirm_password:
            return cleaned_data
        elif not password.strip():
            raise forms.ValidationError(u'Пароли не может быть пустым')
        else:
            raise forms.ValidationError(u'Пароли не совпадают')

    def set_password(self):
        token = self.cleaned_data.get('token', None)
        if token:
            try:
                client = Client.objects.get(token=token)
            except Client.DoesNotExist:
                return False

            client.user.set_password(self.cleaned_data.get('password'))
            client.user.save()
            return True
        else:
            return False


class EditClient(ImprovedFormMixin, forms.ModelForm):
    password = forms.CharField(label=u'Пароль', max_length=100, widget=forms.PasswordInput(), required=False)
    first_name = forms.CharField(label=u'Имя', max_length=255, required=False)
    #last_name = forms.CharField(label=u'Фамилия', max_length=255)
    region = forms.ModelChoiceField(label=u'Регион', queryset=Region.objects.order_by('name'))
    city = ChainedModelChoiceField(app_name='django_geoip', model_name='City', chain_field='region', model_field='region', show_all=False, auto_choose=True)
    email = forms.EmailField(label=u'Электронная почта')

    class Meta:
        model = Client
        exclude = ('status',)
        widgets = {
            'user': forms.HiddenInput(),
            'interests': forms.CheckboxSelectMultiple()
            # 'type': forms.HiddenInput(),
        }

    def clean(self):
        cd = self.cleaned_data
        if cd.get('type') == Client.PRIVATE and not cd.get('first_name', '').strip():
            self.add_error('first_name', u'Обязательное поле')
        return cd

    def save(self, commit=True):
        client = super(EditClient, self).save(commit=False)
        user = client.user
        pwd = self.cleaned_data['password']
        if pwd.strip():
            user.set_password(pwd)
            client.raw_password = pwd
        user.first_name = self.cleaned_data['first_name']
        #user.last_name = self.cleaned_data['last_name']
        user.city = self.cleaned_data['city']
        user.email = self.cleaned_data['email']
        user.save()

        return super(EditClient, self).save(commit=commit)


class PersonalInfoForm(forms.ModelForm):
    class Meta:
        model = PersonalInfo


class OrganizationInfoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(OrganizationInfoForm, self).__init__(*args, **kwargs)
        self.fields['name'].required = True
        self.fields['short_name'].required = True

    class Meta:
        model = OrganizationInfo
        widgets = {
            'description': forms.Textarea(attrs={'maxlength': '1000'})
        }


class PhoneForm(forms.ModelForm):
    class Meta:
        model = Phone
        widgets = {
            'client': forms.HiddenInput()
        }

    def clean(self):
        cleaned_data = super(PhoneForm, self).clean()
        client = cleaned_data.get("client")
        number = cleaned_data.get("number")
        if client.type == 1:
            return self.cleaned_data
        elif client.type == 2:
            if number == u'' or number is None:
                msg = u"Обязательное поле"
                self._errors["number"] = self.error_class([msg])
            return self.cleaned_data


class CheckPasswordForm(ImprovedFormMixin, forms.ModelForm):
    raw_password = forms.CharField(label=u'Пароль', max_length=100, widget=forms.PasswordInput())

    def clean(self):
        cd = self.cleaned_data
        if self.instance.raw_password != cd.get('raw_password', '').strip():
            self.add_error('raw_password', u'Введите пароль, котоырй был в письме')
        return cd

    class Meta:
        model = Client
        fields = ['raw_password']