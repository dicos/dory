$(document).ready(function() {
    // переключение в окне регистрации
    $('.select_type').click(function(e) {
        if ($(this).val() == 1) {
            $('.personal_reg_form').removeClass('hidden');
            $('.organization_reg_form').addClass('hidden');
        } else {
            $('.personal_reg_form').addClass('hidden');
            $('.organization_reg_form').removeClass('hidden');
        }
    })







    //подтверждение удаления объявления
    $('.ad__delete').click(function(e) {
        e.preventDefault();
        $(this).hide();
        $(this).next('.ad__delete_popup').removeClass('hidden');
        $('.ad__delete_ok').click(function(e) {
            e.preventDefault();
            var form = $(this).parent('form');
            $.get(
                form.prop('action'),
                function(resp) {
                    if (resp.status) {
                        window.location.reload();
                    }
                }
            )
        });
        $('.ad__delete_cancel').on('click', function(e) {
            e.preventDefault();
            $(this).parents('.ad__delete_popup').addClass('hidden');
            $('.ad__delete').show();
        });
    });

    //подтверждение деактивации объявления
    $('.ad__deactivate').click(function(e) {
        e.preventDefault();
        $(this).hide();
        $(this).next('.ad__deactivate_popup').removeClass('hidden');
        $('.ad__deactivate_ok').click(function(e) {

            var block_ad = $(this).parents("div.b-ad-short.i-white-box");
            
            e.preventDefault();
            var form = $(this).parent('form');
            $.get(
                    form.prop('action'),
                    function(resp) {
                        if (resp.status) {
                            block_ad.remove();
//                            window.location.reload();
                        }
                    }
                )
        });
        $('.ad__deactivate_cancel').on('click', function(e) {
            e.preventDefault();
            $(this).parents('.ad__deactivate_popup').addClass('hidden');
            $('.ad__deactivate').show();
        });
    });

    //активация объявления
    
    $('.ad__activate').click(function(e) {
        e.preventDefault();
        $(this).hide();
        $(this).next('.ad__activate_popup').removeClass('hidden');
        $('.ad__activate_ok').click(function(e) {
            var block_ad = $(this).parents("div.b-ad-short.i-white-box");
            e.preventDefault();
            var form = $(this).parent('form');


            $.get(
                form.prop('action'),
                function(resp) {
                    if (resp.status) {
                        if(resp.redirect_url){

                            window.location.href = resp.redirect_url
                        } else if (resp.message) {
                            block_ad.remove();
//                            window.location.reload();


                        } else {
                            window.location.reload();
                        }
                    } else {
                    }
                }
            )
        });
        $('.ad__activate_cancel').on('click', function(e) {
            e.preventDefault();
            $(this).parents('.ad__activate_popup').addClass('hidden');
            $('.ad__activate').show();
        });
    });



    //вопросы к объвлению
    $('.c-bt').on('click', function(e) {
        e.preventDefault();
        var url = $('form.comment-add').prop('action');
        var data = $('form.comment-add').serialize();
        $.post(
            url,
            data,
            function(resp) {
                console.log(resp);
                if (resp.redirect_url) {
                        window.location.href = resp.redirect_url;
                     }
            }
        )
    })
    //comment answer
    $('.i-bt-answer').on('click', function(e) {
        e.preventDefault();
        var url = $(this).parent().prop('action');
        var data = $(this).parent().serialize();
        $.post(
            url,
            data,
            function(resp) {
                console.log(resp);
                if (resp.redirect_url) {
                    window.location.href = resp.redirect_url;
                }
            }
        )
    })

    //принять предложение попап
    $('.ad__action--yes').click(function(e) {
        e.stopPropagation();
        var left = $(this).offset().left - 30;
        var top = $(this).offset().top - 150;

        $('.ad__popup').addClass('ad__popup--hide');
        var popup = $('#' + $(this).attr('data') + '.apply_offer')
        if ($(popup).hasClass('ad__popup--show')) {
            $(popup)
                .removeClass('ad__popup--show')
                .addClass('ad__popup--hide')
        } else {
            $(popup)
                .removeClass('ad__popup--hide')
                .addClass('ad__popup--show')
                .css({
                    'top': top,
                    'left': left
                })
        }
        $('input[data=apply_offer]').click(function(e) {
            e.stopPropagation();
            var form = $(this).parents('form');
            var url = form.prop('action');
            var data = form.serialize();
            $.get(
                url,
                data,
                function(resp) {
                    if (resp.status) {
                        $(popup)
                            .removeClass('ad__popup--show')
                            .addClass('ad__popup--hide');
                        window.location.reload();
                    }
                }
            )
        })

        $('body, .js-close').click(function(event) {
            $('.ad__popup')
                .removeClass('ad__popup--show')
                .addClass('ad__popup--hide')
        });
    })

    //Отклонить предложение попап
    $('.ad__action--no').click(function(e) {
        e.stopPropagation();
        var left = $(this).offset().left - 30;
        var top = $(this).offset().top - 150;

        $('.ad__popup').addClass('ad__popup--hide');
        var popup = $('#' + $(this).attr('data') + '.cancel_offer')
        if ($(popup).hasClass('ad__popup--show')) {
            $(popup)
                .removeClass('ad__popup--show')
                .addClass('ad__popup--hide')
        } else {
            $(popup)
                .removeClass('ad__popup--hide')
                .addClass('ad__popup--show')
                .css({
                    'top': top,
                    'left': left
                })
        }
        $('input[data=cancel_offer]').click(function(e) {
            e.stopPropagation();
            var form = $(this).parents('form');
            var url = form.prop('action');
            var data = form.serialize();
            $.get(
                url,
                data,
                function(resp) {
                    if (resp.status) {
                        $(popup)
                            .removeClass('ad__popup--show')
                            .addClass('ad__popup--hide');
                        lwindow.ocation.reload()
                    }
                }
            )
        })
    })

    $('.ad__popup').click(function(event) {
        event.stopPropagation()
    });
    $('body').click(function(event) {
        $('.ad__popup')
            .removeClass('ad__popup--show')
            .addClass('ad__popup--hide')
    });

    //удалить предложение попап
    $('.ad__action--delete--offer').on('click', function(e) {
        e.preventDefault();
        var left = $(this).position().left;
        var top = $(this).position().top;

        var popup = $('#' + $(this).attr('data') + '.delete_offer')
        if ($(popup).hasClass('ad__popup--show')) {
            $(popup)
                .removeClass('ad__popup--show')
                .addClass('ad__popup--hide')
        } else {
            $(popup)
                .removeClass('ad__popup--hide')
                .addClass('ad__popup--show')
                .css({
                    'top': top,
                    'left': left
                })
        }
        console.log(popup)

        $('input[data=delete_offer]').click(function(e) {
            e.stopPropagation();
            var form = $(this).parents('form');
            var url = form.prop('action');
            var data = form.serialize();
            $.post(
                url,
                data,
                function(resp) {
                    if (resp.status) {
                        $(popup)
                            .removeClass('ad__popup--show')
                            .addClass('ad__popup--hide');
                        window.location.reload();
                    }
                }
            )
        })

        $('.js-close').click(function(e) {
            e.stopPropagation();
            $(this).closest('div.ad__popup')
                .removeClass('ad__popup--show')
                .addClass('ad__popup--hide')
        })

    })

    /////////////////////////
    //ajax отправка формы создания объявления
    // $('.js-submit').click(function(e) {
    //     e.preventDefault();
    //     var form = $(this).parents('form');
    //     var url = form.attr('action');
    //     var data = form.serialize();
    //     $.post(
    //         url,
    //         data,
    //         function(resp) {
    //             console.log(resp)
    //             if (resp.status) {
    //                 if (resp.redirect_url) {
    //                     window.location.href = resp.redirect_url;
    //                 }
    //                 $('div.page').html('<h1>Объявление успешно сохранено</h1><p>Вы можете посмотреть его по этой <a class="i-link" href="' + resp.url + '">ссылке</a></p>')
    //             } else {
    //                 $.each(resp.errors, function(index, item) {
    //                     if (item[1] != null) {
    //                         $('[name=' + item[0] + ']').addClass('er')
    //                     }
    //                 })
    //             }
    //         }
    //     )
    // })

    // загрузка фоток с описанием

    // var upload_temp = $('.b-ad-new .upload-about .template').html() //шаблон фотки с описанием
    // $('.b-ad-new .upload-about .template').remove() // удаляем шаблон из верстки

    // var photoAbout_counts = $('.photo-about--item').length //текущие кол-во фоток(для последущего редактирования)

    // $('.b-ad-new .ad-form-1 .bt-upload-area').fileupload({
    //     url: '/catalog/upload_images/',
    //     dataType: 'json',
    //     done: function(e, data) {
    //         var form = $(e.target).parents('form')
    //         console.log(form)
    //         $(form).find('.upload-about table').append(_replace(data.result.files, data.result.template))
    //         repaintPreview()
    //         console.log(data.result)

    //     },
    //     fail: function(e, data) {
    //         repaintPreview()
    //     }
    // })

    // $('.b-ad-new .ad-form-2 .bt-upload-area').fileupload({
    //     url: '/catalog/upload_coupon_images/',
    //     dataType: 'json',
    //     done: function(e, data) {
    //         var form = $(e.target).parents('form')
    //         $(form).find('.upload-about table').append(_replace(data.result.files, data.result.template))
    //         // перерисовываем превьюшку
    //         console.log(data.result.files)
    //     },
    //     fail: function(e, data) {
    //         // перерисовываем превьюшку
    //     }
    // })

    // function _replace(array, template) {
    //     var html = ''

    //     $.each(array, function(index, val) {
    //         ++photoAbout_counts
    //         var t = template
    //         t = t.replace('[count]', photoAbout_counts)
    //         t = t.replace('[img]', val.thumbnailUrl)
    //         console.log(val.url)
    //         t = t.replace('[big_img]', val.url)
    //         html = html + t
    //         $('input[name="photo-about_max-counts"]').val(photoAbout_counts)
    //     });
    //     console.log(html)
    //     return html
    // }

    //удаление фото при создании редактировании объявления
    $(document).on('click', '.b-ad-new .upload-about .remove.f-ad', function() {
        var tr = $(this).parents('tr.photo-about--item')
        console.log(tr)
        $.get($(this).attr('data-url'), function(data) {
            if (data.status) {
                tr.remove()
                repaintPreview() // перерисовываем превьюшку
            } else {
                console.log(data)
            }
        })
        console.log(121231)
    })

    //удаление фото при создании редактировании купона
    $(document).on('click', '.b-ad-new .upload-about .remove.f-coupon', function() {
        var tr = $(this).parents('tr.photo-about--item')
        console.log(tr)
        $.get($(this).attr('data-url'), function(data) {
            if (data.status) {
                tr.remove()
                repaintPreview() // перерисовываем превьюшку
            } else {
                console.log(data)
            }
        })
    })

    // //сообщения об оплате доп услуг по объявленияю
    // $(document).on('click', '.ad-form :checkbox[name=options]', function(e) {
    //     var opt_sum = 0;
    //     $.each($(".ad-form input[name=options]:checked"), function() {
    //         opt_sum = opt_sum + parseInt($(this).attr('data'));
    //     })
    //     if (opt_sum <= parseInt($('input#balance').attr('value'))) {
    //         $('td.payment_message_inline').html('Хватает средств').closest('table').show();
    //     } else {
    //         var need = opt_sum - parseInt($('input#balance').attr('value'));
    //         $('td.payment_message_inline').html("<h1>Для публикации не хватает " + need + " руб.</h1><h2>Стоимость публикации " + opt_sum + " руб</h2><div>Ваш баланс: " + $('input#balance').attr('value') + " руб</div><p>После сохранения объявления Вы будете перенаправлены на страницу пополнения баланса</p><p>Данное объявление будет иметь статус неопубликовано, вы сможете опубликовать его из личного кабинета</p>").closest('table').show();
    //     }
    //     console.log(opt_sum)
    // })


    // //удаление неоплаченных опций при редактировании объявления
    // $('.delete_ad_option_order').click(function(e) {
    //     e.preventDefault();
    //     var obj = $(this);
    //     var url = $(this).attr('data-url');
    //     $.get(
    //         url,
    //         function(resp) {
    //             if (resp.status) {
    //                 obj.closest('tr').remove();
    //                 $.get(
    //                     '/catalog/load_ad_options/' + obj.attr('data') + '/',
    //                     function(resp) {
    //                         if (resp.status) {
    //                             $('table.load_ad_options').html(resp.html);
    //                         } else {
    //                             console.log(resp);
    //                         }
    //                     }
    //                 )

    //             } else {
    //                 console.log(resp);
    //             }
    //         }
    //     )
    // })
});
