#-* coding: UTF-8 *-

from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template import RequestContext

from payments.models import RefillOrder

from models import PaymentCheck
from payments.settings import ORDER_IS_PAID, ORDER_IS_UNPAID, ORDER_FAILED
from robokassa.settings import PASSWORD1
import settings
from utils import get_signature


def result_view(request):
    if request.method == 'POST':
        try:
            order_cost = request.POST['OutSum']
            order_number = request.POST['InvId']
            signature = request.POST['SignatureValue']
        except KeyError:
            raise Http404

        try:
            order = RefillOrder.objects.get(number=order_number)
        except RefillOrder.DoesNotExist:
            order = None
            message = u'Заказ с номером %s не найден' % order_number

        if order:
            signature_for_check = get_signature(str(order.amount), str(order.number),
                                                settings.PASSWORD2)
            if signature_for_check != signature:
                message = u'Неверная сигнатура'

            if str(order.amount) != order_cost:
                message = u'Стоимость не совпадает'

            if signature == signature_for_check and str(order.amount) == order_cost:
                check = PaymentCheck(order_number=order_number, order_cost=order_cost)
                check.save()
                message = 'OK' + order_number

        return HttpResponse(message)

    raise Http404


def success_view(request, template='robokassa/success.html'):
    if request.method == 'POST':
        order_cost = request.POST.get('OutSum')
        order_number = request.POST.get('InvId')
        signature = request.POST.get('SignatureValue')

        try:
            order = RefillOrder.objects.get(number=order_number)
        except RefillOrder.DoesNotExist:
            order = None
            message = u'Извините, но заказ с таким номером не найден'

        if order:
            signature_for_check = get_signature(str(order.amount), str(order.number), PASSWORD1)
            if signature_for_check != signature:
                message = u'Неверная сигнатура'

            if str(order.amount) != order_cost:
                message = u'Стоимость не совпадает'

            if signature == signature_for_check and str(order.amount) == order_cost:
                try:
                    check = PaymentCheck.objects.get(order_number=order_number)
                except:
                    message = u'Произошла ошибка при подтверждении платежа'
                else:
                    if check.order_cost == order_cost:
                        order.status = ORDER_IS_PAID
                        order.save()
                        message = u'Спасибо, платеж по заказу %s прошел успешно' % order.number
                    else:
                        message = u'Произошла ошибка при поддтверждении платежа'

        return render_to_response(template, RequestContext(request, {'message': message}))

    raise Http404


def fail_view(request, template='robokassa/fail.html'):
    if request.method == 'POST':
        order_cost = request.POST.get('OutSum')
        order_number = request.POST.get('InvId')
        signature = request.POST.get('SignatureValue')

        try:
            order = RefillOrder.objects.get(number=order_number)
        except RefillOrder.DoesNotExist:
            order = None
            message = u'Извините, но заказ с таким номером не найден'

        if order:
            signature_for_check = get_signature(str(order.amount), str(order.number))
            if signature_for_check != signature:
                message = u'Неверная сигнатура'

            if str(order.amount) != order_cost:
                message = u'Стоимость не совпадает'

            if signature == signature_for_check and str(order.amount) == order_cost:
                order.status = ORDER_FAILED
                order.save()
                message = u'Извините, но платеж по заказу %s не прошел' % order.number

        return render_to_response(template, RequestContext(request, {'message': message}))

    raise Http404