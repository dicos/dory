from django.conf.urls import patterns, include, url, static
from msg.views import CreateMailMessage, AdMessages, CreateAdMessage, ChatView, CreateChatMessage


urlpatterns = patterns('',
    url(r'^create_mail_message/', CreateMailMessage.as_view(), name='create_mail_message'),
    url(r'^create_ad_message/(?P<ad_id>\d+)/', CreateAdMessage.as_view(), name='create_ad_message'),
    url(r'^get_ad_messages/(?P<ad_id>\d+)/', AdMessages.as_view(), name='get_ad_messages'),
    url(r'^my/$', 'msg.views.chat_list_view', name='chat_list_view'),
    url(r'^chat/(?P<pk>\d+)/$', ChatView.as_view(), name='chat_view_messages'),
    url(r'^create_chat_message/$', CreateChatMessage.as_view(), name='create_chat_message'),

)