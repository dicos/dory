#!-*-coding:utf-8-*-
import json
import mimetypes

from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.db.models import Q
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.template import RequestContext
from django.template.loader import render_to_string
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.utils.decorators import method_decorator
from django.utils.html import escape
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST, require_GET
from django.views.generic import DetailView, ListView, DeleteView, CreateView, UpdateView, View
from django.views.generic.base import TemplateView

from django_geoip.models import Region, City

from catalog.models import AdFavorits
from catalog.settings import REQUIRED_LOGIN, AD_TYPES, BIDS
from client.models import Client, PurchaseContact
from client.utils import get_city
from forms import AdForm, AdImageForm
from msg.forms import MailAdMessageForm, AdMessageForm
from models import Ad, Parameter_group, Parameter, AdParams, AdPhoto
from models import Category
from payments.models import Balance


### HANDLERS ###
from payments.utils import activate_ad


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def load_params(request, category):
    if request.is_ajax():
        try:
            category = Category.objects.get(machine_id=category)
        except Category.DoesNotExist:
            return HttpResponse(json.dumps({'status': False, 'message': u'Такой категории не существует'}),
                                content_type='application/json')

        html = render_to_string('load_params.html', locals())

        return HttpResponse(json.dumps({'status': True, 'html': html}), content_type='application/json')

    return HttpResponse(json.dumps({'status': False, 'message': u'Неверный запрос'}), content_type='application/json')


@require_POST
def upload_images(request):

    img_form = AdImageForm(request.POST, request.FILES)

    if img_form.is_valid():
        image = img_form.save()
        files = [{
             'url': image.img.url,
             'name': image.img.name,
             'type': mimetypes.guess_type(image.img.path)[0] or 'image/png',
             'thumbnailUrl': image.img.url,
             'size': image.img.size,
             # 'deleteUrl': reverse('/catalog/upload_delete/%d/' % image.pk),
             'deleteType': 'DELETE',
             'image': image.pk,
         }]
        template = render_to_string('catalog/load_preview_template.html', {'img': image})
        response = json.dumps({'files': files, 'template': template})
        return HttpResponse(response, content_type='application/json')

    return HttpResponse('FAIL')


@login_required()
def delete_images(request, image_id):
    try:
        photo = AdPhoto.objects.get(pk=image_id)
    except AdPhoto.DoesNotExist:
        return HttpResponse(json.dumps({'status': False}), 'json')

    client = Client.objects.get(user=request.user)

    if not photo.ad or photo.ad in client.client_ads.all():
        photo.delete()
        return HttpResponse(json.dumps({'status': True}), 'json')

    return HttpResponse(json.dumps({'status': False}), 'json')


@require_GET
def get_cities(request):
    regions = Region.objects.order_by('name')
    response = list()
    for region in regions:
        cities = list()
        for city in region.cities.all():
            cities.append({'id': city.pk, 'name': city.name})

        response.append({'name': region.name, 'cities': cities})
    return HttpResponse(json.dumps(response), 'json')


def asearch(request):
    val = request.GET.get('val', None)
    city = request.session.get('city_ad_display_name')
    if city:
        ads = Ad.objects.filter(city__name=city)
    else:
        city = get_city(request)
        ads = Ad.objects.filter(city=city)
    if val:
        val = val.strip().lower()
        q_cnd = Q(title__icontains=val) | Q(text__icontains=val)
        ads = ads.filter(q_cnd)
    return render_to_response('catalog/ajax_search.html',
                              {'ads': ads},
                              context_instance=RequestContext(request))


def check_city_ad_display(request):
    if request.is_ajax():
        if request.method == 'GET':
            try:
                city_ad_display = City.objects.get(pk=int(request.GET.get('city_id')))
            except Exception:
                return HttpResponse(json.dumps({'status': False}), 'json')
            # client = Client.get_client(request)
            # if client:
            #     client.city = city_ad_display
            #     client.save()
            request.session['city_ad_display'] = city_ad_display.pk
            request.session['city_ad_display_name'] = city_ad_display.name
            return HttpResponse(json.dumps({'status': True}), 'json')

    return HttpResponse(json.dumps({'status': False}), 'json')


### AD ###


class AdFilterMixin(object):
    """
    GET arguments:
        type
        pfilter
        pfilter_value
    """

    def get_filters(self):

        self.filters = []
        self.filters_context = {}

        type_filter_value = self.request.GET.get('type', None)
        if type_filter_value:
            self.filters.append({'type': type_filter_value})
            self.filters_context['type'] = type_filter_value
            # self.filters['type'] = type_filter_value
        pfilters = []
        pfilter_values = []
        if self.request.GET.getlist('parameter') and self.request.GET.getlist('parameter_value'):
            for pfilter, pfilter_value in zip(self.request.GET.getlist('parameter'),
                                              self.request.GET.getlist('parameter_value')):
                try:
                    pfilter = int(pfilter)
                    pfilter_value = int(pfilter_value)
                except:
                    continue
                if pfilter and pfilter_value:
                    pfilters.append(pfilter)
                    pfilter_values.append(pfilter_value)

        if pfilters and pfilter_values:
            for pfilter, pfilter_value in zip(pfilters, pfilter_values):
                if pfilter and pfilter_value:
                    self.filters_context[pfilter] = pfilter_value
                    self.filters.append({'ad_parameters__parameter__id__exact': pfilter,
                                         'ad_parameters__parameter_value__id__exact': pfilter_value})

        if any([type_filter_value, pfilters, pfilter_values]):
            return True
        else:
            return False


class AdSortingMixin(object):
    def get_sorting(self):
        self.sortings = []

        by_price_direct = self.request.GET.get('by_price_d', None)
        by_price_reverse = self.request.GET.get('by_price_r', None)
        by_date_direct = self.request.GET.get('by_date_d', None)
        by_date_reverse = self.request.GET.get('by_date_r', None)

        if by_date_direct:
            self.sortings.append('date')
            self.request.session['sorting_data'] = 'date'
        elif by_date_reverse:
            self.sortings.append('-date')
            self.request.session['sorting_data'] = '-date'
        if by_price_direct:
            self.sortings.append('price')
            self.request.session['sorting_price'] = 'price'
        elif by_price_reverse:
            self.sortings.append('-price')
            self.request.session['sorting_price'] = '-price'

        print(self.sortings)


class CategoryView(ListView, AdFilterMixin, AdSortingMixin):
    model = Ad
    template_name = 'catalog/category_ads.html'
    context_object_name = 'ads'

    def get_context_data(self, *args, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        context['category'] = self.category
        context['filters'] = self.filters_context
        context['sortings'] = self.sortings
        context['types'] = AD_TYPES
        context['parameters'] = self.category.parameters.all()
        context['categories'] = Category.objects.filter(parent__isnull=True)
        return context

    def get_queryset(self):
        filtered = self.get_filters()
        self.get_sorting()
        self.category = get_object_or_404(Category, machine_id=self.kwargs['slug'])
        client = Client.get_client(self.request)
        city = get_city(self.request)

        ad_list = self.category.get_all_children().filter(city=city, is_active=True)

        if filtered:
            if self.filters:
                for param_filter in self.filters:
                    ad_list = ad_list.filter(**param_filter)

        if 'ad_type' in self.kwargs.keys():
            if self.kwargs['ad_type'] in [k for k,v in AD_TYPES]:
                ad_list = ad_list.filter(type=self.kwargs['ad_type'])

        sorting_date = self.request.session.get('sorting_date', None)
        sorting_price = self.request.session.get('sorting_price', None)
        sortings = [x for x in (sorting_date, sorting_price) if x]
        sortings.append('-date')
        ad_list = ad_list.order_by(*sortings)

        return ad_list


class CatalogView(ListView):
    queryset = Category.objects.filter(is_active=True, parent__isnull=True)
    template_name = 'catalog/catalog.html'
    context_object_name = 'categories'


class AdView(DetailView):
    model = Ad
    template_name = 'catalog/ad_view.html'
    context_object_name = 'ad'

    def get_context_data(self, **kwargs):
        context = super(AdView, self).get_context_data(**kwargs)
        context['questions'] = self.object.ad_messages.filter(parent__isnull=True)
        context['mail_message_form'] = MailAdMessageForm(initial={'ad': self.object})
        context['ad_message_form'] = AdMessageForm(
            initial={'ad': self.object, 'client': Client.get_client(self.request)})
        context['client'] = Client.get_client(self.request)

        if context['client'] != context['ad'].client:
            context['ad'].views += 1
            context['ad'].save()
        #context['need_auth'] = self.object.type in REQUIRED_LOGIN and not self.request.user.is_authenticated()
        return context


@csrf_exempt
def decline_ad(request, pk):
    """ Отклонить подобранное предложение """
    curr_ad = get_object_or_404(Ad, pk=pk)
    decline = get_object_or_404(Ad, pk=request.POST.get('id'))
    curr_ad.declines.add(decline)
    return HttpResponse("ok")


class AdSearchView(CategoryView):
    def get_context_data(self, *args, **kwargs):
        context = super(CategoryView, self).get_context_data(**kwargs)
        context['q'] = self.q
        return context

    def get_queryset(self):
        q = self.request.GET.get('q', None)
        self.q = q.strip()
        if self.q and len(self.q) >= 3:
            self.queryset = Ad.objects.filter(
                Q(title__icontains=self.q) |
                Q(text__icontains=self.q)
            )
        if self.q == '':
            self.queryset = Ad.objects.all()
        return self.queryset


class MyAdListView(ListView):
    template_name = 'catalog/my_ad_list_view.html'
    context_object_name = 'ads'

    def get_queryset(self):
        if self.kwargs.get('state', None) == 'inactive':
            self.state = 'inactive'
            qs = Ad.objects.filter(client__user=self.request.user, is_active=False, is_deleted=False)
        elif self.kwargs.get('state', None) == 'deleted':
            self.state = 'deleted'
            qs = Ad.objects.filter(client__user=self.request.user, is_deleted=True)
        else:
            self.state = 'active'
            qs = Ad.objects.filter(client__user=self.request.user, is_active=True)
        return qs.order_by('-date')

    def get_context_data(self, **kwargs):
        context = super(MyAdListView, self).get_context_data(**kwargs)
        context['request'] = self.request
        context['state'] = self.state
        context['active_count'] = Ad.objects.filter(client__user=self.request.user, is_active=True).count()
        context['inactive_count'] = Ad.objects.filter(client__user=self.request.user, is_active=False, is_deleted=False).count()
        context['deleted_count'] = Ad.objects.filter(client__user=self.request.user, is_deleted=True).count()

        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(MyAdListView, self).dispatch(request, *args, **kwargs)


class CreateAd(CreateView):
    model = Ad
    form_class = AdForm
    template_name = 'catalog/create_ad.html'
    ad_created = False

    def get_context_data(self, **kwargs):
        context = super(CreateAd, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.filter(is_active=True, parent__isnull=True)
        context['image_form'] = AdImageForm()
        context['client'] = Client.get_client(self.request)
        context['position'] = 0 # TODO: что-то надо сделать с позицией
        return context

    def get_initial(self):
        self.client = get_object_or_404(Client, user=self.request.user)
        self.ip = self.request.META.get('REMOTE_ADDR')
        return {'client': self.client, 'ip': self.ip}


    def form_invalid(self, form):
        response = json.dumps({
            'status': False,
            'errors': [(field.name, field.errors) for field in form if field.errors],
        })

        return HttpResponse(response, 'json')


    def form_valid(self, form):
        self.object = form.save()
        self.parameters = self.request.POST.getlist('parameter')
        self.parameter_values = self.request.POST.getlist('parameter_value')
        self.parameters = zip(self.parameters, self.parameter_values)
        for p in self.parameters:
            parameter = Parameter_group.objects.get(pk=int(p[0]))
            good_params = AdParams(ad=self.object, parameter=parameter)
            if parameter.parameter_type == 0:
                value = Parameter.objects.get(pk=int(p[1]))
                good_params.parameter_value = value
            elif parameter.parameter_type == 1:
                value = escape(p[1])
                good_params.text = value
            elif parameter.parameter_type == 2:
                value = float(p[1])
                good_params.number = value
            good_params.save()

        self.img_ids = self.request.POST.getlist('image')
        # индекс не нужен, и + он не работает
        self.img_indexes = self.request.POST.getlist('index')
        img_errors = []
        count = 1
        for img_id, img_index in zip(self.img_ids, self.img_indexes):
            try:
                image = AdPhoto.objects.get(pk=int(img_id))
                image.ad = self.object
                image.index = count
                image.save()
                count += 1
            except AdPhoto.DoesNotExist:
                img_errors.append(img_id)
                continue
        if img_errors:
            return HttpResponse(json.dumps({'status': False, 'errors': img_errors}), 'json')
        return HttpResponse(json.dumps({'status': True, 'url': reverse('my_ad_list_view')}), 'json')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CreateAd, self).dispatch(request, *args, **kwargs)


class EditAd(UpdateView):
    model = Ad
    form_class = AdForm
    template_name = 'catalog/edit_ad.html'
    ad_created = False
    context_object_name = 'ad'

    def get_context_data(self, **kwargs):
        object = super(EditAd, self).get_object()
        context = super(EditAd, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.filter(is_active=True, parent__isnull=True)
        context['image_form'] = AdImageForm()
        context['ad_edited'] = self.ad_created
        context['client'] = Client.get_client(self.request)
        adv = Ad.objects.filter(date__lt=object.date)
        context['position'] = adv.count() + 1
        context['cat_position'] = context['position']
        return context

    def get_initial(self):
        self.client = get_object_or_404(Client, user=self.request.user)
        self.ip = self.request.META.get('REMOTE_ADDR')
        return {'client': self.client, 'ip': self.ip}

    def form_invalid(self, form):
        response = json.dumps({
            'status': False,
            'errors': [(field.name, field.errors) for field in form if field.errors],
        })

        return HttpResponse(response, 'json')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        parameters = self.request.POST.getlist('parameter')
        parameter_values = self.request.POST.getlist('parameter_value')
        parameters = zip(parameters, parameter_values)
        self.object.ad_parameters.all().delete()
        for p in parameters:
            parameter = Parameter_group.objects.get(pk=int(p[0]))
            good_params = AdParams(ad=self.object, parameter=parameter)
            if parameter.parameter_type == 0:
                value = Parameter.objects.get(pk=int(p[1]))
                good_params.parameter_value = value
            elif parameter.parameter_type == 1:
                value = escape(p[1])
                good_params.text = value
            elif parameter.parameter_type == 2:
                value = float(p[1])
                good_params.number = value
            good_params.save()

        self.img_ids = self.request.POST.getlist('image')
        self.img_indexes = self.request.POST.getlist('index')
        img_errors = []
        count = 1
        # индексы не работают прпробуем с count
        for img_id, img_index in zip(self.img_ids, self.img_indexes):
            try:
                image = AdPhoto.objects.get(pk=int(img_id))
            except AdPhoto.DoesNotExist:
                img_errors.append(img_id)
                continue
            image.ad = self.object
            image.index = count
            image.save()
            count += 1

        if img_errors:
            return HttpResponse(json.dumps({'status': False, 'errors': img_errors}), 'json')

        return HttpResponse(json.dumps({'status': True, 'url': reverse('ad_view', args=[self.object.id])}), 'json')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        client = Client.get_client(request)
        if self.get_object() not in client.client_ads.all():
            raise Http404
        return super(EditAd, self).dispatch(request, *args, **kwargs)


class DeactivateAd(DetailView):
    model = Ad

    def get(self, request, *args, **kwargs):
        ad = super(DeactivateAd, self).get_object()
        if ad.client.user == self.request.user:
            ad.is_active = False
            ad.save()
            return HttpResponse(json.dumps({'status': True}), 'json')

        return HttpResponse(json.dumps({'status': False}), 'json')


class ActivateAd(DetailView):
    model = Ad

    def get(self, request, *args, **kwargs):
        ad = super(ActivateAd, self).get_object()
        return activate_ad(request, ad)

class DeleteAd(DeleteView):
    model = Ad

    def get(self, request, *args, **kwargs):
        ad = super(DeleteAd, self).get_object()
        if ad.client.user == self.request.user:
            ad.is_deleted = True
            ad.save()
            return HttpResponse(json.dumps({'status': True}), 'json')

        return HttpResponse(json.dumps({'status': False}), 'json')


class AdStrip(ListView):
    model = Ad
    context_object_name = 'ads'
    template_name = 'catalog/lenta.html'

    def get_queryset(self):
        client = Client.get_client(self.request)
        client_interests = client.interests.all()
        client_region = client.region
        result = Ad.objects.filter(category__in=client_interests,
                                   region=client_region,
                                   type__in=BIDS,
                                   is_shown=True) \
                            .exclude(client_declines=client)
        result = result.exclude(client=client)
        self.all_types = set(result.distinct().values_list('type', flat=True))
        if 'ad_type' in self.kwargs:
            if self.kwargs['ad_type'] in [k for k, v in AD_TYPES]:
                result = result.filter(type=self.kwargs['ad_type'])
        return result

    def get_context_data(self, **kwargs):
        context = super(AdStrip, self).get_context_data(**kwargs)
        context['all_types'] = self.all_types  # TODO: говнокод, надо будет заменить на контекстный процессор
        return context


    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AdStrip, self).dispatch(request, *args, **kwargs)


class HideAd(AdStrip):
    def post(self, request, *args, **kwargs):
        client = Client.get_client(self.request)
        hidden = self.request.POST.get('val')
        adhid = get_object_or_404(self.get_queryset(), pk=hidden)
        adhid.client_declines.add(client)
        return HttpResponse("<p>Предложение удалено</p>")


class AdFavorities(ListView):
    model = AdFavorits
    context_object_name = 'favorits'
    template_name = 'catalog/favorits.html'

    def get_queryset(self):
        client = Client.get_client(self.request)
        client = client.client_favorits.filter(ad__is_active=True)
        ads = list()
        for c in client: ads.append(c.ad)
        return ads


def AdFavoritiesChange(request, mod, ad_id):
    try:
        a = Ad.objects.get(pk=ad_id)
        c = Client.get_client(request)
        if mod == 'del':
            fav = AdFavorits.objects.get(ad=a, client=c)
            fav.delete()
        elif mod == 'add':
            fav = AdFavorits(ad=a, client=c)
            fav.save()
            # return HttpResponse(json.dumps({'status': True, 'message': u'Неверный запрос'}), 'json')
        return redirect(request.META.get('HTTP_REFERER'))
    except AdPhoto.DoesNotExist:
        return HttpResponse(json.dumps({'status': False}), 'json')


class BuyContactPageView(TemplateView):
    template_name = 'payments/buy_contacts.html'

    def get_context_data(self, **kwargs):
        ctx = super(BuyContactPageView, self).get_context_data(**kwargs)
        ad = self.request.GET.get('ad')
        ctx['ad'] = ad
        ad = Ad.objects.get(pk=ad)
        ctx['client'] = ad.client
        ctx['next'] = self.request.GET.get('next')
        ctx['prev_page'] = self.request.META.get('HTTP_REFERER', '/')
        return ctx


def buy_contact(request):

    ad = request.GET.get('ad')
    ad = Ad.objects.get(pk=ad)
    if PurchaseContact.objects.filter(who=request.user.client, client=ad.client):
        if request.GET.get('next'):
            return HttpResponseRedirect(request.GET.get('next'))
        return HttpResponse(json.dumps({'success': True}), content_type='application/json')
    try:
        balance = Balance.objects.get(client=request.user.client)
        ad_price = Category.objects.get(pk=ad.category.pk).price
        if balance.money < ad_price:
            response = json.dumps({'success': False})
        else:
            balance.money -= ad_price
            balance.save()

            p = PurchaseContact()
            p.who = request.user.client
            p.client = ad.client
            p.save()
            response = json.dumps({'success': True})
    except Balance.DoesNotExist:
        response = json.dumps({'success': False})
    if request.GET.get('next'):
        return HttpResponseRedirect(request.GET.get('next'))
    return HttpResponse(response, content_type='application/json')
