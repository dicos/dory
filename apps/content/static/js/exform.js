(function($) {
    "use strict";
    var Exform = {
        init: function(Options, form) {
            var exform = this
            exform.$elem = $(form)
            exform.options = $.extend(exform, $.fn.exForm.options, Options)
            exform.check_names = exform.check_names.split(' ')
            exform.error_names = exform.check_names

            $.each($(form).find('[data-exform]'), function(i, elem) {
                $(elem).addClass('exform-element')
            });

            exform.build(exform)

        },
        build: function() {
            var base = this
            base.password_Build()
            base.stepper_Build()
            base.errorlist_Build()
            base.number_Build()
            base.email_Build()
            base.final_Build()
        },
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        password_Build: function() {
            var base = this
            base.password_Wrap()
            base.password_AddEye()
            base.password_AddRepeat()
            base.password_Events()
        },
        password_Wrap: function() {
            var base = this
            var elem = $(base.$elem).find('[data-exform=password]')
            $(elem).wrap('<div class="' + base.password_wrap_class + '"></div>').wrap('<div></div>').addClass(base.password_class_)
            base.password_margins = {}
            base.password_margins.padding = $(elem).css('padding')
            base.password_margins.margin = $(elem).css('margin')
            base.password_margins.border = $(elem).css('border')
        },
        password_AddEye: function() {
            var base = this
            var elem = $(base.$elem).find('[data-exform=password]')
            var margins = base.password_margins
            if (elem.length != 0) {
                $(elem).css('padding-right', $(elem).outerHeight() + 8)
                var b = $('<span class="' + base.password_eye_class + '"></span>').css({
                    position: 'absolute',
                    width: $(elem).outerHeight(),
                    height: $(elem).outerHeight(),
                    // width: $(elem).height(),
                    // height: $(elem).height(),
                    display: 'inline-block',

                    // 'border': margins.border,
                    // 'border-color': 'transparent',
                    // 'padding': margins.padding,
                    // 'margin': margins.margin,


                    'text-align': 'center',
                    'cursor': 'pointer',
                    'background-image': base.password_OpenOrClose($(elem)),
                    'background-position': 'center',
                    'background-repeat': 'no-repeat',
                    'background-size': $(elem).height(),
                })
                b = $(b).css({
                    'margin-left': '-' + $(b).outerWidth() + 'px',
                })

                $(elem).after(b)
            }
        },
        password_AddRepeat: function() {
            var base = this
            var margins = base.password_margins
            var elem = $(base.$elem).find('[data-exform=password]')
            if (elem.length != 0) {
                var elem_pass_repeat = elem
                    .clone()
                    .removeAttr('name')
                    .removeAttr('data-exform')
                    .removeAttr('title')
                    .attr('placeholder', base.password_repeat_placeholder)
                    .addClass(base.password_repeat_class)
                    .removeClass(base.password_class_)
                    .removeClass('error')
                elem_pass_repeat = '<div>' + elem_pass_repeat[0].outerHTML + '</div>'
                $(elem)
                    .parent()
                    .after(elem_pass_repeat)
                //
                var elem = $(elem).closest('.' + base.password_wrap_class).find('.' + base.password_repeat_class)

                var b = $('<span class="' + base.password_repeat_ico_class + '"></span>').css({
                    position: 'absolute',
                    width: $(elem).outerHeight(),
                    height: $(elem).outerHeight(),
                    // width: $(elem).height(),
                    // height: $(elem).height(),
                    display: 'inline-block',

                    // 'border': margins.border,
                    // 'border-color': 'transparent',
                    // 'padding': margins.padding,
                    // 'margin': margins.margin,

                    'opacity': '.1',
                    'text-align': 'center',
                    'background-image': base.password_repeat_ico_true,
                    'background-position': 'center',
                    'background-repeat': 'no-repeat',
                    'background-size': $(elem).height(),
                })
                b = $(b).css({
                    'margin-left': '-' + $(b).outerWidth() + 'px',
                })
                $(elem).after(b)
            }
        },

        password_OpenOrClose: function(elem) {
            var background = this.password_eye_open
            if ($(elem).prop('type') == 'password') {
                background = this.password_eye_close
            }
            return background
        },
        password_Events: function() {
            var base = this
            base.password_Check()
            base.password_Show()
        },
        password_Show: function() {
            var base = this
            var form = $(base.$elem)
            $(form).on('click', '.' + base.password_eye_class, function(event) {
                var eye = this
                var p = $(this).siblings('input')
                var pp = $(this).parent().parent().find('.' + base.password_repeat_class)
                if ($(p).prop('type') === 'password') {
                    $(this).css('background-image', base.password_eye_open)
                    $(p).prop('type', 'text')
                    $(pp).prop('type', 'text')
                } else {
                    $(this).css('background-image', base.password_eye_close)
                    $(p).prop('type', 'password')
                    $(pp).prop('type', 'password')
                };
            })
        },
        password_Check: function() {
            var base = this
            var form = $(base.$elem)
            $(form).on('keyup', ('.' + base.password_repeat_class), function(event) {
                var pp = $(this)
                var p = $(this).parent().parent().find('.' + base.password_class_)
                var s = $(this).siblings('span')
                base.password_CampareValues(p, pp)
                base.event_FormChange()
            })
            $(form).on('keyup', ('.' + base.password_class_), function(event) {
                var p = $(this)
                var pp = $(this).parent().parent().find('.' + base.password_repeat_class)
                var s = $(this).siblings('span')
                base.password_CampareValues(p, pp)
                base.event_FormChange()
            })
        },
        password_CampareValues: function(p, pp) {
            if (pp.val() == p.val() && p.val() != '') {
                $(pp).siblings('span').css('opacity', 1)
                $(p).removeClass('error')
            } else {
                $(pp).siblings('span').css('opacity', '.1')
                $(p).addClass('error')
            }
        },
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        stepper_Build: function() {
            var base = this
            base.stepper_Wrap()
            base.stepper_Events()
        },
        stepper_Wrap: function() {
            var base = this
            var elem = $(base.$elem).find('[data-exform=stepper]')
            var minus = ' <input type="button" class="' + base.stepper_minus_class + '" value="&minus;"> '
            var plus = ' <input type="button" class="' + base.stepper_plus_class + '" value="+"> '
            $(elem)
                .addClass(base.stepper_class)
                .wrap('<span class="' + base.stepper_wrap_class + '"></span>')
                .before(minus).after(plus)
        },
        stepper_Events: function() {
            var base = this
            var form = $(base.$elem)
            $(form).on('keydown', '.' + base.stepper_class, function(e) {
                var key = e.keyCode
                var arr = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105]
                switch (key) {
                    case (9):
                        //Tab
                        break
                    case (8):
                        //Backspace
                        break
                    case (38):
                        e.preventDefault()
                        base.stepper_Plus($(this))
                        break
                    case (40):
                        e.preventDefault()
                        base.stepper_Minus($(this))
                        break
                    default:
                        e.preventDefault()
                        break
                }
            })
            $(form).on('click', '.' + base.stepper_plus_class, function(event) {
                base.stepper_Plus($(this).prev('input[data-exform=stepper]'))
                base.stepper_Check()
            })

            $(form).on('click', '.' + base.stepper_minus_class, function(event) {
                base.stepper_Minus($(this).next('input[data-exform=stepper]'))
                base.stepper_Check()
            })
        },
        stepper_Plus: function(it) {
            var base = this
            var o = base.toJSON($(it).data('option'))
            if (o.length == 0) {
                o.min = 0;
                o.step = 1;
                o.max = ''
            };
            if (it.val() == '' || isNaN(it.val())) {
                it.val(o.min)
            };
            if (o.max != '') {
                if ((it.val() * 1 + o.step * 1) > o.max) {
                    it.val(o.max)
                } else {
                    it.val(it.val() * 1 + o.step * 1)
                }
            } else {
                it.val(it.val() * 1 + o.step * 1)
            }
        },
        stepper_Minus: function(it) {
            var base = this
            var o = base.toJSON(($(it).data('option')))
            if (o.length == 0) {
                o.min = 0;
                o.step = 1;
                o.max = ''
            };
            if (it.val() == '' || isNaN(it.val())) {
                it.val(o.min)
            };
            if ((it.val() * 1 - o.step * 1) < o.min) {
                it.val(o.min)
            } else {
                it.val(it.val() * 1 - o.step * 1)
            }
        },
        stepper_Check: function() {
            var base = this
            $.each(base.$elem.find('.' + base.stepper_class), function(i, elem) {
                if ($(elem).val() == '') {
                    $(elem).addClass('error')
                } else {
                    $(elem).removeClass('error')
                }
            });
            base.errorlist_Check()
            base.errorlist_ShowError()
        },
        toJSON: function(str) {
            if (str == undefined) {
                return ({
                    length: 0
                })
            } else {
                str = ('{\"' + str + '\"}')
                    .replace(/:/g, '\":\"')
                    .replace(/,/g, '\",\"')
                str = $.parseJSON(str)
                return str
            }
        },
        event_FormChange: function() {
            var base = this
            var form = $(base.$elem)
            $(form).trigger({
                type: 'formchange'
            })
        },
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        errorlist_Build: function() {
            var base = this
            base.errorlist_BeforeAddError()
            base.errorlist_Embed()

            base.errorlist_Check()
            base.errorlist_ShowError()
            base.errorlist_Event()
        },
        errorlist_BeforeAddError: function() {
            var base = this
            var form = base.$elem
            var list = base.check_names
            $.each(list, function(index, val) {
                $(form).find('[name=' + val + ']').addClass('error')
            });
        },
        errorlist_Embed: function() {
            var base = this
            var form = base.$elem
            if (base.errorlist_insert_to != false) {
                $('.' + base.errorlist_insert_to).html('<div class="' + base.errorlist_class + '"></div>')
            } else {
                form.append('<div class="' + base.errorlist_class + '"></div>')
            }
        },
        errorlist_Check: function() {
            var base = this
            var form = base.$elem
            var list = base.check_names
            var error_names = []

            $.each(list, function(index, name) {
                var elem = $(form).find('[name=' + name + ']')
                var title = $(form).find('[name=' + name + ']').prop('title')
                var type = ($(elem).data('exform') || $(elem).prop('type'))
                var error = false

                switch (type) {
                    case ('email'):
                        checkClass(elem)
                        break
                    case ('number'):
                        if ($(elem).val() == '') {
                            error = true
                        } else {
                            $(elem).removeClass('error')
                        }
                        break
                    case ('password'):
                        checkClass(elem)
                        break
                    case ('stepper'):
                        checkClass(elem)
                        break
                    case 'radio':
                        var check = $(form).find('[name=' + name + ']:checked').length
                        if (check) {
                            $(elem).removeClass('error')
                        } else {
                            $(elem).addClass('error')
                            error = true
                        }
                        break
                    case 'checkbox':
                        var check = $(form).find('[name=' + name + ']:checked').length
                        if (check) {
                            $(elem).removeClass('error')
                        } else {
                            $(elem).addClass('error')
                            error = true
                        }
                        break
                    case 'text':
                        if ($(elem).val() == '') {

                            error = true
                        } else {
                            $(elem).removeClass('error')
                        }
                        break
                    case 'textarea':
                        if ($(elem).val() == '') {

                            error = true
                        } else {
                            $(elem).removeClass('error')
                        }
                        break
                }

                function checkClass(elem) {
                    if ($(elem).hasClass('error')) {
                        error = true
                    }
                }
                if (error) {
                    error_names.push(title)
                }

            });
            base.error_names = error_names
            base.final_Prepare()

        },
        errorlist_ShowError: function() {
            var base = this
            var form = base.$elem
            if (base.errorlist_show) {
                var punctuation
                var html
                if (base.error_names.length != 0) {
                    if (base.error_names.length == 1) {
                        html = base.errorlist_captions[0]
                        punctuation = '.'
                    } else {
                        html = base.errorlist_captions[1]
                        punctuation = ', '
                    }
                    $.each(base.error_names, function(i, val) {
                        if (i == base.error_names.length - 1) {
                            punctuation = '.'
                        }
                        html = html + '<span>' + val + '</span>' + punctuation
                    });

                } else {
                    html = ''
                }
                if (base.errorlist_insert_to != false) {
                    $('.' + base.errorlist_insert_to).find('.' + base.errorlist_class).html(html)

                } else {
                    $(form).find('.' + base.errorlist_class).html(html)

                }
            }

        },
        errorlist_Event: function() {
            var base = this
            var form = base.$elem
            $(form).on('formchange', function(event) {
                var form = $(this).parents('form')
                base.errorlist_Check(form)
                base.errorlist_ShowError()
            });
            $(form).on('change paste keyup', 'input', function(event) {
                var form = $(this).parents('form')
                base.errorlist_Check(form)
                base.errorlist_ShowError()
            })
        },
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        number_Build: function() {
            var base = this
            base.number_Check()

            var form = base.$elem
            $(form).on('keydown keypress', 'input[data-exform=number]', function(event) {
                console.log(event)
                if (!onlyNumber(event)) {
                    return false
                }
            })

            function onlyNumber(e) {
                var key = e.which
                var altKey = e.shiftKey || e.ctrlKey || e.altKey
                var return_
                var arr_key = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 46, 9, 13, 8, 39, 37, 38, 40, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105]
                if ($.inArray(key, arr_key) === -1 || altKey) {
                    return_ = false
                } else {
                    if (key == 38) {
                        base.stepper_Plus($(e.target))
                    };
                    if (key == 40) {
                        base.stepper_Minus($(e.target))
                    };
                    return_ = true
                }
                base.number_Check()
                return return_
            }
        },
        number_Check: function() {
            var base = this
            var form = base.$elem
            var list = $(form).find('input[data-exform=number]')
            $.each(list, function(i, elem) {
                if ($(elem).val() == '') {
                    $(elem).addClass('error')
                } else {
                    $(elem).removeClass('error')
                }
            });
        },
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        email_Build: function() {
            var base = this
            base.email_Check()
            base.email_Events()
        },
        email_Check: function() {
            var base = this
            var form = base.$elem
            var elem = $(form).find('input[data-exform=email]')
            var str = $(elem).val()
            var r = /.+@.+\..+/;
            if (r.test(str)) {
                $(elem).removeClass('error')
            } else {
                $(elem).addClass('error')
            }
        },
        email_Events: function() {
            var base = this
            var form = base.$elem
            var elem = $(form).find('input[data-exform=email]')
            $(form).on('change paste keyup', elem, function(event) {
                base.email_Check()
                base.errorlist_Check()
                base.errorlist_ShowError()
            })
        },
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        // #######################################################
        final_Build: function() {
            var base = this
            base.final_Prepare()
            base.final_Submit()
        },
        final_Prepare: function() {
            var base = this
            var form = base.$elem
            var button = $(form).find('[data-exform=submit]')
            if (button.length != 0) {
                if (base.error_names.length == 0) {
                    $(button).removeClass('exform-disabled')
                    $(button).prop('disabled', false)
                } else {
                    $(button).prop('disabled', true)
                    $(button).addClass('exform-disabled')

                }
            }
        },
        final_Submit: function() {
            var base = this
            var form = base.$elem
            var msg = ''
            $(form).on('click', '[data-exform=submit]', function(event) {
                event.preventDefault();
                msg = $(form).serialize()
                $('.finish').html(msg)
                /* Act on the event */
            });
        }
    }
    $.fn.exForm = function(Options) {
        return this.each(function() {
            var exform = Object.create(Exform);
            exform.init(Options, this)
        })
    };
    $.fn.exForm.options = {

        stepper_class: 'exform-stepper', //класс поля счетчика
        stepper_wrap_class: 'exform-stepper-wrap', //класс обертки
        stepper_minus_class: 'exform-stepper-minus', // класс поля минус
        stepper_plus_class: 'exform-stepper-plus', // класс поля плюс
        stepper_min: 0, //минимальное значение
        stepper_max: '', //максимальное значение
        stepper_step: 1, //шаг
        //Таб, бекспейс, выделить все — не работает

        phone: '', // — Добавь

        text_min_symbol: 4, // — Добавь
        text_max_symbol: 255, // — Добавь

        password_wrap_class: 'exform-password-wrap', // класс обертка 
        password_class_: 'exform-password', // класс поля пароля

        password_eye: true, // отображать/скрытвать пароль
        password_eye_class: 'exform-password-eye', // имя класса картинки
        password_eye_open: 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAAd0lEQVR4Ac3MvQnCUACF0TNCRskCwYwjKJJeUWvdQSdQ1xAhPlMEZ7CJK/gKX8CfAXK+9nINzMjqXenH0sPZxtzCVq2z1svULoqvt6sgA44O/jnFkOvS0ljQmPXPTzmVnSSoBXfJXkUZSxq3WCspYh8m2tjUwLwAp0ogbvkxBCoAAAAASUVORK5CYII=)',
        // картинка если пароль открыт
        password_eye_close: 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAATElEQVR4AWOAglHgAIR45QIY7jMIYJUWAMokgBjzwQwBNMkEoOh8KA/IOc/wn2E/w3qGBoYGILkfyDvDEIduoANDAVhBApAlQMdQAACsMhIOcNoaqAAAAABJRU5ErkJggg==)',
        // картинка если пароль закрыт

        password_repeat: true, // повторить пароль
        password_repeat_class: 'exform-password-repeat', //имя класса для поля 
        password_repeat_placeholder: 'Повторите пароль', //Текст подсказки
        password_repeat_ico_class: 'exform-password-repeat-ico', // имя класса иконки
        password_repeat_ico_true: 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAACWCAMAAAC/1XffAAAAM1BMVEX////v7+8QEBDPz88wMDBgYGCfn5+vr68gICB/f39wcHBAQEDf39+Pj49QUFC/v78AAAD4eoyYAAAAEHRSTlMAEO8wz59gUN+Aj78gcK9ATrgPIQAAAwtJREFUeF7tnMvOnTAMhBNIAiEQ5v2ftquyKZVFQb/zSZ31WTiS52IZnzAWyjQHMuZJa2TXL6XIrl/q8PqlHJCISb9xwOuXCrx+AcU0S8YLSPXzxLTqD6QAQtENloDBohtklIGhO2i/q3+NNAPgqmgyCEwyAGIWqnABKmIL0CxDgIgGMM1EAWWOAk03qHAB7XQB4hD4FJvA88ROEDHBCdzgBN7gBC50AgtO4FsB2uAjWIMniBVOYNEJXOAEziACwx0swx2swB1shjtYXHWDBI/Q0x4oqHACLGITYJ/YBIgJToAMJ0CBE2D+TwBfdPgMcOgOGT6DrZEd4TTDCXDACdDgBJjoBFjgBNjgBEiRQwC2gsYEV9AMV9AidgbdJ0cFjR8IXZLjJrW9D7ubZwbN7+P6qVucP1X/2xfEydGC8wcjU3dsoPzB0Hc4WnA1PPNFhKg/7z7lywiRPNyzfKegml3cv3ymoNUpvSzPFdStgZZPdqDdr4E2ffCCQ45DQDNe8O8ZtLmOUFN8m0G1+3yK8XwKr04KdGF+94JZ8t5kLHrRw3H1UiBbRfILC64++8TnLzg9G8gWkjJ0A9lSZL+gWw3kLUU6/4X+6xhraduS44NXO6Qi25K7hlrGNz00tEUeY/xzRWkPib8MttuS8qMG6mPcKNjBfnnAGW9HVjEaaJxVRjLE1GygNsatvt0YizynGNtb78XUbqA6xrm4LUXdyBCeaLLZuehxhvAPpjrNn+SBT04vKbJjkz+qQeTTcItxaZAfZKYxaXBcOmWMkaO6gc6/R746+OnsxdP0YgrwD0Vqh4Q4atgnPUIb+3rKxj70/ZqNijmgAjDYGJFJVxmVyuAL6TGDmVpaOXc8vKuATmKwnUuJ34QuIAbbudQ+bOMZ8hQpfytlzMHUJkq0kxJ7G8ZKdT0wUKkSemE1QhBUidYYMNgMCUUqUQoknIaEApuoBxbiZEsoKljXgEMHpVB7xD8CEIcurAGJZAzynFjaAhSb4WEUM8gBiwLwMHuFvAUw5svDqMiqAY2YYvDHLwsA/xsPLKryAAAAAElFTkSuQmCC)',
        //картинка

        errorlist_class: 'exform-errorlist', // имя класса обертки
        errorlist_show: true, // отображать не заполненые поля
        errorlist_captions: ['Заполните поле: ', 'Заполните поля: '], // название для одного или нескольок не заполненых полей
        errorlist_insert_to: '', //куда вставить ошибки || пусто — в конец формы
    }
})(jQuery);
// $('.form2').exForm({
//     check_names: 'name mail summa pass col col2 variant comment',
//     errorlist_show: true,
//     errorlist_insert_to: 'for-error',
//     errorlist_captions: ['Заполните поле: <br>', 'Заполните поля: <br>']
// })
// $('.form3').exForm({
//     check_names: 'mail pass passpass',
// })
// таб не прожимается на полях для числе
