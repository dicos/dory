# -*- coding:utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from models import Client, PersonalInfo, OrganizationInfo, Phone, PurchaseContact
from django_geoip.models import Region, City


class ClientInline(admin.StackedInline):
    model = Client
    can_delete = False
    verbose_name = u'Данные клиента'


class UserAdmin(UserAdmin):
    inlines = (ClientInline,)


class ClientPersonalInfoInline(admin.StackedInline):
    model = PersonalInfo
    verbose_name = u'Персональная информация'
    extra = 0


class ClientOrganizationInfoInline(admin.StackedInline):
    model = OrganizationInfo
    verbose_name = u'Информация об организации'
    extra = 0


class ClientPhoneInline(admin.StackedInline):
    model = Phone
    extra = 0


class ClientAdmin(admin.ModelAdmin):
    inlines = (ClientPhoneInline,
               ClientPersonalInfoInline,
               ClientOrganizationInfoInline)


class RegionAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)


class CityAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(PurchaseContact)
