from django.conf.urls import patterns, include, url, static
from django.views.generic import base


urlpatterns = patterns('',
    url(r'^$', 'content.views.index', name='index'),
)