#!-*-coding: utf-8-*-
import json
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from client.models import Client
from feedbacks.forms import FeedbackForm
from feedbacks.models import Feedback


class FeedbackListView(ListView):
    model = Feedback
    context_object_name = 'feedbacks'

    def get_context_data(self, **kwargs):
        context = super(FeedbackListView, self).get_context_data(**kwargs)
        context['feedbacks_by_me'] = Feedback.objects.filter(clien=self.client)
        return context

    def get_queryset(self):
        self.client = Client.get_client(self.request)
        feedbacks = Feedback.objects.filter(about=self.client)
        return feedbacks

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(FeedbackListView, self).dispatch(request, *args, **kwargs)


class CreateFeedback(CreateView):
    model = Feedback
    form_class = FeedbackForm

    def get(self, request, *args, **kwargs):
        about_id = request.GET.get('about_id', None)
        if not about_id:
            return HttpResponse(json.dums({'status': False}), content_type="application/json")
        client = Client.get_client(request)
        form = FeedbackForm(initial={'about': Client.objects.filter(pk=about_id).first(), 'client': client})
        html = render_to_string('feedbacks/popup_create_feedback.html', {'form': form})
        return HttpResponse(json.dumps({'status': True, 'html': html}), content_type="application/json")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if Feedback.objects.filter(about=self.object.about, client=self.object.client).exists():
            return HttpResponse(json.dumps({'status': False}), content_type="application/json")
        else:
            form.save()
        return HttpResponse(json.dumps({'status': True}), content_type="application/json")

    def form_invalid(self, form):
        errors = [(field.label, field.errors) for field in form]
        return HttpResponse(json.dumps({'status': False, 'errors': errors}), content_type="application/json")

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CreateFeedback, self).dispatch(request, *args, **kwargs)