'use strict'
// Новое объявлени или акция
$(function() {
    $("#id_price_free, #id_price_agreement").change(function() {
        var j_this = $(this);
        var checked = j_this.prop('checked');
        $("#id_price_free, #id_price_agreement").prop('checked', false);
        j_this.prop('checked', checked);
    });
    var templates = {
        ad: {
            title: {
                type: '<span class="blank">Тип</span>',
                title: '<span class="blank">Название</span>',
                price: '<span class="blank price">Цена</span>',
                html: '[str_before]<span class="i-link">[link]</span>[str_after]'
            },
            content: {
                photo: '<div class="blank-b" style="line-height: 0px;!important"><img style="width:250px;" src="/static/img/no-image.png"></div>',
                description: '<div class="blank-b">Описание</div>',
                html: '<div class="gallery js-photos-fullscreen">[photo]</div><div class="content js-temp-description-ad">[description]</div>'
            }

        }
    };
    $('body').on('change', '.ad-form-2 select', function() {
        repaintPreviewContent.text('coupon')
    });
    $('body').on('keyup', '.ad-form-2 input', function() {
        repaintPreviewContent.text('coupon')
    });
    $('body').on('keyup', '.ad-form-2 textarea', function() {
        repaintPreviewContent.text('coupon')
    });
    //
    $('body').on('change', '.ad-form-1 select', function() {
        repaintPreviewContent.text('ad')
    });
    $('body').on('keyup', '.ad-form-1 input', function() {
        repaintPreviewContent.text('ad')
    });
    $('body').on('change', '.ad-form-1 input ', function() {
        repaintPreviewContent.text('ad')
    });
    $('body').on('keyup', '.ad-form-1 textarea', function() {
        repaintPreviewContent.photo('ad')
    });
    $(document).on('click', '.b-ad-new .upload-about .remove', function() {
        $(this).parents('.photo-about--item').remove()
        if ($('.ad-form-1').length > 0) {
            repaintPreviewContent.photo('ad') // перерисовываем превьюшку
        };
        if ($('.ad-form-2').length > 0) {
            repaintPreviewContent.photo('coupon')
        };
    })


    var upload_temp = $('.b-ad-new .upload-about .template').html() //шаблон фотки с описанием
    $('.b-ad-new .upload-about .template').remove() // удаляем шаблон из верстки
    var photoAbout_counts = $('.photo-about--item').length //текущие кол-во фоток(для последущего редактирования)


    $('.b-ad-new .ad-form-1 .bt-upload-area').fileupload({
        url: '/catalog/upload_images/',
        dataType: 'json',
        done: function(e, data) {
            var form = $(e.target).parents('form')
            $(form).find('.upload-about table').append(paste(data.result.files, data.result.template))
            repaintPreviewContent.photo('ad') // перерисовываем превьюшку
            hideShowPhotoUpDownButton()
        },
        fail: function(e, data) {
            // перерисовываем превьюшку
        }
    })
    $('.b-ad-new .ad-form-2 .bt-upload-area').fileupload({
        url: '/catalog/upload_coupon_images/',
        dataType: 'json',
        done: function(e, data) {
            var form = $(e.target).parents('form')
            $(form).find('.upload-about table').append(paste(data.result.files, data.result.template))
            repaintPreviewContent.photo('coupon') // перерисовываем превьюшку
            hideShowPhotoUpDownButton()
        },
        fail: function(e, data) {
            // перерисовываем превьюшку
        }
    })

    var repaintPreviewContent = {

        text: function(type) {
            var form = $('.ad-form-1');
            var list = form.serialize();
            list = listToObj(list);
            var category = form.find('#id_type :selected').html();
            var temp = templates.ad.title;
            var html = temp.html;
            var free = document.getElementById('id_price_free');
            var agree = document.getElementById('id_price_agreement');
            //console.log(list);
            if (list.type && !list.title) {
                html = html
                    .replace('[str_before]', ' ')
                    .replace('[link]', category)
                    .replace('[str_after]', temp.title + ' ')
            }
            if (!list.type && list.title) {
                html = html
                    .replace('[str_before]', temp.type + ' ')
                    .replace('[link]', list.title)
                    .replace('[str_after]', ' ')
            }
            if (list.type && list.title) {
                html = html
                    .replace('[str_before]', ' ')
                    .replace('[link]', category + ' ' + list.title)
                    .replace('[str_after]', ' ')
            }
            if (!list.type && !list.title) {
                html = html
                    .replace('[str_before]', temp.type + ' ')
                    .replace('[link]', '')
                    .replace('[str_after]', temp.title + ' ')
            }
            //free
            if (list.price && !free.checked && !agree.checked) {
                html = html + ', ' + list.price + ' руб.'
            } else {
                html = html + ' ' + temp.price
            }
            if (!list.price && free.checked) {
                html = html.replace('<span class="blank price">Цена</span>', 'бесплатно');
            } else {

            }
            if (list.price && free.checked) {
                html = html.replace('<span class="blank price">Цена</span>', 'бесплатно')
            } else {

            }
            //agree
            if (!list.price && agree.checked) {
                html = html.replace('<span class="blank price">Цена</span>', 'договорная цена');
            } else {

            }
            if (list.price && agree.checked) {
                html = html.replace('<span class="blank price">Цена</span>', 'договорная цена')
            } else {

            }

            $('.js-ad-new .title').html(html);
            $('.js-temp-description-ad').html(list['short_text']);

        },
        photo: function(type) {
            var photos = $('.photo-about--item').length;
            var form = $('.ad-form-1');
            var list = form.serialize();
            list = listToObj(list);
            var temp = templates.ad.content;
            var html = temp.html;


            if (photos > 1) {
                $('.js-ad-new .b-item-ad').removeClass('b-item-ad--one')
            } else {
                $('.js-ad-new .b-item-ad').addClass('b-item-ad--one')
            }
            //
            var img_html = '';
            $.each($('.photo-about--item'), function(index, val) {
                var src = $(val).find('img').prop('src');
                var o_src = $(val).find('img').data('url');
                var description = $(val).find('textarea').prop('value');
                img_html = img_html + '<img height="180" title="' + description + '" src="' + src + '" data-url="' + o_src + '">'
            });

            //
            if (photos == 0) {
                html = html.replace('[photo]', temp.photo)
            }
            if (photos == 1) {
                html = html.replace('[photo]', img_html)
            }
            if (photos > 1) {
                html = html.replace('[photo]', img_html)
            }

            if (list.text) {
                html = html.replace('[description]', list.text)
            } else {
                html = html.replace('[description]', temp.description)
            }

            $('.b-item-ad .content-wrap').html(html);
        }
    }

    function paste(array, template) {
        var html = '';
        $.each(array, function(index, elem) {
            ++photoAbout_counts;
            var t = template;
            t = t.replace(/\[count\]/g, photoAbout_counts);
            t = t.replace('[img]', elem.thumbnailUrl);
            t = t.replace(/\[big_img\]/g, elem.url);
            html = html + t
            $('input[name="photo-about_max-counts"]').val(photoAbout_counts)
        });
        return html
    }

    function listToObj(list) {
        list = '{"' + list + '"}'
        list = list.replace(/=/g, '":"')
        list = list.replace(/&/g, '","')
        list = list.replace(/%0D%0A/g, '<br>')
        list = list.replace(/\+/g, ' ')
        list = $.parseJSON(list)

        $.each($('.b-ad-new form select'), function(i, el) {
            var name = $(el).prop('name')
            var val = $(el).find('option:selected').val()
            list[name] = val
        });
        for (var i in list) {
            list[i] = decodeURIComponent(list[i])
        }
        for (var i in list) {
            if (list[i] == '') {
                delete list[i]
            }
        }
        return list
    }

    String.prototype.toMoney = function() {
        var str = this;
        
        if (!isNaN(str)) {
            str = str.replace('/[ \f\n\r\t\v]/g', '');
            str = str.replace('/(\d)(?=(\d\d\d)+([^\d]|$))/g', '$1 ');
            return str
        } else {
            return false
        }
    }
    //
    (function(factory) {
        if (typeof define === "function" && define.amd) {

            // AMD. Register as an anonymous module.
            define(["../datepicker"], factory);
        } else {

            // Browser globals
            factory(jQuery.datepicker);
        }
    }(function(datepicker) {

        datepicker.regional['ru'] = {
            closeText: 'Закрыть',
            prevText: '&#x3C;Пред',
            nextText: 'След&#x3E;',
            currentText: 'Сегодня',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
            ],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'
            ],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            weekHeader: 'Нед',
            dateFormat: 'dd.mm.yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        datepicker.setDefaults(datepicker.regional['ru']);

        return datepicker.regional['ru'];

    }));
    var datepicker = {
        dateFormat: "dd.mm.y",
        autoSize: true
    };
    $('#id_date_start').datepicker(datepicker)
    $('#id_date_finish').datepicker(datepicker)
    //
    //
    //из additional.js
    $('.js-submit').click(function(e) {
        e.preventDefault();
        var form = $(this).closest('form');

        var url = form.attr('action');
        var data = "";

        $.each(form.serializeArray(), function(i, field) {
            if (field.name == "price") {
                var val = field.value.split(" ").join("");
            } else {
                var val = field.value;
            }
            data += (field.name + '=' + val + "&");
        });
        $("ul.errorlist").remove();
        $.post(
            url,
            data,
            function(resp) {
                if (resp.status) {
                    if (resp.url) {
                        window.location.href = resp.url;
                    } else {
                        window.location.reload();
                    }
                } else if (resp.errors) {
                    $.each(resp.errors, function(index, item) {
                        if (item[1] != null) {
                            var ul = $('<ul class="errorlist"></ul>');
                            for (index in item[1]) {
                                ul.append("<li>" + item[1][index] + "</li>");
                            }
                            $('*[name=' + item[0] + ']').after(ul);
                        }
                    })
                    $('.js-submit--info').html('<br/><br/><p>Пожалуйста, заполните все необходимые поля.</p>').show();
                }
            }
        )
    })

    // ajax подгрузка параметров для выбранной категории на странице подачи объявления
    $('select[name=category]').on('change', function() {
        var url = $(this).children('option:selected').attr('url_load_params');
        var color_price = $(this).children('option:selected').attr('price');
        $('.color_price').html('0');
        $('.color_price').html(color_price).css("color", "red");
        $.get(
            url,
            function(resp) {
                if (resp.status) {
                    // console.log(resp.html);
                    $('tr.params').remove();
                    //                    $('span.params').removeClass('hidden');
                    $('tr.before_params').after(resp.html);
                } else {
                    $('tr.before_params').after(resp.message);
                }
            }
        )
    });
    //меняем расположение фотографий


    $('.b-ad-new').on('click', '.up', function(event) {
        Reposition(event, 'up')
    })
    $('.b-ad-new').on('click', '.down', function(event) {
        Reposition(event, 'down')
    })

    function Reposition(event, where) {
        var parent = $(event.target).closest('table')
        var el = $(event.target).closest('tr')
        var c_el = $(el)[0].outerHTML

        if (where === 'up') {
            $(el).prev().before(c_el)
        }
        if (where === 'down') {
            $(el).next().after(c_el)
        }
        el.remove()
        hideShowPhotoUpDownButton()
        if ($('.ad-form-1').length) {
            repaintPreviewContent.photo('ad')
        }
    }


    function hideShowPhotoUpDownButton() {
        $('.b-ad-new .upload-about table .up').show()
        $('.b-ad-new .upload-about table .down').show()

        $('.b-ad-new .upload-about table tr:eq(0)').find('.up').hide()

        $('.b-ad-new .upload-about table tr:eq(-1)').find('.down').hide()
    }
    if ($('.photo-about--item').length) {
        repaintPreviewContent.photo('coupon')
    }
})
