from django.contrib import admin
from payments.models import Balance, RefillOrder, ShowPersonalInfoOrder

class BalanceAdmin(admin.ModelAdmin):
    list_display = ('client','number','money',)

class RefillOrderAdmin(admin.ModelAdmin):
    list_display = ('balance','number','amount','date','status','confirmed','date_confirmed',)


admin.site.register(Balance, BalanceAdmin)
admin.site.register(ShowPersonalInfoOrder)
admin.site.register(RefillOrder, RefillOrderAdmin)

