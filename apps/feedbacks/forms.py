#!-*-coding: utf-8-*-

from django import forms
from feedbacks.models import Feedback, Answer


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        exclude = ('date', 'is_active', 'type')
        widgets = {
            'about': forms.HiddenInput(),
            'client': forms.HiddenInput(),
            'rating': forms.HiddenInput()
        }

    def clean(self):
        data = self.cleaned_data
        if data.get('text', None):
            return data
        else:
            raise forms.ValidationError('Provide text')


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        exclude = ('date', 'is_active')
        widgets = {
            'feedback': forms.HiddenInput()
        }
