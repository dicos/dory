#-* coding: UTF-8 *-

import hashlib, urllib

import settings


def get_redirect_url(refill_order, amount=None, email=None):
    params = {}
    params['MerchantLogin'] = settings.LOGIN
    params['OutSum'] = str(amount) if amount else str(refill_order.amount)
    params['InvId'] = str(refill_order.number)
    params['InvDesc'] = 'Пополнение баланса'
    params['SignatureValue'] = get_signature(params['MerchantLogin'], params['OutSum'], params['InvId'], settings.PASSWORD1)
    if email:
        params['Email'] = email
    return settings.FORM_TARGET + '?' + urllib.urlencode(params)


def get_signature(*args):
    return hashlib.md5(':'.join([x for x in args]).encode('cp1251')).hexdigest().upper()
