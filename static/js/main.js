'use strict';

// Общий попап и выбор города
$(function () {

    var last_open = '';
    var cities = [];

    function slidePopup(node_class) {


        if (last_open == node_class) {
            $('.b-slide-popup').slideUp(111);
            last_open = '';
            return false
        }
        $('.b-slide-popup').slideUp(111);
        var p = $('.' + node_class).parents('.b-slide-popup');
        $(p).find('.' + node_class).show(0);
        $(p).slideDown(111);
        last_open = node_class


    }

    $(document).on('click', '.js-slide-popup-close', function (event) {
        $('.b-slide-popup').slideUp(111);
        last_open = ''
    });
    //$('.add-bt').click(function () {
    //    var node_class = $(this).data('node-class');
    //    slidePopup(node_class)
    //});

    $('.js-city-select').click(function () {
        var node_class = $(this).data('node-class');
        slidePopup(node_class)
    });

    $('span.js-auth').click(function () {
        var node_class = $(this).data('node-class');
        slidePopup(node_class)
    });

    $('.elem a.add-bt').click(function () {
        var node_class = $(this).data('node-class');
        slidePopup(node_class)
    });

    //подгрузка городов
    var list = '';
    var cities_loaded = false;
    $.get(
        '/catalog/get_cities/',
        function (resp) {
            if (resp) {
                cities = resp;
                cities_loaded = true;
                build_cities();
            }
        }
    );

    function build_cities() {
        for (var i in cities) {
            list = list + '<li><span class="i-link i-link--fake">' + cities[i].name + '</span></li>'
        }
        list = '<ul>' + list + '</ul>';
        $('.b-city-select .city-list').find('ul').remove();
        $('.b-city-select .region-list').html(list);

        $('.b-city-select .search').keyup(function (event) {
            var str = $(this).val().toLocaleLowerCase();

            $.each($('.region-list li'), function (i, el) {
                var text = $(el).text().toLocaleLowerCase();
                if (text.search(str) == -1) {
                    $(el).hide()
                } else {
                    $(el).show()
                }
            });
            $('.city').css('display', 'inline-block');
            var city = $(this).val().toLocaleLowerCase();
            var html = '';
            for (var i in cities) {
                for (var j in (cities[i].cities)) {
                    var text = cities[i].cities[j].name.toLocaleLowerCase();
                    if (text.indexOf(city) == -1) {
                    } else {
                        html = html + '<li><span class="i-link" data-name="city_id" type="checkbox" data-value="' + cities[i].cities[j].id + '">' + cities[i].cities[j].name + '</span></li>'
                    }
                }
            }
            $('.city-list').html('<ul>' + html + '</ul>');
        });

        $(document).on('click', '.b-city-select .region-list .i-link', function () {
            var q = $(this).text().toLocaleLowerCase();
            var html = '';
            for (var i in cities) {
                if (q == cities[i].name.toLocaleLowerCase()) {

                    for (var j in (cities[i].cities)) {
                        html = html + '<li><span class="i-link" data-name="city_id" type="checkbox" data-value="' + cities[i].cities[j].id + '">' + cities[i].cities[j].name + '</span></li>'
                    }
                }
            }
            $('.city-list').html('<ul>' + html + '</ul>');
            $('.city').css('display', 'inline-block');
        });
        //активный регион
        $(document).on('click', '.b-city-select .i-link', function () {
            $('.b-city-select .i-link').removeClass('i-link--selected')
            $(this).addClass('i-link--selected')
        });
        // отправак формы по клику по городу
        $(document).on('click', '.b-city-select .city-list .i-link', function () {
//            var data = $(event.target).data('name') + '=' + $(event.target).data('value');
            var data = $(this).data('name') + '=' + $(this).data('value')
            var url = $('form#check_city_ad_display').prop('action');
            $.get(
                url,
                data,
                function (resp) {
                    if (resp.status == true) {
                        location.reload();
                    }
                }
            )
        });
        //new
        //$(document).on('keypress', '.b-city-select .header input.search', function() {
        //
        //});

    }

    $('.purchase_cancel').click(function (e) {
        e.preventDefault();
        $(this).parent().hide();
    });

    $('.show_purchase').click(function (e) {
        e.preventDefault();
        $(this).parent().find('.purchase_links').show();
    });

    // $('.bet_button').click(function(e){
    //     e.preventDefault();
    //     if ($('#id_bet').val()) {
    //         $('.bet_table').hide();
    //     }
    // });
    // $('.bet_button').click(function(e){
    //     e.preventDefault();
    //     $('.bet_field').show();
    //     $(this).hide();
    // });

    $('.buy_contact').click(function (e) {
        e.preventDefault();
        var obj = $(this);
        $.ajax({
            url: '/catalog/buy_contact/?ad=' + $(obj).attr('data-ad'),
            type: "GET",
            dataType: "json",
            success: function (e) {
                if (e.success) {
                    location.reload();
                } else {
                    window.location.href = '/payments/';
                }
            }
        });
    });
})


//
// Попап фоток на полный экран
$(function () {
    var maxPhoto = {
        current_position: '',
        imgs: '',
        setPosition: function (n) {
            $('.b-fullscreen .img').attr('src', 'http://green.cwr-team.ru/static/img/spiffygif_32x32.gif');

            $('.b-fullscreen .img').attr('src', $(maxPhoto.imgs[n]).data('url'))
            $('.b-fullscreen .note').text(maxPhoto.imgs[n].title)
            //console.log(maxPhoto.imgs[n])
            if (maxPhoto.imgs[n].title !== '') {
                $('.b-fullscreen .bot').show()
            } else {
                $('.b-fullscreen .bot').hide()
            }
            maxPhoto.current_position = maxPhoto.imgs[n]
        },
        next: function () {
            var s = maxPhoto.imgs.length
            this.imgs.each(function (i, el) {
                if (maxPhoto.current_position.src === el.src) {
                    if (i === s - 1) {
                        maxPhoto.setPosition(0)
                    } else {
                        maxPhoto.setPosition(i + 1)
                    }
                    return false
                }
            })
        },
        prev: function () {
            this.imgs.each(function (i, el) {
                if (maxPhoto.current_position.src === el.src) {
                    if (i === 0) {
                        maxPhoto.setPosition(maxPhoto.imgs.length - 1)
                    } else {
                        maxPhoto.setPosition(i - 1)
                    }
                    return false
                }
            })
        }
    };
    $('.js-photos-fullscreen').click(function (event) {
        maxPhoto.imgs = $(this).find('img');
        maxPhoto.current_position = event.target;
        event.stopPropagation();
        var url = $(event.target).data('url');
        var text = event.target.title;
        if (event.target.title !== '') {
            $('.b-fullscreen .bot').show()
        } else {
            $('.b-fullscreen .bot').hide()
        }

        $('.b-fullscreen .img').prop('src', url);
        $('.b-fullscreen .note').html(text);
        $('.b-fullscreen').show();
        $('body').css({
            overflow: 'hidden'
        });
    });
    $('body').on('click', '.b-fullscreen .next', function (event) {
        maxPhoto.next();
    });
    $('body').on('click', '.b-fullscreen .img', function (event) {
        maxPhoto.next();
    });
    $('body').on('click', '.b-fullscreen .prev', function (event) {
        maxPhoto.prev();
    });
    $('body').on('click', '.b-fullscreen .close', function (event) {
        $('.b-fullscreen').hide();
        $('body').css({
            overflow: 'auto'
        });
    });
});


//Попап авторизации и регистрации
$(function () {

    function login(msg) {
        $.ajax({
            url: '/client/login/',
            type: 'POST',
            dataType: 'json',
            data: msg,
            success: function (resp) {
                if (resp.ok) {
                    $('.b-auth-popup .error').show().html('<p>' + resp.message + '</p>');
                    setTimeout(function () {
                        document.location.reload(true)
                    }, 300)
                } else {
                    $('.b-auth-popup .error').show().html('<p>' + resp.message + '</p>')
                }
            },
            error: function () {
                $('.b-auth-popup .error').show().html('<p>Сервер не отвечает</p>')
            }
        })
    }

    $('.b-auth-popup .js-login').click(function (event) {
        var msg = $(this).parents('form').serialize();
        login(msg);
    });

    $("#log_form input").keypress(function(e){
       if(e.keyCode==13){
           var msg = $(this).parents('form').serialize();
           login(msg);
       }
     });

    $('.b-auth-popup .js-reset').click(function (event) {
        var msg = $(this).parents('form').serialize();
        $.ajax({
            url: '/client/reset_password/',
            type: 'POST',
            dataType: 'json',
            data: msg,
            success: function (resp) {
                if (resp.ok) {
                    $('.b-auth-popup .error').show().html('<p>' + resp.message + '</p>')
                    setTimeout(function () {
                        document.location.reload(true)
                    }, 1000)
                } else {
                    $('.b-auth-popup .error').show().html('<p>' + resp.message + '</p>')
                }
            },
            error: function () {
                $('.b-auth-popup .error').show().html('<p>Сервер не отвечает</p>')
            }
        })
    });

    $('.b-auth-popup .js-auth').click(function (event) {
        var msg = $(this).parents('form').serialize();
        $('.b-auth-popup .error').show().html('<img src="/static/img/ajax-loader-2.gif" width="60">')
        $.ajax({
            url: '/client/signup/',
            type: 'POST',
            dataType: 'json',
            data: msg,
            success: function (resp) {
                if (resp.message) {
                    $('.b-auth-popup .error').html('<p>' + resp.message + '</p>')
                    setTimeout(function () {
                            document.location.reload(true)
                        }, 5000)
                    $("input").prop("disabled", true)
                } else if (resp.link) {
                    window.location.href = resp.link;
                }
            },
            error: function () {
                $('.b-auth-popup .error').html('<p>Сервер не отвечает</p>')
            }
        })
    });

    $('.b-auth-popup .js-switch-tab').click(function (event) {
        var tab = $(this).data('tab')
        $('.b-auth-popup .js-switch-tab').removeClass('i-link--selected')
        $(this).addClass('i-link--selected')
        $('.b-auth-popup .tabs').hide()
        $('.b-auth-popup .' + tab).show()
        $('.b-auth-popup .error').hide()
    });
    $('.b-auth-popup .tabs').hide()
    $('.b-auth-popup .' + $('.i-link--selected').data('tab')).show()

});

function signup_submitHandler() {
    var msg = $('#signup_form').serialize();
    console.log(msg);
        $.ajax({
            url: '/client/signup/',
            type: 'POST',
            dataType: 'json',
            data: msg,
            success: function (resp) {
                $('.b-auth-popup .error').show().html('<p>' + resp.message + '</p>');
                setTimeout(function () {
                        document.location.reload(true)
                    }, 300)
            },
            error: function () {
                $('.b-auth-popup .error').show().html('<p>Сервер не отвечает</p>')
            }
        });
    return false;
}

function reset_submitHandler() {
    var msg = $('#reset_form').serialize();
    $.ajax({
            url: '/client/reset_password/',
            type: 'POST',
            dataType: 'json',
            data: msg,
            success: function (resp) {
                if (resp.ok) {
                    $('.b-auth-popup .error').show().html('<p>' + resp.message + '</p>')
                    setTimeout(function () {
                        document.location.reload(true)
                    }, 1000)
                } else {
                    $('.b-auth-popup .error').show().html('<p>' + resp.message + '</p>')
                }
            },
            error: function () {
                $('.b-auth-popup .error').show().html('<p>Сервер не отвечает</p>')
            }
        })
    return false;
}

// Поиск в шапке
//$(function() {
//    var list = ['первый', 'второй', 'третий', 'четвертый', 'пятый']
//    $('.b-header .search').keyup(function(e) {
//        if (e.which == 38 || e.which == 40 || e.which == 13) {
//            return false;
//        };
//        e.stopPropagation()
//        $.ajax({
//            url: '/catalog/search/',
//            type: 'GET',
//            data: $('.b-header .search').serialize(),
//            success: function(resp) {
//                if (resp.ok) {
//                    $('.b-header .search .search-slide-list').html(resp.data)
//                }
//            },
//            error: function() {
//                var str = ''
//                $.each(list, function(i, el) {
//                    str = str + '<div><span>' + el + '</span></div>'
//                });
//                $('.b-header .search-slide-list').html(str).show()
//
//            }
//        })
//   })
//   $('body').click(function(event) {
//       $('.b-header .search-slide-list .active').removeClass('active')
//       $('.b-header .search-slide-list').hide()
//   });
//    $('.b-header .search').keyup(function(event) {
//       switch (event.which) {
//            case 40:
//                //console.log('↓')
//                if (!$('.active').length) {
//                    $('.b-header .search-slide-list div').eq(0).addClass('active')
//                } else {
//                    $('.b-header .search-slide-list .active').next().addClass('active')
//                    $('.b-header .search-slide-list .active').eq(0).removeClass('active')
//                }

//                break;
//            case 38:
//                //console.log('↑')
//                if (!$('.active').length) {
//                    $('.b-header .search-slide-list div').eq(-1).addClass('active')
//                } else {
//                    $('.b-header .search-slide-list .active').prev().addClass('active')
//                    $('.b-header .search-slide-list .active').eq(-1).removeClass('active')
//                }
//                break;
//            case 13:
//                event.preventDefault()
//                //console.log('Enter')
//                break;
//        }
//    });

//})
// пеключатель форм (регистрация, новое объявление )
$(function () {
    $.each($('.f-form .switch-form'), function (i, el) {
        $('.' + $(el).data('show')).hide(0)
    });
    $.each($('.f-form .switch-form:checked'), function (i, el) {
        $('.' + $(el).data('show')).show(0)
    });
    $(document).on('click', '.switch-form', function () {
        var show_form = $(this).data('show')
        switchForm(show_form)
    })

    function switchForm(show_form) {
        $.each($('.f-form .switch-form'), function (i, el) {
            $('.' + $(el).data('show')).hide(0)
        });
        $('.' + show_form).show(0)
    }
})
// форма регистрации. выбор категории для юр лица
$(function () {
    var categoryList = {
        'cat_312': {
            1: 'Новостройка',
            2: 'Старострока'
        }
    }
    var current_id
    $(document).on('click', '.b-registration .js-category', function () {
        $('.b-registration .js-category').removeClass('i-link--selected')
        $(this).addClass('i-link--selected')
        $('.b-registration .category-list').hide(0)
        $('.b-registration .category-list-' + $(this).data('list')).show(0)
        current_id = $(this).data('list')
    })

    $(document).on('click', '.b-registration .js-category-list', function () {
        var all = $(this).parents('.category-list').find('input').length
        var all_chk = $(this).parents('.category-list').find('input:checked').length
        var html = '(' + all_chk + '/' + all + ')'
        if (all_chk == 0) {
            html = ''
        }
        $('span[data-list=' + current_id + ']').next('.count').html(html)
    })
    //фотка
    $('.b-registration .upload .upload-input').fileupload({
        //        url: 'http://www.jquery-file-upload.appspot.com/',
        url: '/client/upload_image/',
        dataType: 'json',
        submit: function (e, data) {
            var parent = $(e.target).parents('.upload')
            $(parent).find('.add-link').css('visibility', 'hidden')
        },
        done: function (e, data) {
            var parent = $(e.target).parents('.upload')
            var el_result = $(e.target).parents('.upload').find('.rezult')
            $.each(data.result.files, function (index, file) {
                //console.log(file);
                $(el_result).html($('<div class="img-wrap" style="background-image: url(' + file.url + '); background-size: cover; background-position: center; padding-top: 75%; top: -225px"></div>'))
            });
            /*<img src="' + file.url + '">*/
            $(parent).find('.add-link').css('visibility', 'visible').text('Загрузить другую')
        },
        progressall: function (e, data) {
            var el_progress = $(e.target).parents('.upload').find('.progress')
            var progress = parseInt(data.loaded / data.total * 100, 10);
            //console.log(progress)
            $(el_progress).css('visibility', 'visible')
            $(el_progress).find('.bar').css('width', progress + '%');
            if (progress === 100) {
                $(el_progress).css('visibility', 'hidden')
            }
        }
    })

});

//Отправить предложение со страницы объявлений
$(function () {

    $('.b-item-ad').on('click', '.js-popup', function (event) {
        var top = $(this).offset().top;
        event.preventDefault();
        var ad = $(this).parents('.b-item-ad');
        var send = ad.parents().find('.send');
        send.css('top', top);
        send.show();
        var url_ad_popup = $(this).attr('data-url');
        $.get(
            url_ad_popup,
            function (resp) {
                if (resp.status == true) {
                    send.html(resp.html);
                } else {
                    //console.log('Ошибка загрузки формы предложения')
                }
            }
        )
    });
    $('.send').on('click', '.js-close', function (event) {
        var pp = $(this).parents().find('.send');
        pp.hide()
    });
    $('.send').on('click', '.i-bt', function (event) {
        var popup = $(this).parents().find('.send');
        var form = $(this).parents('form');
        var status = $("#offer-send-" + form.find("#id_ad").val());
        var url = form.attr('action');
        var data = form.serialize();

        $.post(
            url,
            data,
            function (resp) {
                if (resp.status) {
                    status.text('✓ Отправить еще одно предложение')
                    //location.reload(); зачем?
                } else {
                    status.text('Ошибка при отправке предложения')
                }
            }
        );
        popup.hide()
    })
});


////редактировние предложения
//$(function () {
//    $('.b-ad-short').on('click', '.js-edit', function (e) {
//        e.preventDefault();
//        var ad = $(this).parents('.b-ad-short')
//        var url = $(this).attr('href');
//        var edit = ad.find('.edit')
//        var state = ad.find('.state')
//
//        $.get(url, function (resp) {
//            if (resp.status) {
//                edit.html(resp.html);
//                edit.show(0)
//                state.hide(0)
//            } else {
//                //console.log(resp.message)
//            }
//            //console.log(resp)
//        })
//    });
//
//    $('.b-ad-short').on('click', '.js-offer', function (e) {
//        var ad = $(this).parents('.b-ad-short');
//        var popup = ad.find('.b-offer-popup');
//        var footer = $(this).parents('.footer');
//        var status = footer.find('.js-popup');
//        var url = $(this).closest('form#edit_offer').attr('action');
//        var data = $(this).closest('form#edit_offer').serialize();
//        $.post(
//            url,
//            data,
//            function (resp) {
//                if (resp.status) {
//                    status.text('✓ Предложение отредактировано');
//                    location.reload();
//                } else {
//                    status.text('Ошибка при отправке предложения')
//                }
//                //console.log(resp);
//            }
//        );
//        status.removeClass('i-link').removeClass('i-link--fake')
//    });
//
//    $('.b-ad-short').on('click', '.js-close', function (e) {
//        var ad = $(this).parents('.b-ad-short')
//        var state = ad.find('.state')
//        var edit = ad.find('.edit')
//        edit.hide()
//        state.show()
//    });
//})
////  Отменить предложение в объявлении
//$(function () {
//    $('.b-ad-short').on('click', '.js-delete-offer-show', function (event) {
//        event.preventDefault();
//        $('.b-ad-short .delete-offer').show()
//        $(this).hide()
//    });
//    $('.b-ad-short').on('click', '.js-delete-offer', function (event) {
//        var form = $(this).parents('form');
//        var url = form.prop('action');
//        var data = form.serialize();
//        var popup = form.parent()
//        $.post(
//            url,
//            data,
//            function (resp) {
//                if (resp.status) {
//                    popup.hide()
//                    location.reload();
//                }
//            }
//        )
//    })
//
//    $('.delete-offer').on('click', '.js-close-delete-offer', function (e) {
//        var popup = $(this).parents('.delete-offer')
//        $('.js-delete-offer-show').show()
//        popup.hide()
//    })
//})

//Страница пользывателя
$(function () {
    $('.b-user-profile').on('click', '.js-message', function (event) {
        event.preventDefault();
        $('.b-user-profile .forms').show()
        $('.b-user-profile .message').show()
        $('.b-user-profile .alarm').hide()
    });
    $('.b-user-profile').on('click', '.js-alarm', function (event) {
        event.preventDefault();
        $('.b-user-profile .forms').show()
        $('.b-user-profile .message').hide()
        $('.b-user-profile .alarm').show()
    });
})
//ответ на вопрос по объявлению
$(function () {
    $('.comment-answer-form .b-bt').click(function (e) {
        e.preventDefault();
        var url = $(this).closest('form').attr('action');
        var data = $(this).closest('form').serialize();
        $.post(
            url, data,
            function (resp) {
                //console.log(resp);
            }
        )
    })
});


//сообщение в приватном чате
$(function () {
    $('form#create_chat_message .i-bt').click(function (e) {
        e.preventDefault();
        var url = $(this).closest('form#create_chat_message').prop('action');
        var data = $(this).closest('form#create_chat_message').serialize();
        $.post(
            url,
            data,
            function (resp) {
                if (resp.status) {
                    location.reload()
                } else {
                    //console.log(resp);
                }
            }
        )
    })
});


//фильтры, выбор типа объявления
$(function () {
    $('.ads-type-wrap .b-link').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('data')
        //console.log(id)
        $('input#filter_by_type').attr('value', id)
        $('.ads-type-wrap').removeClass('ads-type-wrap--select');
        $('.ads-type__item').removeClass('ads-type__item--select');
        $(this).parents('div.ads-type-wrap').addClass('ads-type-wrap--select');
        $(this).closest('a').addClass('ads-type__item--select');
    })
})

//чекбксы в попапе предложений
$('.offer-popup__proposal-check').click(function (event) {
    if ($(this).prop('checked') === true) {
        $(this).parents('.offer-popup__proposal').addClass('offer-popup__proposal--checked')
    } else {
        $(this).parents('.offer-popup__proposal').removeClass('offer-popup__proposal--checked')
    }
});



//↓ to old8
$(function () {

    // a = $('.control-panel-static').offset().top
    // $(document).scroll(function(){
    //  b = $(document).scrollTop()

    //  if (b>a){
    //      $('.control-panel').addClass('cp-fixed')
    //  } else {
    //      $('.control-panel').removeClass('cp-fixed')
    //  }
    // })


    // srollto
    $('.header-bot__back-top').click(function () {
        $('body').scrollTo(0, 300)
    })
    //tops
    $('.js-tops-hide').click(function () {
        $('.steps').slideToggle(300)
    })


    // расскрываем комментарии/активность
    $('.proposals-list__title').click(function () {
        $('.proposals-list').slideToggle(333)
    })
    $('.comments__title').click(function () {
        $('.comments').slideToggle(333)
    })
    // показываем шаги
    $('.project').click(function () {
        $('.tops-wrap').show()
    })
    $('.tops__close').click(function () {
        $('.tops-wrap').hide()
    })

    // расскрываем по клику категории и тд все что с ним связано
    $(function () {
        $('.b-category-line__bt').click(function (e) {
            e.stopPropagation()
            check_select(this)
            if ($('.b-categories-wrap').css('display') == 'none') {
                $('.b-categories-wrap').fadeIn()
            } else {
                $('.b-categories-wrap').fadeOut()
            }
        })
        $(document).click(function () {
            $('.b-categories-wrap').fadeOut()
            check_select('hide')
        })
        $('.b-category-caption').click(function () {
            var list = $(this).data('category')
            $('.b-category-caption--select').removeClass('b-category-caption--select')
            $(this).addClass('b-category-caption--select')
            $('.js-category').hide()
            $('.js-' + list).show()
        })
        $('.b-categories-wrap').click(function (e) {
            e.stopPropagation()
        })

        function check_select(e) {
            if (e == 'hide') {
                $('.b-category-line__bt').removeClass('b-category-line__bt--select')
                return false;
            }
            if ($(e).hasClass('b-category-line__bt--select')) {
                $(e).removeClass('b-category-line__bt--select')
            } else {
                $(e).addClass('b-category-line__bt--select')
            }
        }

    })
    // блок регистраци авторизации на главной
    $('.user-panel__auth').click(function () {
        if ($('.user-panel__auth-wrap').css('display') != 'none') {
            $(this).removeClass('user-panel--select')
            $(this).children('span').addClass('link').addClass('link--fake')
            $('.user-panel__auth-wrap').slideUp(300)
        } else {
            $(this).addClass('user-panel--select')
            $(this).children('span').removeClass('link').removeClass('link--fake')
            $('.user-panel__auth-wrap').slideDown(300)
        }
    })

    // авторизация регистрация --бекграунд при фокусе
    $('.auth-wrap__input').focus(function () {
        $(this).parents('.auth-wrap').addClass('auth-wrap--select')
    })
    $('.auth-wrap__input').blur(function () {
        $(this).parents('.auth-wrap').removeClass('auth-wrap--select')
    })
    // форма создание объявления
    $('.add-ad__bt-type').click(function () {
        $('.add-ad__bt-type').removeClass('add-ad__bt-type--select')
        $(this).addClass('add-ad__bt-type--select')
    })
    // галерея 
    if ($('.ad__imgs--big').length > 0) {
        $('.ad__imgs--big').kinetic({
            'cursor': '-'
        })
    }
    ;
    //всякое
    $('.toggle-auth').click(function () {
        $('.toggle__auth').hide(0)
        $('.toggle__no-auth').show(0)
    })
    //
    $('.js-show-user--on').hide()
    $('.js-show-user').click(function () {
        $('.js-show-user--off').hide()
        $('.js-show-user--on').show()
        $('.user-panel__auth').hide()
        $('.user-panel__auth-wrap').hide()
        $('.user-panel__auth-on').css('display', 'table-cell')
    })
    // попап категорий
    $('.category-popup-show-link').click(function () {
        var top = $(this).offset().top
        $('.category-popup').css('top', top)
        $('.category-popup').show()
    })
    $('.category-popup').click(function (e) {
        e.stopPropagation()
    })
    $('.category-popup-show-link').click(function (e) {
        e.stopPropagation()
    })
    $('body').click(function () {
        $('.category-popup').hide()
    })
    // попап авторизации
    $('.js-auth-link').click(function (e) {
        e.preventDefault()
        $('.auth-popup').show()
    })
    $('.auth-popup-box').click(function (e) {
        e.stopPropagation()
    })
    $('.js-auth-link').click(function (e) {
        e.stopPropagation()
    })
    $('body').click(function () {
        $('.auth-popup').hide()
    })

    // выбор категории с чек листами
    $('.js-popup-category-check-link').click(function () {
        var top = $(this).offset().top
        $('.category-check').css('top', top)
        $('.category-check').show()
    })
    $('.category-check').click(function (event) {
        event.stopPropagation()
    });
    $('.js-popup-category-check-link').click(function (event) {
        event.stopPropagation()
    });
    $('body').click(function () {
        $('.category-check').hide()
    })
    // попап по клику Отправить предложение
    $('.ad__action--offer').click(function (event) {
        event.stopPropagation()
        var that = $(this)
        var left = $(this).offset().left
        var top = $(this).offset().top + $(this).height()
        //console.log(left + '  -  ' + top)
        var popup = $('.ad__popup')
        if ($(popup).hasClass('ad__popup--show')) {
            $(popup)
                .removeClass('ad__popup--show')
                .addClass('ad__popup--hide')
        } else {
            $(popup)
                .removeClass('ad__popup--hide')
                .addClass('ad__popup--show')
                .css({
                    'top': top,
                    'left': left
                })
        }
        $('.b-bt').click(function () {
            that.text('✓ Предложение отправленно')
            that.removeClass('b-link').removeClass('b-link--fake')
            $('.ad__popup')
                .removeClass('ad__popup--show')
                .addClass('ad__popup--hide')
        })
    })
    $('.ad__popup').click(function (event) {
        event.stopPropagation()
    });
    $('body').click(function (event) {
        $('.ad__popup')
            .removeClass('ad__popup--show')
            .addClass('ad__popup--hide')
    });
    //     // Действие по объявлению купить продать лбменть
    // $('.ad__action').click(function(event) {
    //     event.stopPropagation()
    //     $('.wrapper').hide()
    //     $('.ad__action__popup__bg').show()
    // })
    // $('.ad__action__popup__bg').click(function(event) {
    //     event.stopPropagation()
    // })
    // $('body').click(function() {
    //     $('.wrapper').show()
    //     $('.ad__action__popup__bg').hide()
    // })
    // $('.ad__action__popup__close').click(function(event) {
    //     $('.wrapper').show()
    //     $('.ad__action__popup__bg').hide()
    // });
    //чекбксы в попапе предложений
    $('.offer-popup__proposal-check').click(function (event) {
        if ($(this).prop('checked') === true) {
            $(this).parents('.offer-popup__proposal').addClass('offer-popup__proposal--checked')
        } else {
            $(this).parents('.offer-popup__proposal').removeClass('offer-popup__proposal--checked')
        }
    });


    //по клику отображает форму для ответа в кооментах
    $('.comment-answer-link').click(function (event) {
        //console.log($(this))
        // $('.comment-answer-form').show()
        // $('.comment-answer-link').hide()
        $(this).hide();
        $(this).next().show();
    });
})


//ajax loader indicator
$('html').ajaxStart(function () {
    var offset = 20;
    $('html').mousemove(function (e) {
        $('.ajax_load').css('top', (e.pageY + offset) + 'px').css('left', (e.pageX + offset) + 'px')
    });
    $('.ajax_load').fadeIn('fast');
});

$('html').ajaxStop(function () {
    var offset = 20;
    $('html').mousemove(function (e) {
        $('.ajax_load').css('top', (e.pageY + offset) + 'px').css('left', (e.pageX + offset) + 'px')
    });
    $('.ajax_load').fadeOut('fast');
});

//Ajax csrf token
jQuery(document).ajaxSend(function (event, xhr, settings) {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function sameOrigin(url) {
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') || !(/^(\/\/|http:|https:).*/.test(url));
    }

    function safeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
});


$(document).ready(function () {

    //$('.elem a.add-bt.non-auth').click(function () {
    //    $('.b-slide-popup').toggle();
    //});

    $('.content #id_value').attr('maxlength','70');

    $(document.body).on('click', '#bet_add', function () {
        var bet = $('#id_bet').val();
        $('#bet_description').html('');
        var category = $('select[name="category"]').val();
        update_bet_description(bet, category, '#bet_description');
        return false;
    });

    $("a.ad_up").click(function(e) {
        e.preventDefault();
        $("#" + $(this).data('form-id')).show();
    })

    $(document.body).on('click', 'button.bet_add', function () {
        var j_this = $(this);
        var bet = $("#" + j_this.data('input-id')).val();
        $('#bet_description').html('');
        var category = j_this.data('category');
        update_bet_description(bet, category, "#" + j_this.data('description-id'));
        return false;
    });

    function update_bet_description(bet, category, id_description) {
        if (category) {
            $.ajax({
                url: '/catalog/bet/position/' + category + '/',
                data: {bet: bet},
                success: function (json) {
                    var text = '<br>При ставке в ' + json['bet'] + ' р за клик вы будете: ' +
                                '<br>на ' + json['pos_parent'] + ' месте в списке всех объявлений' +
                                '<br>в категории ' + json['parent_name'] +
                                '<br>на ' + json['pos_category'] + ' месте в подкатегории' +
                                '<br> "' + json['category_name'] + '"';
                    $(id_description).html(text);

                }
            });
        } else {
            alert('Выбирите категорию !')
        }
    }

     $('#isearch').keyup(function () {
        $("div.page-search").remove();
        var val = $('input[id=isearch]').val();
        if (val.length) {
            $.ajax({
                type: 'get',
                //dataType : "json",
                url: '/catalog/asearch/',
                data: {
                    val: val,
                    city: $('#actual_city').val()
                },
                success: onAjaxSuccess,
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error: " + errorThrown);
                }
            });
        } else {
            $("div.page").show();
        }


        function onAjaxSuccess(data) {
            // устал
            $("div.page-search").remove();
            var container = $("<div class='page page--margin page-search'></div>");
            var content = $("<div class='page-content'></div>").html(data);
            container.append(content);
            $("div.page-wrap>div.page").hide().after(container);
        }
    });

    $('.hide-ad').click(function () {
        var parent = $(this).parents().closest('.b-item-ad');
        $.ajax({
            type: 'POST',
            url: '/catalog/hide/',
            data: {
                val: $(this).attr('data-ad')
            },
            success: function(data) {
                parent.remove();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log("Error: " + errorThrown);
            }
        });
    });

});

