from gettext import Catalog
from django.conf.urls import patterns, include, url, static

from catalog.models import AdFavorits
from catalog.views import AdView,  MyAdListView, DeleteAd, CatalogView, \
    CategoryView, CreateAd, AdSearchView, EditAd, AdStrip, AdFavorities, ActivateAd, DeactivateAd, \
    BuyContactPageView, HideAd, decline_ad


urlpatterns = patterns('',

    url(r'^$', CatalogView.as_view(), name='catalog'),
    # url(r'^category/(?P<slug>[A-Za-z0-9-_]+)/', 'catalog.views.category', name='category'),
    url(r'^category/(?P<slug>[A-Za-z0-9_-]+)/(?P<ad_type>\w+)?', CategoryView.as_view(), name='category'),
    url(r'^ad/(?P<pk>\d+)/', AdView.as_view(), name='ad_view'),
    url(r'^ad/decline/(?P<pk>\d+)/', decline_ad, name='decline_ad'),
    url(r'^my_ads/(?P<state>\w+)/', MyAdListView.as_view(), name='my_ad_list_view_state'),
    url(r'^my_ads/', MyAdListView.as_view(), name='my_ad_list_view'),
    url(r'^search/', AdSearchView.as_view(), name='search'),
    url(r'^deals/(?P<pk>\d+)/$', AdView.as_view(template_name='catalog/deals-list.html'), name='ad_deals'),
    url(r'^buy_contact/page/', BuyContactPageView.as_view(), name='buy_contact_page'),
    url(r'^buy_contact/', 'catalog.views.buy_contact', name='buy_contact'),

    url(r'^create/', CreateAd.as_view(), name='create_ad'),
    url(r'^edit_ad/(?P<pk>\d+)/', EditAd.as_view(), name='edit_ad'),
    url(r'^delete/(?P<pk>\d+)/', DeleteAd.as_view(), name='delete_ad'),
    url(r'^activate/(?P<pk>\d+)/', ActivateAd.as_view(), name='activate_ad'),
    url(r'^deactivate/(?P<pk>\d+)/', DeactivateAd.as_view(), name='deactivate_ad'),

    url(r'^asearch/', 'catalog.views.asearch', name='a_search'),
    url(r'^hide/', HideAd.as_view(), name='hide'),

    url(r'^load_params/(?P<category>[\w\d_]+)/', 'catalog.views.load_params', name='load_params'),
    url(r'^upload_images/', 'catalog.views.upload_images', name='upload_images'),
    url(r'^upload_delete/(\d+)/', 'catalog.views.delete_images', name='upload_delete'),
    url(r'^get_cities/', 'catalog.views.get_cities', name='get_cities'),
    url(r'^check_city_ad_display/', 'catalog.views.check_city_ad_display', name='check_city_ad_display'),

    url(r'^strip/(?P<ad_type>\w+)?$', AdStrip.as_view(), name='strip'),
    url(r'^favorits/$', AdFavorities.as_view(), name='favorits'),
    url(r'^favorits/(add|del)/(\d+)/', 'catalog.views.AdFavoritiesChange', name='ad_favorits_change'),

)

