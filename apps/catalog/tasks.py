from celery.task import periodic_task
from celery.task import task
from datetime import timedelta
from models import Ad
import datetime

now = datetime.datetime.now()
endDate = now.today()-timedelta(days=30)
ads = Ad.objects.filter(date__lt=endDate)


@task(name='catalog.tasks.active')
@periodic_task(run_every=timedelta(seconds=10))
# @periodic_task(run_every=timedelta(days=1))
def active():
    for ad in ads:
        ad.is_active = 0
        ad.save()