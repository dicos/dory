"use strict";
(function($) {

    var Pillars = {
        init: function(el, parent, td) {
            var b = this
            b.el = $(el)
            b.td = $(td)
            b.pp = $(parent)

            //
            b.refreshPositionValue()
            //
            b.eventScroll()

        },

        refreshPositionValue: function() {
            var b = this

            b.pp.top = b.pp.offset().top
            b.pp.h = b.pp.height()

            b.td.l = b.td.offset().left
            b.td.w = b.td.width()

            b.el.h = b.el.height()

            b.win = {}
            b.win.scroll = $(document).scrollTop()
            b.win.h = $(window).height()
            b.win.top = b.win.scroll
            b.win.topfix = b.win.top
            if (b.win.top < 0) {
                b.win.topfix = 0
            }
            if (b.win.top > (b.pp.h - b.el.h)) {
                b.win.topfix = b.pp.h - b.el.h
            }

            b.win.bot = b.win.top + b.win.h

            b.win.topmax = (b.pp.top + b.pp.h - b.el.h)
            //
            b.el.width(b.td.w)
            b.el.css('left', b.td.l)
            b.td.css('height', b.el.h)
        },
        eventScroll: function() {
            var b = this
            b.scrolling = b.whereScrolling()
            $(document).scroll(function(event) {
                b.scrolling = b.whereScrolling()
                b.refreshPositionValue()
                fixedPillars()
            });
            $(window).resize(function(event) {
                b.scrolling = b.whereScrolling()
                b.refreshPositionValue()
                fixedPillars()

            });


            function log(n) {
                if (b.el.hasClass('x1')) {
                    console.log(n)
                }
            }

            function fixedPillars() {
                if (b.scrollBefore() && b.isBig() && b.scrolling == 'down') {
                    b.el.css('top', b.pp.top - b.win.top)
                }
                if (b.scrollBefore() && b.isBig() && b.scrolling == 'up') {
                    b.el.css('top', b.pp.top - b.win.top)
                }
                //
                if (b.scrollCenter() && b.isBig() && b.scrolling == 'down') {
                    var i = b.pp.top - b.win.top
                    if (i < b.win.h - b.el.h) {
                        i = b.win.h - b.el.h
                    };
                    b.el.css('top', i)
                }
                //
                if (b.scrollCenter() && b.isBig() && b.scrolling == 'up') {
                    var i = b.pp.top - b.win.top
                    if (i < b.win.h - b.el.h) {
                        i = b.win.h - b.el.h
                    };
                    b.el.css('top', i)
                }
                if (b.scrollAfter() && b.isBig() && b.scrolling == 'down') {
                    console.log('after')
                    b.el.css('top', (b.pp.h - b.el.h) + b.pp.top - b.win.top)
                }
                //
                if (b.scrollAfter() && b.isBig() && b.scrolling == 'up') {
                    console.log('after')
                    b.el.css('top', (b.pp.h - b.el.h) + b.pp.top - b.win.top)
                }
                //
                if (!b.isBig()) {
                    var i = b.win.top
                    if (i > b.pp.h + b.pp.top - b.el.h) {
                        i = b.pp.h + b.pp.top - b.win.top
                    };

                    if (b.win.top < b.pp.top) {
                        b.el.css('top', (-b.win.top + b.pp.top))
                        log(1)
                    }
                    if (b.win.top > b.pp.top && (b.win.top) < (b.pp.top + b.pp.h - b.el.h)) {
                        b.el.css('top', 0)
                        log(2)
                    }
                    if ((b.win.top) > (b.pp.top + b.pp.h - b.el.h)) {
                        b.el.css('top', i - b.el.h)
                        log(3)
                    }
                }
            }

            function dragUp() {

            }

            function dragDown() {
                b.el.css('top', b.win.top - b.pp.top)
            }
        },

        isBig: function() {
            var b = this
            var r = false
            if (b.el.height() > $(window).height()) {
                r = true
            }
            return r
        },
        scrollBefore: function() {
            var b = this
            if (b.win.top < b.pp.top) {
                return true
            } else {
                return false
            }
        },
        scrollCenter: function() {
            var b = this
            if (b.win.top > b.pp.top && b.win.top < b.pp.top + b.pp.h) {
                return true
            } else {
                return false
            }
        },
        scrollAfter: function() {
            var b = this
            if (b.win.top + b.win.h > b.pp.top + b.pp.h) {
                return true
            } else {
                return false
            }
        },
        whereScrolling: function() {
            var b = this
            if (b.win.scroll > $(document).scrollTop()) {
                return 'up'
            } else {
                return 'down'
            }
        }
    }
    $.fn.pillars = function(name_child, name_img_wrap) {
        var parent = this
        var load = setInterval(wait, 0)

        function wait() {
            var imgs = $(name_img_wrap).length

            $.each($(name_img_wrap), function(index, el) {
                if ($(el).height() > 0) {
                    imgs = imgs - 1
                }
            });
            //console.log(imgs);
            if (imgs === 0) {
                init()
                clearInterval(load)
            }
        }


        function init() {
            parent.find(name_child).each(function() {
                var pillars = Object.create(Pillars);
                var td = $(this).parent('td')
                var img_wrap = name_img_wrap
                pillars.init(this, parent, td, img_wrap)
            })
        }

    };
})(jQuery);
$(function() {
    if ($('.b-offer').length) {
        $('.b-offer').pillars('.b-offer__col', '.b-one-ad__photo-wrap')
    }
    if ($('.b-ones').length) {
        $('.b-ones').pillars('.col', '.b-one-ad__photo-wrap')

    }
})
