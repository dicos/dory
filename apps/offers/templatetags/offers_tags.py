#!-*-coding:utf-8-*-
from django.template import Library
from django.core.urlresolvers import resolve
from django.core.urlresolvers import reverse

register = Library()



@register.inclusion_tag('offers/offer-filter-side.html', takes_context=True)
def offer_filter_side(context):
    request = context['request']
    view = resolve(request.get_full_path())   
    current_offer_state = view.kwargs['offer_state'] if 'offer_state' in view.kwargs else None
    for k,v in view.kwargs.items():        
        if v is None:
                view.kwargs.pop(k)
    

    from offers.models import OFFER_STATUS
    items=list()
    if 'offer_state' in view.kwargs.keys(): view.kwargs.pop('offer_state')
    items.append({'title': u'Все', 'url': reverse(view.url_name, args=view.args, kwargs=view.kwargs), 'current': current_offer_state==None})

    for k,v in OFFER_STATUS:
        view.kwargs['offer_state']=k
        items.append({'title':v, 'url': reverse(view.url_name, args=view.args, kwargs=view.kwargs), 'current': str(k)==str(current_offer_state)})

    return {'offer_states':items}
