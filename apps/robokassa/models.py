#-* coding: UTF-8 *-

from django.db import models

class PaymentCheck(models.Model):
    order_number = models.IntegerField(u'InvId')
    order_cost = models.CharField(u'OutSum', max_length=50)
    date = models.DateTimeField(u'Date', auto_now=True)

    def __unicode__(self):
        return '%d / %s / %s' % (self.order_number, self.order_cost, self.date)
