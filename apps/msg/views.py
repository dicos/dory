#!-*-coding:utf-8-*-
import json
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Q, Max
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, DetailView
from catalog.models import Ad
from client.models import Client
from forms import AdMessageForm, MailAdMessageForm, PrivateMessageForm
from models import AdMessage, MailAdMessage, PrivateMessage


class AdMessages(ListView):
    model = AdMessage
    template_name = 'msg/ad_messages-list.html'
    context_object_name = 'ad_messages'

    def get(self, request, *args, **kwargs):
        html = render_to_string(self.template_name, {'ad_messages': self.get_queryset()})
        return HttpResponse(json.dumps({'html': html}), 'json')

    def get_queryset(self):
        ad_messages = AdMessage.objects.filter(ad__pk=self.kwargs['ad_id'])
        return ad_messages


class CreateAdMessage(CreateView):
    model = AdMessage
    form_class = AdMessageForm

    def form_invalid(self, form):
        return HttpResponse(json.dumps({'status': False}), 'json')

    def form_valid(self, form):
        ad_message = form.save()
        ad_message.save()
        return HttpResponse(json.dumps({'status': True, 'redirect_url': reverse('ad_view', args=[self.request.POST.getlist('ad')[0]])}), 'json')



class CreateMailMessage(CreateView):
    model = MailAdMessage
    form_class = MailAdMessageForm

    def get_initial(self):
        try:
            return {'ad': Ad.objects.get(ad__pk=self.kwargs['ad_id'])}
        except Exception:
            return {}

    def form_valid(self, form):
        message = form.save()
        email = render_to_string('msg/email/mail_ad_message.html', {'message': message})
        message.ad.client.user.email_user(u'Вопрос по объявлению %s' % message.ad.title, email)
        return HttpResponse(json.dumps({'status': True}), 'json')


@login_required()
def chat_list_view(request, template_name='msg/my_messages.html'):
    client = Client.get_client(request)
    all_messages = PrivateMessage.objects.filter(Q(recipient=client) | Q(sender=client))
    new_messages = all_messages.filter(is_readed=False, recipient=client)
    companions_new = Client.objects.filter(client_sended_messages__in=new_messages).annotate(
        Max('client_sended_messages__date')).order_by('-client_sended_messages__date__max')
    companions_all = Client.objects.filter(client_sended_messages__in=all_messages).annotate(
        Max('client_sended_messages__date')).order_by('-client_sended_messages__date__max')
    if companions_new:
        return HttpResponseRedirect(reverse('chat_view_messages', kwargs={'pk': companions_new.first().pk}))
    elif companions_all:
        return HttpResponseRedirect(reverse('chat_view_messages', kwargs={'pk': companions_all.first().pk}))
    else:
        return render_to_response(template_name, RequestContext(request, {'msg': u'Сообщений нет'}))


class ChatView(DetailView):
    model = Client
    template_name = 'msg/chat_messages.html'
    context_object_name = 'companion'

    def get_context_data(self, **kwargs):
        context = super(ChatView, self).get_context_data(**kwargs)
        client = Client.get_client(self.request)
        all_messages = PrivateMessage.objects.filter(Q(recipient=client) | Q(sender=client))
        new_messages = all_messages.filter(is_readed=False, recipient=client)
        messages = PrivateMessage.objects.filter(
            (Q(sender=client) & Q(recipient=self.object)) | (Q(recipient=client) & Q(sender=self.object)))
        companions_new = Client.objects.filter(client_sended_messages__in=new_messages) \
            .annotate(Max('client_sended_messages__date')).order_by('-client_sended_messages__date__max')
        companions_all = Client.objects.filter(
            Q(client_sended_messages__in=all_messages) | Q(client_received_messages__in=all_messages)) \
            .annotate(Max('client_sended_messages__date')).order_by('-client_sended_messages__date__max') \
            .exclude(pk__in=[x.pk for x in companions_new]).exclude(pk=client.pk)
        context['show_next'] = (len(companions_new) + len(companions_all)) > 25

        if companions_new.count() < 25:
            companions_all = companions_all[:(25 - companions_new.count())]
        else:
            companions_all = Client.objects.none()
        context['companions_new'] = companions_new
        context['companions_list'] = list(companions_new) + list(companions_all)
        context['client'] = client
        context['user_messages'] = messages
        messages.filter(recipient=client).update(is_readed=True)
        create_chat_message_form = PrivateMessageForm(initial={'sender': client, 'recipient': self.get_object()})
        context['form'] = create_chat_message_form

        return context

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ChatView, self).dispatch(request, *args, **kwargs)


class CreateChatMessage(CreateView):
    model = PrivateMessage
    form_class = PrivateMessageForm

    def form_invalid(self, form):
        return HttpResponse(json.dumps({'status': False, 'form': form}))

    def form_valid(self, form):
        self.object = form.save()
        return HttpResponse(json.dumps({'status': True}), 'json')
