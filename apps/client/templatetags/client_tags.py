#!-*-coding:utf-8-*-

from django.template import Library
from catalog.models import Ad
from client.models import Client, PurchaseContact
from msg.models import PrivateMessage
from django_geoip.models import IpRange

from client.utils import get_city

register = Library()


@register.inclusion_tag('blocks/user_menu.html', takes_context=True)
def render_user_menu(context):
    data = {}
    user = context['request'].user
    try:
        client = Client.objects.get(user=user)
    except Exception:
        client = None
    # page = context.META['PATH_INFO']
    page = context['request'].META['PATH_INFO']

    try:
        balance = client.client_balance
    except:
        balance = None

    data = {
        'auth': user.is_authenticated(),
        'back': page,
        'client': client,
        'ads_count': client.client_ads.count() if client else 0,
        'my_offers_count': client.offers.count() if client and client.offers.exists() else 0,
        'balance': balance,
        'new_chat_messages_count': PrivateMessage.objects.filter(is_readed=False, recipient=client).count(),
        'all_chat_messages_count': PrivateMessage.objects.filter(recipient=client),
        'need_auth': context.get('need_auth'),
    }

    return data

from django.middleware.csrf import get_token
@register.inclusion_tag('blocks/signup_form.html', takes_context=True)
def render_signup_form(context):
    return {'csrf_token': get_token(context['request'])}


@register.inclusion_tag('blocks/login_form.html', takes_context=True)
def render_login_form(context):
    return {'csrf_token': get_token(context['request'])}


@register.inclusion_tag('blocks/reset_form.html', takes_context=True)
def render_reset_form(context):
    return {'csrf_token': get_token(context['request'])}


@register.inclusion_tag('blocks/city_name.html', takes_context=True)
def get_city_ad_display(context):
    request = context.get('request', None)
    city = get_city(request)
    return {'city_name': city.name}


@register.filter(name='get_root_categories')
def get_root_categories(value):
    if value:
        return value.filter(parent__isnull=True)
    else:
        return value


@register.assignment_tag(name="ad_has_offer_by_me", takes_context=True)
def ad_has_offer_by_me(context, ad_id):
    request = context['request']
    client = Client.get_client(request)
    try:
        ad = Ad.objects.get(pk=ad_id)
    except Exception:
        ad = None
    if client and ad:
        return ad.ad_offers.filter(client=client).exists()

    return False


@register.filter
def check_purchase(owner, user):
    if owner == user:
        return True
    try:
        PurchaseContact.objects.get(who=user, client=owner)
        return True
    except PurchaseContact.DoesNotExist:
        return False
